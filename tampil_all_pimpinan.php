<?php include 'config/konek_database.php' ?>


<?php
$data_pimpinan = $pimpinan->tampil_pimpinan();
?>


<!DOCTYPE html>
<html>
<head>	
	<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>SIM LPPM UNHAN RI</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
		<link rel="stylesheet" href="css/linearicons.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/magnific-popup.css">
		<link rel="stylesheet" href="css/jquery-ui.css">				
		<link rel="stylesheet" href="css/nice-select.css">							
		<link rel="stylesheet" href="css/animate.min.css">
		<link rel="stylesheet" href="css/owl.carousel.css">				
		<link rel="stylesheet" href="css/main.css">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> SIM LPPM UNHAN RI</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../dist/css/admin.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="assets/flex/flexslider.css">
	
</head>
<body>	
			<header id="header">
				<div class="header-top">
					<div class="container">
			  		<div class="row align-items-center">
			  			<div class="col-lg-6 col-sm-6 col-6 header-top-left">
			  				<ul>
			  					<li><a href="#">home</a></li>
			  					<li><a href="login.php">login</a></li>
			  				</ul>			
			  			</div>
			  			<div class="col-lg-6 col-sm-6 col-6 header-top-right">
							<div class="header-social">
								<a href="https://id-id.facebook.com/unhanhumas.idu.ac.id" target="_blank"><i class="fa fa-facebook"></i></a>

								<a href="https://twitter.com/Unhan_RI" target="_blank"><i class="fa fa-twitter"></i></a>

								<a href="https://www.instagram.com/unhan_ri" target="_blank"><i class="fa fa-instagram"></i></a>

								<a href="https://www.youtube.com/c/universitaspertahananofficial" target="_blank"><i class="fa fa-youtube"></i></a>

							</div>
			  			</div>
			  		</div>			  					
					</div>
				</div>
				<div class="container main-menu">
					<div class="row align-items-center justify-content-between d-flex">
				      <div id="logo">
				        <a href="index.php"><img src="img/a.png" alt="" title="" /></a>
				      </div>
				      <nav id="nav-menu-container">
				        <ul class="nav-menu">
				          <li><a href="index.php">Home</a></li>
				           <li class="menu-has-children"><a href="">Tentang LPPM</a>
				            <ul>
				              <li><a href="">Organisasi</a></li>
				              <li><a href="tampil_pimpinan.php">Pimpinan</a></li>
				              <li><a href="">Staff</a></li>
				            </ul>
				          </li>
				          <li class="menu-has-children"><a href="">Kerja Sama LPPM</a>
				            <ul>
				              <li><a href="">Dalam Negeri</a></li>
				              <li><a href="">Luar Negeri</a></li>
				            </ul>
				          </li>
				          	
				         <li class="menu-has-children"><a href="">DATA REKAPITULASI</a>
				           <ul>
				            <li><a href="elements.php">PENELITIAN</a></li>
				            <li><a href="elements.php">PENGABDIAN KEPADA MASYARAKAT</a></li>
							    <li class="menu-has-children"><a href="">FAKULTAS </a>
								    <ul>
										<li class="menu-has-children"><a href="">FAKULTAS TEKNOLOGI PERTAHANAN</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI TEKNOLOGI PENGINDERAAN</a>
												</li>
												<li><a href="#">PRODI INDUSTRI PERTAHANAN</a></li>
												<li><a href="#">PRODI TEKNOLOGI PERSENJATAAN</a></li>
												<li><a href="#">PRODI TEKNOLOGI DAYA GERAK</a></li>
								   			</ul>
										</li>

										<li><a href="#">FAKULTAS STRATEGI PERTAHANAN</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI TEKNOLOGI PENGINDERAAN</a>
												</li>
												<li><a href="#">PRODI DIPLOMASI PERTAHANAN</a></li>
												<li><a href="#">PRODI STRATEGI PERANG SEMESTA</a></li>
												<li><a href="#">PRODI STRATEGI PERANG LAUT</a></li>
												<li><a href="#">PRODI STRATEGI PERANG UDARA</a></li>
								   			</ul>
										</li>
								    </ul>
							    </li>					                		
				           </ul>
				          </li>					          					          		          
				         
				        </ul>
				      </nav><!-- #nav-menu-container -->					      		  
					</div>
				</div>
			</header><!-- #header -->

				<!-- start footer Area -->		
			<footer class="footer-area section-gap">		
				<div class="row footer-bottom d-flex justify-content-between align-items-center">	
					<div class="text-center">
						<h1 class="text-white">PIMPINAN </h1>
						<h2 class="text-white">LEMBAGA PENELITIAN & PENGABDIAN KEPADA MASYARAKAT </h2>
						<h6 class="text-white"> UNIVERSITAS PERTAHANAN REPUBLIK INDONESIA </h6>
					</div>	
				</div>
				<div class="row footer-bottom d-flex justify-content-between align-items-center">	</div>
			</footer>
			<!-- End footer Area -->

			<section class="">
				<div class=""></div>				
				<div class="container">
					<div class="row fullscreen align-items-center justify-content-between">
						<div class="col-lg-6 col-md-6 banner-left">
						
							<h1 class="text-white">LPPM </h1>
								<h2 class="text-white">LEMBAGA PENELITIAN & PENGABDIAN KEPADA MASYARAKAT </h2>
							<h6 class="text-white"> 
								UNIVERSITAS PERTAHANAN REPUBLIK INDONESIA
							</h6>
						
						</div>
						
					</div>
				</div>					
			</section>
			
			<!-- start banner Area -->

		<body>
		<div class="inner">
		<div class="panel panel-primary">
		<div class="panel-heading text-center">
		<h2></h2>
		</div>
		<?php foreach ($data_pimpinan as $key => $value) : ?>
		<div class="panel-body">
		<div class="col-md-4">
			<img src="assets/img/pimpinan/<?php echo $value['foto_pimpinan'] ?>" class="img-responsive" width="250">
		</div>
		 	<div class="col-md-8">
				<table class="table">
					<tr>
						<th>Nama Pimpinan</th>
						<th><?php echo $value['nama_pimpinan']; ?></th>
					</tr>
					<tr>
						<th>NIP/NRP</th>
						<th><?php echo $value['nomor']; ?></th>
					</tr>
					<tr>
						<th>Pangkat</th>
						<th><?php echo $value['pangkat']; ?></th>
					</tr>
					<tr>
						<th>Jabatan</th>
						<th><?php echo $value['jabatan']; ?></th>
					</tr>								 		
				</table>
			</div>
		</div>
		<?php endforeach ?>	
	</div>
</div>
</body>
	
						
						<div>
							<?php foreach ($data_pimpinan as $key => $value) : ?>
								<div class="panel-body">
								 	<div class="col-md-4">
								 		<img src="assets/img/pimpinan/<?php echo $value['foto_pimpinan'] ?>" class="img-responsive" width="250">
								 	</div>

								 	<div class="col-md-8">
								 	<table class="table">
								 		<tr>
								 			<th>Nama Pimpinan</th>
								 			<th><?php echo $value['nama_pimpinan']; ?></th>
								 		</tr>
								 		<tr>
								 			<th>NIP/NRP</th>
								 			<th><?php echo $value['nomor']; ?></th>
								 		</tr>
								 		<tr>
								 			<th>Pangkat</th>
								 			<th><?php echo $value['pangkat']; ?></th>
								 		</tr>
								 		<tr>
								 			<th>Jabatan</th>
								 			<th><?php echo $value['jabatan']; ?></th>
								 		</tr>
								 		
								 	</table>
								 	</div>
								 </div>
								
								
							<?php endforeach ?>	
						</div>
						
				
			<!-- End banner Area -->
			

			<!-- start footer Area -->		
			<footer class="footer-area section-gap">
				<div class="container">


					<div class="row footer-bottom d-flex justify-content-between align-items-center">
						<p class="col-lg-8 col-sm-12 footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | UNIVERSITAS PERTAHANAN REPUBLIK INDONESIA  <a href="https://www.idu.ac.id/" target="_blank">www.idu.ac.id</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
						<div class="col-lg-4 col-sm-12 footer-social">
							<a href="https://id-id.facebook.com/unhanhumas.idu.ac.id" target="_blank"><i class="fa fa-facebook"></i></a>
							<a href="https://twitter.com/Unhan_RI" target="_blank"><i class="fa fa-twitter"></i></a>
							<a href="https://www.instagram.com/unhan_ri" target="_blank"><i class="fa fa-instagram"></i></a>
							<a href="https://www.youtube.com/c/universitaspertahananofficial" target="_blank"><i class="fa fa-youtube"></i></a>
						</div>
					</div>
				</div>
			</footer>
			<!-- End footer Area -->	

			<script src="js/vendor/jquery-2.2.4.min.js"></script>
			<script src="js/popper.min.js"></script>
			<script src="js/vendor/bootstrap.min.js"></script>			
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>		
 			<script src="js/jquery-ui.js"></script>					
  			<script src="js/easing.min.js"></script>			
			<script src="js/hoverIntent.js"></script>
			<script src="js/superfish.min.js"></script>	
			<script src="js/jquery.ajaxchimp.min.js"></script>
			<script src="js/jquery.magnific-popup.min.js"></script>						
			<script src="js/jquery.nice-select.min.js"></script>					
			<script src="js/owl.carousel.min.js"></script>							
			<script src="js/mail-script.js"></script>	
			<script src="js/main.js"></script>	
		</body>






</html>