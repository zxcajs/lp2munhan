<?php 

$title = "";
$menuHeader = "";
$menuHeaderDetail = "";
$bannerInfo = "";
if (! function_exists('saveFileFromBase64')) {
    function saveFileFromBase64($targetPath, $base64String, $fileName)
    {
        if (!file_exists($targetPath)) {
            mkdir($targetPath, 0777, true);
        }
        
        $img = $base64String;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace('data:image/jpeg;base64,', '', $img);
        $img = str_replace('data:image/jpg;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = $targetPath.$fileName;
        
        return file_put_contents($file, $data);
    }
}

if (! function_exists('setTitle')) {
    function setTitle($param){
        global $title;

        $title = $param;
    }
}

if (! function_exists('getTitle')) {
    function getTitle(){
        global $title;

        echo $title;
    }
}

if (! function_exists('setMenuHeader')) {
    function setMenuHeader($param){
        global $menuHeader;

        $menuHeader = $param;
    }
}

if (! function_exists('getMenuHeader')) {
    function getMenuHeader(){
        global $menuHeader;
        
        echo htmlspecialchars_decode($menuHeader);
    }
}

if (! function_exists('setMenuHeaderDetail')) {
    function setMenuHeaderDetail($param){
        global $menuHeaderDetail;

        $menuHeaderDetail = $param;
    }
}

if (! function_exists('getMenuHeaderDetail')) {
    function getMenuHeaderDetail(){
        global $menuHeaderDetail;
        
        echo htmlspecialchars_decode($menuHeaderDetail);
    }
}

if (! function_exists('setBannerInfo')) {
    function setBannerInfo($param){
        global $bannerInfo;

        $bannerInfo = $param;
    }
}

if (! function_exists('getBannerInfo')) {
    function getBannerInfo(){
        global $bannerInfo;
        
        echo htmlspecialchars_decode($bannerInfo);
    }
}