<?php 

    $basePath = '/'.APP_ROOT;
    $basePath = $basePath=='/' ? '' : $basePath;
    //For get URL PATH
    $urlpath = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    switch ($urlpath) {
        case $basePath.'/' :
        case $basePath.'/index.php':
            require "main/home/home.php";
            break;
        //SIM
        case $basePath.'/sim' :
            include "main/sim/index_sim.php";
            break;
        case $basePath.'/sim-organisasi' :
            require_once "main/sim/organisasi/organisasi.php";
            break;
        case $basePath.'/sim-pimpinan' :
            require_once "main/sim/pimpinan/pimpinan.php";
            break;
        case $basePath.'/sim-staff' :
            require_once "main/sim/staff/staff.php";
            break;
        case $basePath.'/sim-kermadn' :
            require_once "main/sim/kermadn/kermadn.php";
            break;
        case $basePath.'/sim-kermaln' :
            require_once "main/sim/kermaln/kermaln.php";
            break;
        //LITDOS
        case $basePath.'/litdos' :
            include 'main/siplitdos/index_litdos.php';
            break;
        case $basePath.'/litdos-data-penelitian' :
            require_once 'main/siplitdos/penelitian/tampil_penyakit.php';
            break;
        case $basePath.'/litdos-detail-penelitian' :
            require_once 'main/siplitdos/penelitian/detail_penelitian.php';
            break;
        case $basePath.'/litdos-data-penelitian-dosen' :
            require_once 'main/siplitdos/dosen/tampil_dosen.php';
            break;
        case $basePath.'/litdos-detail-penelitian-dosen' :
            require_once 'main/siplitdos/dosen/detail_dosen.php';
            break;
        case $basePath.'/litdos-rekap-all' :
            require_once 'main/siplitdos/rekapitulasi/rekap_all.php';
            break;
        case $basePath.'/litdos-detail-fakultas' :
            require_once 'main/siplitdos/fakultas/detail_fakultas.php';
            break;
        case $basePath.'/litdos-detail-prodi' :
            require_once 'main/siplitdos/prodi/detail_prodi.php';
            break;
        case $basePath.'/pkm' :
            include 'main/pkm/index_pkm.php';
            break;
        case $basePath.'/sibana' :
            include 'main/sibana/index_sibana.php';
            break;
        default:
            http_response_code(404);
            header("location: ".Helper::baseUrl()."admin/404/index.php", true, 301);
            exit();
            break;
    }
?>