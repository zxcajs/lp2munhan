<?php include 'Helper/Helper.php'; ?>
<?php include 'config/app.config.php' ?>
<?php include 'config/helper.php' ?>
<?php setBannerInfo('<div class="container pb-5">
    	<div class="row py-5 align-items-center">
      	<div class="col-lg-6">
        <h5 class="display-4 mb-4 font-weight-bold text-white"></h5>
        <h1 class="display-4 mb-4 font-weight-bold text-white"> SIMLPPM </h1>
		<h2 class="text-white">SISTEM INFORMASI MANAJEMEN LPPM</h2>
		<h2> </h2>	
		<p class="h5 aa mb-4 pb-3 text-white-50">       
			<div class=""> 
				<a href=""><i class=""></i><h6 class="text-white"> LEMBAGA PENELITIAN DAN PENGABDIAN KEPADA MASYARAKAT </h6></a> 
			</div>
		</p>
		<p class="h5 aa mb-4 pb-3 text-white-50">       
			<div class=""> 
				<a href="">
				<i class=""></i>
				</a> 
				<a href="">
				<i class=""></i>
				</a> 
				<a href="">
				<i class=""></i>
				</a> 
			</div>
		</p>
        <p class="h5 aa mb-4 pb-3 text-white-50"></p>
        <h3 class="text-white mb-5"> </h3>
        <h3 class="text-white mb-5"> </h3>
        <p class="h5 aa mb-4 pb-3 text-white-50"></p>  
      </div>
      <div class="col-lg-6 text-lg-right text-center mt-5 mt-lg-0">
        <div class="banner-phone-image"> <img src="assets/img/penyakit/UNHAN_FIX.png"> </div>
      </div>
    </div>'
		); ?>
<?php setMenuHeader(
				'<div class="container main-menu">
					<div class="row align-items-center justify-content-between d-flex">
					<div id="logo">
						<a href="index.php"><img src="assets/img/penyakit/kecil.png" alt="" title="" /></a>
					</div>
					<nav id="nav-menu-container">
						<ul class="nav-menu">
						<li><a href="'.Helper::baseUrl().'index.php">Home</a></li>
						<li class="menu-has-children"><a href="">Tentang LPPM</a>
							<ul>
							<li><a href="'.Helper::baseUrl().'organisasi.php">Organisasi</a></li>
							<li><a href="'.Helper::baseUrl().'tampil_pimpinan.php">Pimpinan</a></li>
							<li><a href="'.Helper::baseUrl().'tampil_staff.php">Staff</a></li>
							</ul>
						</li>
						<li class="menu-has-children"><a href="">Kerja Sama LPPM</a>
							<ul>
							<li><a href="'.Helper::baseUrl().'tampil_kermadl.php">Dalam Negeri</a></li>
							<li><a href="'.Helper::baseUrl().'tampil_kermaln.php">Luar Negeri</a></li>
							</ul>
						</li>
						</ul>
					</nav><!-- #nav-menu-container -->					      		  
					</div>
				</div>'
		); ?>
<?php include 'layout/header.php'; ?>
<?php include 'layout/section.php'; ?>
<?php include 'layout/footer.php'; ?>