<?php include 'config/konek_database.php' ?>



<?php 
// objek pasien menjalankan fungsi tampil_pasien
$prodi = $prodi->get_fakultas();
 ?>


<!DOCTYPE html>
<html>
<head>	
	<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>SIM LPPM UNHAN RI</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
		<link rel="stylesheet" href="css/linearicons.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/magnific-popup.css">
		<link rel="stylesheet" href="css/jquery-ui.css">				
		<link rel="stylesheet" href="css/nice-select.css">							
		<link rel="stylesheet" href="css/animate.min.css">
		<link rel="stylesheet" href="css/owl.carousel.css">				
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="assets/dataTables/dataTables.min.css">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> SIM LPPM UNHAN RI</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../dist/css/admin.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="assets/flex/flexslider.css">
	
</head>
<body>	
	<header id="header">
				<div class="header-top">
					<div class="container">
			  		<div class="row align-items-center">
			  			<div class="col-lg-6 col-sm-6 col-6 header-top-left">
			  				<ul>
			  					<li><a href="index.php">home</a></li>
			  					<li><a href="login.php">login</a></li>
			  				</ul>			
			  			</div>
			  			<div class="col-lg-6 col-sm-6 col-6 header-top-right">
							<div class="header-social">
								<a href="https://id-id.facebook.com/unhanhumas.idu.ac.id" target="_blank"><i class="fa fa-facebook"></i></a>

								<a href="https://twitter.com/Unhan_RI" target="_blank"><i class="fa fa-twitter"></i></a>

								<a href="https://www.instagram.com/unhan_ri" target="_blank"><i class="fa fa-instagram"></i></a>

								<a href="https://www.youtube.com/c/universitaspertahananofficial" target="_blank"><i class="fa fa-youtube"></i></a>

							</div>
			  			</div>
			  		</div>			  					
					</div>
				</div>
				<div class="container main-menu">
					<div class="row align-items-center justify-content-between d-flex">
				      <div id="logo">
				        <a href="index.html"><img src="assets/img/penyakit/kecil.png" alt="" title="" /></a>
				      </div>
				       <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <nav id="nav-menu-container">
				        <ul class="nav-menu">
				          <li><a href="index_litdos.php">Home</a></li>
				           <li class="menu-has-children"><a href="tampil_laporan_dosen.php">DOSEN</a>	</li>
	      				   <li class="menu-has-children"><a href="">DATA PENELITIAN</a>
				           <ul>
				            <li><a href="elements.html">DATA REKAPITULASI </a></li>
							    <li class="menu-has-children"><a href="">DATA PRODI </a>
								    <ul>
										<li class="menu-has-children"><a href="">DOKTORAL ILMU PERTAHANAN (S3)</a>
											<ul>
												<li class="menu-has-children"><a href="">KONSENTRASI STRATEGI PERTAHANAN</a>
												</li>
												<li><a href="#">KONSENTRASI MANAJEMEN PERTAHANAN</a></li>
												<li><a href="#">KONSENTRASI KEAMANAN NASIONAL</a></li>
												<li><a href="#">KONSENTRASI TEKNOLOGI PERTAHANAN</a></li>
												
								   			</ul>
										</li>
										<li><a href="#">FAKULTAS STRATEGI PERTAHANAN (S2)</a>
											<ul>
												<li><a href="#">PRODI DIPLOMASI PERTAHANAN</a></li>
												<li><a href="#">PRODI STRATEGI PERANG SEMESTA</a></li>
												<li><a href="#">PRODI STRATEGI PERANG LAUT</a></li>
												<li><a href="#">PRODI STRATEGI PERANG UDARA</a></li>
								   			</ul>
										</li>
										<li><a href="#">FAKULTAS MANAJEMEN PERTAHANAN (S2)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI MANAJEMEN PERTAHANAN</a>
												</li>
												<li><a href="#">PRODI EKONOMI PERTAHANAN</a></li>
												<li><a href="#">PRODI KETAHANAN ENERGI</a></li>
								   			</ul>
										</li>
										<li><a href="#">FAKULTAS KEAMANAN NASIONAL (S2)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI MANAJEMEN BENCANA</a>
												</li>
												<li><a href="#">PRODI DAMAI DAN RESOLUSI KONFLIK</a></li>
												<li><a href="#">PRODI KEAMANAN MARITIM</a></li>
								   			</ul>
										</li>
										<li class="menu-has-children"><a href="">FAKULTAS TEKNOLOGI PERTAHANAN (S2)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI TEKNOLOGI PENGINDERAAN</a>
												</li>
												<li><a href="#">PRODI INDUSTRI PERTAHANAN</a></li>
												<li><a href="#">PRODI TEKNOLOGI PERSENJATAAN</a></li>
												<li><a href="#">PRODI TEKNOLOGI DAYA GERAK</a></li>
												<li><a href="#">PRODI TEKNOLOGI PERTAHANAN CYBER</a></li>
								   			</ul>
										</li>
										<li class="menu-has-children"><a href="">FAKULTAS KEDOKTERAN MILITER (S1)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI SARJANA PENDIDIKAN KEDOKTERAN</a>
												</li>
												<li><a href="#">PRODI PROFESI KEDOKTERAN</a></li>
								   			</ul>
										</li>
											<li class="menu-has-children"><a href="">FAKULTAS MIPA MILITER (S1)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI SARJANA BIOLOGI</a>
												</li>
												<li><a href="#">PRODI SARJANA KIMIA</a></li>
												<li><a href="#">PRODI SARJANA FISIKA</a></li>
												<li><a href="#">PRODI SARJANA MATEMATIKA</a></li>
												
								   			</ul>
										</li>
										<li class="menu-has-children"><a href="">FAKULTAS TEKNIK MILITER (S1)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI TEKNOLOGI PENGINDERAAN</a>
												</li>
												<li><a href="#">PRODI SARJANA TEKNIK INFORMATIKA</a></li>
												<li><a href="#">PRODI SARJANA TEKNIK KONSTRUKSI BANGUNANA MILITER</a></li>
												<li><a href="#">PRODI SARJANA TEKNIK ELEKTRO</a></li>
												<li><a href="#">PRODI SARJANA TEKNIK MESIN</a></li>
								   			</ul>
										</li>
											<li class="menu-has-children"><a href="">FAKULTAS FARMASI MILITER (S1)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI SARJANA ILMU FARMASI</a>
												
								   			</ul>
										</li>



										
								    </ul>
							    </li>					                		
				           </ul>
				          </li>	
				     			          					          		          
				         
				        </ul>
				      </nav><!-- #nav-menu-container -->					      		  
					</div>
				</div>
			</header><!-- #header -->
			
		
			

			<!-- start footer Area -->		
			<footer class="footer-area section-gap">		
				<div class="row footer-bottom d-flex justify-content-between align-items-center">	
				<div class="text-center">
						<h1 class="text-white">SISTEM INFORMASI PUBLIKASI PENELITIAN DOSEN </h1>
						<h1 class="text-white">DATA REKAPITULASI LPPM </h1>
						<h2 class="text-white"> </h2>
						<h6 class="text-white"> LEMBAGA PENELITIAN & PENGABDIAN KEPADA MASYARAKAT  </h6>
					</div>		
				</div>
				<div class="row footer-bottom d-flex justify-content-between align-items-center">	</div>
			</footer>
			<!-- End footer Area -->
<body>

	 <div class="panel-primary">
		 <div class="panel-heading text-center">
		 <h2></h2>
		 </div>
		 <div class="panel-body">
		 	<section class="inner">
				<div class="form-body wow fadeIn animated">
				<div class="box">
				<div class="panel-body">
					 <div class="inner">
					 	<div class="panel panel-primary">
							 <div class="panel-heading text-center">
							 <h2></h2>
							 </div> 
						</div>
					</div>
					<div class=" text-center inner">
						<table class="table table-bordered table-striped  " id="datatables">
							<thead>
									<tr>
										<th scope="col" class="text-center">NO </th>			
										<th scope="col" class="text-center">FAKULTAS</th>
										<th scope="col" class="text-center">JUMLAH DOSEN/PENULIS</th>
										<th scope="col" class="text-center"></th>
									</tr>
								</thead>
								<tbody>
								<?php foreach ($prodi as $key => $value) :?>
									<tr>
										<td><?php echo $key+1 ?></td>
									
										<td><?php echo $value['nama_fakultas'] ?></td>
										
										<td><?php echo $value['jml_lit_fakultas'] ?></td>

										<td>
										
										<a href="index.php?halaman=detail_rekapitulasi&id_rekapitulasi=<?php echo $value['id_fakultas']; ?>" class="btn btn-primary">Lihat Data</a>	

										</td>
										
									</tr>
							<?php endforeach ?>
							</tbody>
						</table>
						
					</div>
					 <div class="inner">
					 	<div class="panel panel-primary">
							 <div class="panel-heading text-center">
							 <h2></h2>
							 </div> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
		 </div>
		</div>
	</div>
</body>
			<footer class="footer-area section-gap">
				<div class="container">

					<div class="row">
						<div class="col-lg-3  col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>LPPM Unhan RI</h6>
								<p>
									Merupakan lembaga...
								</p>
							</div>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>Kantor LPPM UNHAN RI</h6>
								<div class="row">
									<div class="col">
										<ul>
											<li><a href="#"> UNIVERSITAS PERTAHANAN RI</a></li>
											<li><a href="#"> PROGRAM STUDI</a></li>
											<li><a href="#"> PASCASARJANA & DOKTORAL </a></li>
											<li><a href="https://goo.gl/maps/FjcfbZjpXkSCMLL87">Jl. Salemba Raya No.3, RT.1/RW.3, Paseban, Jakarta, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10440</a></li>
										
										</ul>
									</div>
																	
								</div>							
							</div>
						</div>							
					
						<div class="col-lg-3  col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>Email</h6>
								<p>
									More information about LPPM UNHAN
								</p>								
								<div id="mc_embed_signup">
									<form target="_blank" action="">
										<div class="input-group d-flex flex-row">
											<input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
											<button class="btn bb-btn"><span class="lnr lnr-location"></span></button>		
										</div>									
										<div class="mt-10 info"></div>
									</form>
								</div>
							</div>
						</div>						
					</div>

					<div class="row footer-bottom d-flex justify-content-between align-items-center">
						<p class="col-lg-8 col-sm-12 footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | UNIVERSITAS PERTAHANAN REPUBLIK INDONESIA  <a href="https://www.idu.ac.id/" target="_blank">www.idu.ac.id</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
						<div class="col-lg-4 col-sm-12 footer-social">
							<a href="https://id-id.facebook.com/unhanhumas.idu.ac.id" target="_blank"><i class="fa fa-facebook"></i></a>
							<a href="https://twitter.com/Unhan_RI" target="_blank"><i class="fa fa-twitter"></i></a>
							<a href="https://www.instagram.com/unhan_ri" target="_blank"><i class="fa fa-instagram"></i></a>
							<a href="https://www.youtube.com/c/universitaspertahananofficial" target="_blank"><i class="fa fa-youtube"></i></a>
						</div>
					</div>
				</div>
			</footer>
			<!-- End footer Area -->	


	

			<script src="js/vendor/jquery-2.2.4.min.js"></script>
			<script src="js/popper.min.js"></script>
			<script src="js/vendor/bootstrap.min.js"></script>			
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>		
 			<script src="js/jquery-ui.js"></script>					
  			<script src="js/easing.min.js"></script>			
			<script src="js/hoverIntent.js"></script>
			<script src="js/superfish.min.js"></script>	
			<script src="js/jquery.ajaxchimp.min.js"></script>
			<script src="js/jquery.magnific-popup.min.js"></script>						
			<script src="js/jquery.nice-select.min.js"></script>					
			<script src="js/owl.carousel.min.js"></script>							
			<script src="js/mail-script.js"></script>	
			<script src="js/main.js"></script>	
			<script src="assets/dataTables/dataTables.min.js"></script>
				<script>
					$(document).ready(function() {
						$('#datatables').DataTable();
					} );
				</script>
		</body>
</html>




