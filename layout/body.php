<body>	
    <div class="banner-image">
        <p class="h5 aa mb-4 pb-3 text-white-50"></p>
        <h3 class="text-white mb-5"> </h3>
        <h3 class="text-white mb-5"> </h3>
        <p class="h5 aa mb-4 pb-3 text-white-50"></p>
        <h3 class="text-white mb-5"> </h3>
        <h3 class="text-white mb-5"> </h3>
        <?php getBannerInfo() ?>
        
        <div class="col-lg-6 text-lg-right text-center mt-5 mt-lg-0">
            
        </div>
        </div>
    </div>
    </div>
    <header id="header">
        <div class="header-top">
            <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-sm-6 col-6 header-top-left">
                    <ul>
                        <li><a href="<?php echo Helper::baseUrl()?>">HOME</a></li>
                        <li><a href="<?php echo Helper::baseUrl()?>admin/login/login.php">LOGIN</a></li>
                    </ul>			
                </div>
                <div class="col-lg-6 col-sm-6 col-6 header-top-right">
                    <div class="header-social">
                        <a href="https://id-id.facebook.com/unhanhumas.idu.ac.id" target="_blank"><i class="fa fa-facebook"></i></a>

                        <a href="https://twitter.com/Unhan_RI" target="_blank"><i class="fa fa-twitter"></i></a>

                        <a href="https://www.instagram.com/unhan_ri" target="_blank"><i class="fa fa-instagram"></i></a>

                        <a href="https://www.youtube.com/c/universitaspertahananofficial" target="_blank"><i class="fa fa-youtube"></i></a>

                    </div>
                </div>
            </div>			  					
            </div>
        </div>
        <?php getMenuHeader(); ?>
    </header><!-- #header -->