<!-- start footer Area -->		
<footer class="footer-area section-gap">
    <div class="container">

        <div class="row">
            <div class="col-lg-3  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>LPPM Unhan RI</h6>
                    <p>
                        Merupakan lembaga...
                    </p>
                </div>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Kantor LPPM UNHAN RI</h6>
                    <div class="row">
                        <div class="col">
                            <ul>
                                <li><a href="#"> UNIVERSITAS PERTAHANAN RI</a></li>
                                <li><a href="#"> PROGRAM STUDI</a></li>
                                <li><a href="#"> PASCASARJANA & DOKTORAL </a></li>
                                <li>
                                    <a>
                                        <a  href="https://goo.gl/maps/FjcfbZjpXkSCMLL87">Jl. Salemba Raya No.3, RT.1/RW.3, Paseban, Jakarta, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10440</a>
                                    </a>
                                    
                                </li>
                            
                                
                            
                            </ul>
                        </div>
                                                        
                    </div>							
                </div>
            </div>							
        
                <div class="col-lg-3  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Email</h6>
                    <p>
                        More information about LPPM UNHAN
                    </p>								
                    <div id="mc_embed_signup">
                        <form target="_blank" action="">
                            <div class="input-group d-flex flex-row">
                                <input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
                                <button class="btn bb-btn"><span class="lnr lnr-location"></span></button>		
                            </div>									
                            <div class="mt-10 info"></div>
                        </form>
                    </div>
                </div>
            </div>

        </div>

        <div class="row footer-bottom d-flex justify-content-between align-items-center">
            <p class="col-lg-8 col-sm-12 footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | UNIVERSITAS PERTAHANAN REPUBLIK INDONESIA  <a href="https://www.idu.ac.id/" target="_blank">www.idu.ac.id</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            <div class="col-lg-4 col-sm-12 footer-social">
                <a href="https://id-id.facebook.com/unhanhumas.idu.ac.id" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://twitter.com/Unhan_RI" target="_blank"><i class="fa fa-twitter"></i></a>
                <a href="https://www.instagram.com/unhan_ri" target="_blank"><i class="fa fa-instagram"></i></a>
                <a href="https://www.youtube.com/c/universitaspertahananofficial" target="_blank"><i class="fa fa-youtube"></i></a>
            </div>
        </div>
    </div>
</footer>
<!-- End footer Area -->	
        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>			
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>		
        <script src="js/jquery-ui.js"></script>					
        <script src="js/easing.min.js"></script>			
        <script src="js/hoverIntent.js"></script>
        <script src="js/superfish.min.js"></script>	
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>						
        <script src="js/jquery.nice-select.min.js"></script>					
        <script src="js/owl.carousel.min.js"></script>							
        <script src="js/mail-script.js"></script>	
        <script src="js/main.js"></script>	
    </body>
</html>