<!DOCTYPE html>
<html lang="zxx" lang="en" class="no-js">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
		<link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet">
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>SISTEM INFORMASI LPPM UNHAN RI</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="css/linearicons.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/magnific-popup.css">
		<link rel="stylesheet" href="css/jquery-ui.css">				
		<link rel="stylesheet" href="css/nice-select.css">							
		<link rel="stylesheet" href="css/animate.min.css">
		<link rel="stylesheet" href="css/owl.carousel.css">				
		<link rel="stylesheet" href="css/main.css">
		<style type="text/css">
			@import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap');
			body {
				font-family: 'Roboto', sans-serif;
			}
			.btn {
				/* padding: 14px 26px; */
				font-weight: 700;
				font-size: 13px;
				letter-spacing: 1px;
				text-transform: uppercase;
			}
			.btn-danger {
				background-color: #e34c43;
				border-color: #e34c43;
			}
			.banner-image{
				background: linear-gradient(rgba(29, 38, 113, 0.8), rgba(195, 55, 100, 0.8)), url(assets/img/penyakit/asd2.png);
				background-size: cover;
				background-position: center;
			}
			.banner-phone-image img{
				width:300px;
			}
			.star-rating a{
			color:rgba(255,255,255,0.5);
			margin-right:10px;
			}
			.star-rating a:hover{
			color:#fff;
			}
			.aa{
			line-height:26px;    
			}
		</style>
	</head>
	