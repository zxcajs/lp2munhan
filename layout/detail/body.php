<body>	
	<header id="header">
		<div class="header-top">
			<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6 col-sm-6 col-6 header-top-left">
					<ul>
						<li><a href="<?php echo Helper::baseUrl()?>">HOME</a></li>
                        <li><a href="<?php echo Helper::baseUrl()?>admin/login/login.php">LOGIN</a></li>
					</ul>			
				</div>
				<div class="col-lg-6 col-sm-6 col-6 header-top-right">
					<div class="header-social">
						<a href="https://id-id.facebook.com/unhanhumas.idu.ac.id" target="_blank"><i class="fa fa-facebook"></i></a>

						<a href="https://twitter.com/Unhan_RI" target="_blank"><i class="fa fa-twitter"></i></a>

						<a href="https://www.instagram.com/unhan_ri" target="_blank"><i class="fa fa-instagram"></i></a>

						<a href="https://www.youtube.com/c/universitaspertahananofficial" target="_blank"><i class="fa fa-youtube"></i></a>

					</div>
				</div>
			</div>			  					
			</div>
		</div>
		<?php getMenuHeaderDetail(); ?>
	</header><!-- #header -->
    <!-- start footer Area -->		
    <footer class="footer-area section-gap">		
        <div class="row footer-bottom d-flex justify-content-between align-items-center">	
        <div class="text-center">
                <h1 class="text-white">SISTEM INFORMASI PUBLIKASI PENELITIAN DOSEN </h1>
                <h1 class="text-white">DATA PENELITIAN LPPM </h1>
                <h2 class="text-white"> </h2>
                <h6 class="text-white"> LEMBAGA PENELITIAN & PENGABDIAN KEPADA MASYARAKAT  </h6>
            </div>		
        </div>
        <div class="row footer-bottom d-flex justify-content-between align-items-center">	</div>
    </footer>
    <!-- End footer Area -->
    <body>
	 <div class="panel-primary">
		 <div class="panel-heading text-center">
		 <h2></h2>
		 </div>
		 <div class="panel-body">
		 	<section class="inner">
				<div class="form-body wow fadeIn animated">
				<div class="box">
				<div class="panel-body">