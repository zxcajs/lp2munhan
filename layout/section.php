<!-- Start blog Area -->
<section class="recent-blog-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-9">
                <div class="title text-center">
                    <h1 class="mb-10">LPPM UNHAN RI</h1>
                    <p>Kegiatan Lembaga Penelitian dan Pengabdian Masyarakat UNHAN RI</p>

                </div>
            </div>
        </div>							
        <div class="row">
            <div class="active-recent-blog-carusel">
                <div class="single-recent-blog-post item">
                    <div class="thumb">
                        <img class="img-fluid" src="img/pks/3.jpg" alt="">
                    </div>
                    <div class="details">
                        <div class="tags">
                            
                        </div>
                        <a href="#"><h4 class="title">LPPM Menerima Kunjungan</h4></a>
                        <p>
                            LPPM Unhan RI Menerima Kunjungan dari instansi/ lembaga lain.
                        </p>
                        <h6 class="date">25 September 2022</h6>
                    </div>	
                </div>
                <div class="single-recent-blog-post item">
                    <div class="thumb">
                        <img class="img-fluid" src="img/pks/5.jpg" alt="">
                    </div>
                    <div class="details">
                        <div class="tags">
                            
                        </div>
                        <a href="#"><h4 class="title">Perjanjian Kerja Sama</h4></a>
                        <p>
                            LPPM melakukan perjanjian kerja sama pendidikan, penelitian dan pengabdian kepada masyarakat.
                        </p>
                        <h6 class="date">16 Oktober 2022</h6>
                    </div>	
                </div>
                <div class="single-recent-blog-post item">
                    <div class="thumb">
                        <img class="img-fluid" src="img/pks/8.jpeg" alt="">
                    </div>
                    <div class="details">
                        <div class="tags">
                        
                        </div>
                        <a href="#"><h4 class="title">Berpartisipasi Research Expo 2022</h4></a>
                        <p>
                            LPPM Unhan RI ikut berpartisipasi dalam agenda kegiatan Research Expo 2022.
                        </p>
                        <h6 class="date">27-30 Oktober 2022</h6>
                    </div>	
                </div>	
                <div class="single-recent-blog-post item">
                    <div class="thumb">
                        <img class="img-fluid" src="img/pks/1.jpeg" alt="">
                    </div>
                    <div class="details">
                        <div class="tags">
                            
                        </div>
                        <a href="#"><h4 class="title">Berpartisipasi Research Expo 2022</h4></a>
                        <p>
                            LPPM Unhan RI ikut berpartisipasi dalam agenda kegiatan Research Expo 2022.
                        </p>
                        <h6 class="date">27-30 Oktober 2022</h6>
                    </div>	
                </div>
                <div class="single-recent-blog-post item">
                    <div class="thumb">
                        <img class="img-fluid" src="img/pks/lit.jpg" alt="">
                    </div>
                    <div class="details">
                        <div class="tags">
                        
                        </div>
                        <a href="#"><h4 class="title">Penelitian LPPM</h4></a>
                        <p>
                            Penelitian Dosen UNHAN RI dalam pengembangan PLTS di pulau Semau NTT.
                        </p>
                        <h6 class="date"> September 2022</h6>
                        
                    </div>	
                </div>
                <div class="single-recent-blog-post item">
                    <div class="thumb">
                        <img class="img-fluid" src="img/pks/foto_lit.png" alt="">
                    </div>
                    <div class="details">
                        <div class="tags">
                            
                        </div>
                        <a href="#"><h4 class="title">Penelitian LPPM</h4></a>
                        <p>
                            Penelitian Dosen UNHAN RI dalam Survey Pendugaan Air Tanah di Wilayah NTB.
                        </p>
                        <h6 class="date">15 Oktober 2022</h6>
                    </div>	
                </div>														

            </div>
        </div>
    </div>	
</section>
<!-- End recent-blog Area -->			