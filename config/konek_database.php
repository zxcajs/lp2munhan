<?php 

include 'db.config.php';

// use PDO;
session_start();
//membuat koneksi database
//harus ada syarat yg terpnuhi yaitu (nama host, username, passwd, nama database);
$userId = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;

date_default_timezone_set('Asia/Jakarta');

// koneksi database
$database = \Config::getConfig();

//membuat class dosen
class RefData
{
	//membuat atribut untuk koneksi (nyambung) ke database
	function __construct($database)
	{
		//mengisi atribut koneksi dengan persyaratan koneksi databaase
		$this->koneksi = $database;
	}

	function getPangkatList()
	{
		$stmt = $this->koneksi->query("SELECT id,name FROM ref_pangkat");
		return $stmt->fetch_all(MYSQLI_ASSOC);
	}

	function getJabatanList()
	{
		$stmt = $this->koneksi->query("SELECT id,`name` FROM ref_jabatan");
		return $stmt->fetch_all(MYSQLI_ASSOC);
	}

	function getJabatanAkademikList()
	{
		$stmt = $this->koneksi->query("SELECT id,nama as `name` FROM ref_jabatan_akademik");
		return $stmt->fetch_all(MYSQLI_ASSOC);
	}

	function getFakultasList()
	{
		$stmt = $this->koneksi->query("SELECT id,nama as `name` FROM ref_fakultas");
		return $stmt->fetch_all(MYSQLI_ASSOC);
	}

	function getProdiList()
	{
		$stmt = $this->koneksi->query("SELECT id,nama as `name` FROM ref_prodi");
		return $stmt->fetch_all(MYSQLI_ASSOC);
	}

	function getFakultasId($id)
	{
		$stmt = $this->koneksi->query("SELECT ref_fakultas_id FROM ref_prodi where id=$id");
		return $stmt->fetch_row()[0];
	}

	function getFakultasName($id)
	{
		$stmt = $this->koneksi->query("SELECT nama FROM ref_fakultas where id=$id");
		return $stmt->fetch_row()[0];
	}

	function getProdiId($id)
	{
		$stmt = $this->koneksi->query("SELECT ref_prodi_id FROM dosen where id=$id");
		return $stmt->fetch_row()[0];
	}

	function getPublisherList()
	{
		$stmt = $this->koneksi->query("SELECT id,name,is_other FROM ref_publisher");
		return $stmt->fetch_all(MYSQLI_ASSOC);
	}

	function getCountryList()
	{
		$stmt = $this->koneksi->query("SELECT id,name FROM ref_country");
		return $stmt->fetch_all(MYSQLI_ASSOC);
	}

	function getJurnalList()
	{
		$data = [
			[
				'id' => 'national_akreditasi',
				'name'=> 'National (Akreditasi)',
			],
			[
				'id' => 'national_nonakreditasi',
				'name'=> 'National (Non Akreditasi)',
			],
			[
				'id' => 'inter_reputasi',
				'name'=> 'International (Bereputasi)',
			],
			[
				'id' => 'inter_nonreputasi',
				'name'=> 'International (Non Reputasi)',
			],
			[
				'id' => 'none',
				'name'=> 'Tidak Ada',
			],
		];

		return $data;
	}

	function getAuthorList()
	{
		$data = [
			[
				'id' => 'tim',
				'name'=> 'TIM',
			],
			[
				'id' => 'mahasiswa',
				'name'=> 'Mahasiswa',
			],
			[
				'id' => 'mandiri',
				'name'=> 'Mandiri',
			],
		];

		return $data;
	}
}

class Grafik
{
	function __construct($database)
	{
		//mengisi atribut koneksi dengan persyaratan koneksi databaase
		$this->koneksi = $database;
	}

	function grafikPenelitianByProdi($id)
	{
		$stmt = $this->koneksi->query("SELECT DISTINCT tahun from penelitian where type='general' and ref_prodi_id=$id order by tahun asc");
		$tahun = [];
		$authorTim = $authorMandiri = $authorMahasiswa = $jurnalNasAkr = $jurnalNasNonAkr = $jurnalInterRep = $jurnalInterNonRep = $google = $nonGoogle = $jmlDosen = $jmlPenelitian
		= $jmlPublish = [];
		while ($row = $stmt->fetch_row()) {
			array_push($tahun,$row[0]);
			$stmtAuthorTim = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_prodi_id=$id and author='tim' and tahun='$row[0]'");
			array_push($authorTim,(int)$stmtAuthorTim->fetch_row()[0]);
			$stmtAuthorMandiri = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_prodi_id=$id and author='mandiri' and tahun='$row[0]'");
			array_push($authorMandiri,(int)$stmtAuthorMandiri->fetch_row()[0]);
			$stmtAuthorMahasiswa = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_prodi_id=$id and author='mahasiswa' and tahun='$row[0]'");
			array_push($authorMahasiswa,(int)$stmtAuthorMahasiswa->fetch_row()[0]);
			
			$stmtJurnalNasAkr = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_prodi_id=$id and jurnal='national_akreditasi' and tahun='$row[0]'");
			array_push($jurnalNasAkr,(int)$stmtJurnalNasAkr->fetch_row()[0]);
			$stmtJurnalNonAkr = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_prodi_id=$id and jurnal='national_nonakreditasi' and tahun='$row[0]'");
			array_push($jurnalNasNonAkr,(int)$stmtJurnalNonAkr->fetch_row()[0]);
			$stmtJurnalInterRep = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_prodi_id=$id and jurnal='inter_reputasi' and tahun='$row[0]'");
			array_push($jurnalInterRep,(int)$stmtJurnalInterRep->fetch_row()[0]);
			$stmtJurnalInterNonRep = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_prodi_id=$id and jurnal='inter_nonreputasi' and tahun='$row[0]'");
			array_push($jurnalInterNonRep,(int)$stmtJurnalInterNonRep->fetch_row()[0]);
			
			$stmtGoogle = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_prodi_id=$id and punya_google_scholar='1' and tahun='$row[0]'");
			array_push($google,(int)$stmtGoogle->fetch_row()[0]);
			$stmtNonGoogle = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_prodi_id=$id and punya_google_scholar='0' and tahun='$row[0]'");
			array_push($nonGoogle,(int)$stmtNonGoogle->fetch_row()[0]);

			$stmtPenelitian = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_prodi_id=$id and tahun='$row[0]'");
			array_push($jmlPenelitian,(int)$stmtPenelitian->fetch_row()[0]);
			$stmtPublish = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_prodi_id=$id and jurnal<>'none' and tahun='$row[0]'");
			array_push($jmlPublish,(int)$stmtPublish->fetch_row()[0]);
			$stmtDosen = $this->koneksi->query("SELECT count(distinct xx.dosen_id) 
												from penelitian x 
												inner join penulis_penelitian xx on x.id=xx.penelitian_id
												where type='general' and x.ref_prodi_id=$id and x.tahun='$row[0]'");
			array_push($jmlDosen,(int)$stmtDosen->fetch_row()[0]);
		}
		
		$series = [
			[
				'name' => 'JUMLAH PENELITIAN',
				'data' => $jmlPenelitian
	
			],
			[
				'name' => 'JUMLAH PUBLIKASI ARTIKEL',
				'data' => $jmlPublish
			],
			[
				'name' => 'JUMLAH DOSEN',
				'data' => $jmlDosen
			],
			[
				'name' => 'JUMLAH JURNAL NASIONAL AKREDITASI',
				'data' => $jurnalNasAkr
	
			],
			[
				'name' => 'JUMLAH JURNAL NASIONAL NON AKREDITASI',
				'data' => $jurnalNasNonAkr
	
			],
			[
				'name' => 'JUMLAH JURNAL INTERNATIONAL REPUTASI',
				'data' => $jurnalInterRep
	
			],
			[
				'name' => 'JUMLAH JURNAL INTERNATIONAL NON REPUTASI',
				'data' => $jurnalInterNonRep
	
			],
			[
				'name' => 'AUTHOR / CORRESPONDING AUTHOR MANDIRI',
				'data' => $authorMandiri
	
			],
			[
				'name' => 'AUTHOR / CORRESPONDING AUTHOR TIM',
				'data' => $authorTim
	
			],
			[
				'name' => 'AUTHOR / CORRESPONDING AUTHOR MAHASISWA',
				'data' => $authorMahasiswa
	
			],
			[
				'name' => 'MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'data' => $google
	
			],
			[
				'name' => 'TIDAK MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'data' => $nonGoogle
	
			],
		];
		
		$response['tahun'] = $tahun;
		$response['series'] = $series;

		return $response;
	}

	function grafikPenelitianByFakultas($id)
	{
		$stmt = $this->koneksi->query("SELECT DISTINCT tahun from penelitian where type='general' and ref_fakultas_id=$id order by tahun asc");
		$tahun = [];
		$authorTim = $authorMandiri = $authorMahasiswa = $jurnalNasAkr = $jurnalNasNonAkr = $jurnalInterRep = $jurnalInterNonRep = $google = $nonGoogle = $jmlDosen = $jmlPenelitian
		= $jmlPublish = [];
		while ($row = $stmt->fetch_row()) {
			array_push($tahun,$row[0]);
			$stmtAuthorTim = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_fakultas_id=$id and author='tim' and tahun='$row[0]'");
			array_push($authorTim,(int)$stmtAuthorTim->fetch_row()[0]);
			$stmtAuthorMandiri = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_fakultas_id=$id and author='mandiri' and tahun='$row[0]'");
			array_push($authorMandiri,(int)$stmtAuthorMandiri->fetch_row()[0]);
			$stmtAuthorMahasiswa = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_fakultas_id=$id and author='mahasiswa' and tahun='$row[0]'");
			array_push($authorMahasiswa,(int)$stmtAuthorMahasiswa->fetch_row()[0]);
			
			$stmtJurnalNasAkr = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_fakultas_id=$id and jurnal='national_akreditasi' and tahun='$row[0]'");
			array_push($jurnalNasAkr,(int)$stmtJurnalNasAkr->fetch_row()[0]);
			$stmtJurnalNonAkr = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_fakultas_id=$id and jurnal='national_nonakreditasi' and tahun='$row[0]'");
			array_push($jurnalNasNonAkr,(int)$stmtJurnalNonAkr->fetch_row()[0]);
			$stmtJurnalInterRep = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_fakultas_id=$id and jurnal='inter_reputasi' and tahun='$row[0]'");
			array_push($jurnalInterRep,(int)$stmtJurnalInterRep->fetch_row()[0]);
			$stmtJurnalInterNonRep = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_fakultas_id=$id and jurnal='inter_nonreputasi' and tahun='$row[0]'");
			array_push($jurnalInterNonRep,(int)$stmtJurnalInterNonRep->fetch_row()[0]);
			
			$stmtGoogle = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_fakultas_id=$id and punya_google_scholar='1' and tahun='$row[0]'");
			array_push($google,(int)$stmtGoogle->fetch_row()[0]);
			$stmtNonGoogle = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_fakultas_id=$id and punya_google_scholar='0' and tahun='$row[0]'");
			array_push($nonGoogle,(int)$stmtNonGoogle->fetch_row()[0]);

			$stmtPenelitian = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_fakultas_id=$id and tahun='$row[0]'");
			array_push($jmlPenelitian,(int)$stmtPenelitian->fetch_row()[0]);
			$stmtPublish = $this->koneksi->query("SELECT count(id) from penelitian where `type`='general' and ref_fakultas_id=$id and jurnal<>'none' and tahun='$row[0]'");
			array_push($jmlPublish,(int)$stmtPublish->fetch_row()[0]);
			$stmtDosen = $this->koneksi->query("SELECT count(distinct xx.dosen_id) 
												from penelitian x 
												inner join penulis_penelitian xx on x.id=xx.penelitian_id
												where type='general' and x.ref_fakultas_id=$id and x.tahun='$row[0]'");
			array_push($jmlDosen,(int)$stmtDosen->fetch_row()[0]);
		}
		
		$series = [
			[
				'name' => 'JUMLAH PENELITIAN',
				'data' => $jmlPenelitian
	
			],
			[
				'name' => 'JUMLAH PUBLIKASI ARTIKEL',
				'data' => $jmlPublish
			],
			[
				'name' => 'JUMLAH DOSEN',
				'data' => $jmlDosen
			],
			[
				'name' => 'JUMLAH JURNAL NASIONAL AKREDITASI',
				'data' => $jurnalNasAkr
	
			],
			[
				'name' => 'JUMLAH JURNAL NASIONAL NON AKREDITASI',
				'data' => $jurnalNasNonAkr
	
			],
			[
				'name' => 'JUMLAH JURNAL INTERNATIONAL REPUTASI',
				'data' => $jurnalInterRep
	
			],
			[
				'name' => 'JUMLAH JURNAL INTERNATIONAL NON REPUTASI',
				'data' => $jurnalInterNonRep
	
			],
			[
				'name' => 'AUTHOR / CORRESPONDING AUTHOR MANDIRI',
				'data' => $authorMandiri
	
			],
			[
				'name' => 'AUTHOR / CORRESPONDING AUTHOR TIM',
				'data' => $authorTim
	
			],
			[
				'name' => 'AUTHOR / CORRESPONDING AUTHOR MAHASISWA',
				'data' => $authorMahasiswa
	
			],
			[
				'name' => 'MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'data' => $google
	
			],
			[
				'name' => 'TIDAK MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'data' => $nonGoogle
	
			],
		];
		
		$response['tahun'] = $tahun;
		$response['series'] = $series;

		return $response;
	}

	function grafikPenelitianAll($tahun='all')
	{
		$andTahun = "";
		if($tahun<>'all'){
			$andTahun = " and tahun='$tahun'";
		}
		$stmt = $this->koneksi->query("SELECT id,nama from ref_fakultas order by nama asc");
		$fakultas = [];
		$refIds = [];
		while ($row = $stmt->fetch_row()) {
			array_push($fakultas,$row[1]);
			array_push($refIds,$row[0]);
		}

		//get list tahun
		$stmtTahun = $this->koneksi->query("SELECT distinct tahun from penelitian where 1=1 $andTahun order by tahun asc");
		$rsTahun = $stmtTahun->fetch_all(MYSQLI_ASSOC);
		
		//set tahun for series
		$series = [];
		foreach($rsTahun as $k => $v){
			$th = $v['tahun'];

			$tmpData = [];
			foreach ($refIds as $fakultasId) {
				$query = "SELECT count(id) as total from penelitian where type='general' and ref_fakultas_id=$fakultasId and tahun='$th' and jurnal<>'none'";
				$stmtData = $this->koneksi->query($query);
				
				array_push($tmpData, (int)$stmtData->fetch_row()[0]);
			}

			$tmpSeries = [
				'name' => $th,
				'data' => $tmpData
			];
			array_push($series,$tmpSeries);
		}
		
		$response['fakultas'] = $fakultas;
		$response['series'] = $series;
		
		return $response;
	}

	function grafikPenelitianAllByTahun($tahun='all')
	{
		$andTahun = "";
		if($tahun<>'all'){
			$andTahun = " and tahun='$tahun'";
		}
		//get list tahun
		$tahun = [];
		$stmtTahun = $this->koneksi->query("SELECT distinct tahun from penelitian where 1=1 $andTahun order by tahun asc");
		while ($row = $stmtTahun->fetch_row()) {
			array_push($tahun,$row[0]);
		}
		
		//set series
		$jmlPenelitian = $jmlPublish = $jmljurnalnasakr = $jmljurnalnasnonakr = $jmljurnalinterrep = $jmljurnalinternonrep = $jmlgoogle = $jmlnongoogle = $jmlDosen = $jmlMandiri = $jmlTim = $jmlMahasiswa = [];
		foreach($tahun as $vTahun){
			$query = "SELECT 
						(select count(id) from penelitian where `type`='general' and tahun='$vTahun') as jml_penelitian,
						(select count(id) from penelitian where `type`='general' and jurnal<>'none' and tahun='$vTahun') as jml_publikasi,
						(select count(distinct xx.dosen_id) 
							from penelitian x 
							inner join penulis_penelitian xx on x.id=xx.penelitian_id
							where x.type='general' and x.tahun= '$vTahun') as jml_dosen,
						(select count(id) from penelitian where `type`='general' and author='tim') as jml_tim,
						(select count(id) from penelitian where `type`='general' and author='mahasiswa') as jml_mahasiswa,
						(select count(id) from penelitian where `type`='general' and author='mandiri') as jml_mandiri,
						(select count(id) from penelitian where `type`='general' and jurnal='national_akreditasi' and tahun='$vTahun') as jml_jurnal_nas_akr,
						(select count(id) from penelitian where `type`='general' and jurnal='national_nonakreditasi' and tahun='$vTahun') as jml_jurnal_nas_nonakr,
						(select count(id) from penelitian where `type`='general' and jurnal='inter_reputasi' and tahun='$vTahun') as jml_jurnal_inter_rep,
						(select count(id) from penelitian where `type`='general' and jurnal='inter_nonreputasi' and tahun='$vTahun') as jml_jurnal_inter_nonrep,
						(select count(id) from penelitian where `type`='general' and punya_google_scholar='1' and tahun='$vTahun') as jml_google,
						(select count(id) from penelitian where `type`='general' and punya_google_scholar='0' and tahun='$vTahun') as jml_nongoogle";
		
			$stmtPublish = $this->koneksi->query($query);
			$rsPublish = $stmtPublish->fetch_assoc();
			
			array_push($jmlPenelitian,(int)$rsPublish['jml_penelitian']);
			array_push($jmlPublish,(int)$rsPublish['jml_publikasi']);
			array_push($jmljurnalnasakr,(int)$rsPublish['jml_jurnal_nas_akr']);
			array_push($jmljurnalnasnonakr,(int)$rsPublish['jml_jurnal_nas_nonakr']);
			array_push($jmljurnalinterrep,(int)$rsPublish['jml_jurnal_inter_rep']);
			array_push($jmljurnalinternonrep,(int)$rsPublish['jml_jurnal_inter_nonrep']);
			array_push($jmlgoogle,(int)$rsPublish['jml_google']);
			array_push($jmlnongoogle,(int)$rsPublish['jml_nongoogle']);
			array_push($jmlDosen,(int)$rsPublish['jml_dosen']);
			array_push($jmlTim,(int)$rsPublish['jml_tim']);
			array_push($jmlMahasiswa,(int)$rsPublish['jml_mahasiswa']);
			array_push($jmlMandiri,(int)$rsPublish['jml_mandiri']);
		}
		$series = [
			[
				'name' => 'JUMLAH PENELITIAN',
				'key' => 'jml_penelitian',
				'data' => $jmlPenelitian
	
			],
			[
				'name' => 'JUMLAH PUBLIKASI ARTIKEL',
				'key' => 'jml_publikasi',
				'data' => $jmlPublish
			],
			[
				'name' => 'JUMLAH DOSEN',
				'key' => 'jml_jml_dosen',
				'data' => $jmlDosen
			],
			[
				'name' => 'JUMLAH JURNAL NASIONAL AKREDITASI',
				'key' => 'jml_jurnal_nas_akr',
				'data' => $jmljurnalnasakr
	
			],
			[
				'name' => 'JUMLAH JURNAL NASIONAL NON AKREDITASI',
				'key' => 'jml_jurnal_nas_nonakr',
				'data' => $jmljurnalnasnonakr
	
			],
			[
				'name' => 'JUMLAH JURNAL INTERNATIONAL REPUTASI',
				'key' => 'jml_jurnal_inter_rep',
				'data' => $jmljurnalinterrep
	
			],
			[
				'name' => 'JUMLAH JURNAL INTERNATIONAL NON REPUTASI',
				'key' => 'jml_jurnal_inter_nonrep',
				'data' => $jmljurnalinternonrep
	
			],
			[
				'name' => 'AUTHOR / CORRESPONDING AUTHOR MANDIRI',
				'key' => 'jml_mandiri',
				'data' => $jmlMandiri
	
			],
			[
				'name' => 'AUTHOR / CORRESPONDING AUTHOR TIM',
				'key' => 'jml_tim',
				'data' => $jmlTim
	
			],
			[
				'name' => 'AUTHOR / CORRESPONDING AUTHOR MAHASISWA',
				'key' => 'jml_mahasiswa',
				'data' => $jmlMahasiswa
	
			],
			[
				'name' => 'MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'key' => 'jml_google',
				'data' => $jmlgoogle
	
			],
			[
				'name' => 'TIDAK MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'key' => 'jml_nongoogle',
				'data' => $jmlnongoogle
	
			],
		];
		
		$response['tahun'] = $tahun;
		$response['series'] = $series;
		
		return $response;
	}

	function grafikPenelitianAllByProdi($tahun='all')
	{
		$andTahun = "";
		if($tahun<>'all'){
			$andTahun = " and tahun='$tahun'";
		}
		$stmt = $this->koneksi->query("SELECT id,nama from ref_prodi order by nama asc");
		$prodi = [];
		$refIds = [];
		while ($row = $stmt->fetch_row()) {
			array_push($prodi,$row[1]);
			array_push($refIds,$row[0]);
		}

		//get list tahun
		$stmtTahun = $this->koneksi->query("SELECT distinct tahun from penelitian where 1=1 $andTahun order by tahun asc");
		$rsTahun = $stmtTahun->fetch_all(MYSQLI_ASSOC);
		
		//set tahun for series
		$series = [];
		foreach($rsTahun as $k => $v){
			$th = $v['tahun'];

			$tmpData = [];
			foreach ($refIds as $prodiId) {
				$query = "SELECT count(id) as total from penelitian where type='general' and ref_prodi_id=$prodiId and tahun='$th' and jurnal<>'none'";
				$stmtData = $this->koneksi->query($query);
				
				array_push($tmpData, (int)$stmtData->fetch_row()[0]);
			}

			$tmpSeries = [
				'name' => $th,
				'data' => $tmpData
			];
			array_push($series,$tmpSeries);
		}
		
		$response['prodi'] = $prodi;
		$response['series'] = $series;
		
		return $response;
	}
}

//membuat class dosen
class dosen
{
	//membuat atribut untuk koneksi (nyambung) ke database
	function __construct($database,$userId)
	{
		//mengisi atribut koneksi dengan persyaratan koneksi databaase
		$this->koneksi = $database;
		$this->userId = $userId;
	}

	//membuat fungsi untuk menampilkan data dosen
	function tampil_dosen()
	{
		//setiap berhubungan dengan database menggunakan $this->koneksi

		//mengambil data dari tabel dosen
		$ambil = $this->koneksi->query("SELECT a.id as id_dosen, a.nama as nama_dosen,a.nrp,b.nama as prodi,c.nama as fakultas,
					(select count(a.id) from penelitian a inner join penulis_penelitian b on a.id=b.penelitian_id where b.dosen_id=id_dosen and type='general') as jml_lit,
					(select count(a.id) from penelitian a inner join penulis_penelitian b on a.id=b.penelitian_id where b.dosen_id=id_dosen and type='general' and jurnal<>'none') as jml_publish
					FROM dosen a 
					INNER JOIN ref_prodi b on a.ref_prodi_id=b.id
					INNER JOIN ref_fakultas c on b.ref_fakultas_id=c.id
					order by a.updated_at desc
				");

		$semuadata = [];
		//data yang terambil diubah ke bentuk array dan diperulangan karena datanya lebih dari 1
		while ($data_array = $ambil->fetch_assoc())
		{
			//menampung data tiap perulangan ke dalam array multi
			$semuadata[]= $data_array;
		}
		
		// outputkan semuadata karena datanya akan dipakai oleh file lain
		return $semuadata;
	}

	function getListDosen()
	{
		//mengambil data dari tabel dosen
		$ambil = $this->koneksi->query("SELECT a.id as id_dosen, a.nama as nama_dosen
					FROM dosen a 
					order by a.nama asc
				");

		return $ambil->fetch_all(MYSQLI_ASSOC);
	}

	function getSelectedListDosen($dosenIds)
	{
		//mengambil data dari tabel dosen
		$ambil = $this->koneksi->query("SELECT a.id as id_dosen, a.nama as nama_dosen
					FROM dosen a 
					order by a.nama asc
				");

		$rs = $ambil->fetch_all(MYSQLI_ASSOC);
		foreach($rs as $k => $v){
			$rs[$k]['selected'] = '0';
			if(in_array($v['id_dosen'],$dosenIds)){
				$rs[$k]['selected'] = '1';
			}
		}
		
		return $rs;
	}

	function simpan_dosen($nama_dosen,$nrp,$pangkat,$jabatan,$jabatan_akademik,$keilmuan,$posisi,$prodi )
	{
		$query = "INSERT INTO dosen (nama,nrp,ref_pangkat_id,ref_jabatan_id,ref_jabatan_akademik_id,keilmuan,posisi,ref_prodi_id,created_by) 
					VALUES ('$nama_dosen','$nrp','$pangkat','$jabatan','$jabatan_akademik','$keilmuan','$posisi','$prodi',$this->userId)";
		
		$this->koneksi->query($query);
	}

	function validasi_nrp($nrp)
	{
		$ambil = $this->koneksi->query ("SELECT * FROM dosen WHERE nrp='$nrp'");
		$hitung = $ambil->num_rows;
		if ($hitung > 0)
		{
			return "sukses";
		}
		else
		{
			return "gagal";
		}
	}

	function ubah_password_lupa($pass, $nrp)
	{
		$this->koneksi->query("UPDATE data_pasien SET password_pasien='$pass' WHERE nrp='$nrp' ");
	}


	function hapus_dosen($id_dosen)
	{
	//mendapatkan data dosen yang akan dihapus supaya tahu nama tahunnya
	//fungsi ini mengakses fungsi detail_dosen
		//$data = $this->hapus_dosen($id_dosen);

	//menghapus data dari database berdasar id_dosen yang akan dihapus
		$this->koneksi->query("DELETE FROM dosen WHERE id='$id_dosen' ");
	}

	function ubah_dosen($nama_dosen,$nrp,$pangkat,$jabatan,$jabatan_akademik,$keilmuan,$posisi,$prodi,$id_dosen )
	{
		//mengubah data di DB
		$query = "UPDATE dosen SET nama='$nama_dosen', nrp='$nrp', ref_pangkat_id='$pangkat', ref_jabatan_id='$jabatan', ref_jabatan_akademik_id='$jabatan_akademik', keilmuan='$keilmuan', posisi='$posisi', ref_prodi_id='$prodi', updated_by='$this->userId' WHERE id='$id_dosen'";
		
		$this->koneksi->query($query);
	}


	function tampil_dosen_id($id_dosen)
	{
		//setiap berhubungan dengan database menggunakan $this->koneksi

		//mengambil data dari tabel dosen
		$ambil = $this->koneksi->query("SELECT * FROM data_pasien WHERE id_dosen='$id_dosen'");

		return $ambil->fetch_assoc();
	}
	

	function detail_dosen($id_dosen)

	{
		$ambil= $this->koneksi->query("SELECT a.nama as nama_dosen,a.nrp,b.name as pangkat, c.name as jabatan,d.nama as jabatan_akademik,
				a.keilmuan,a.posisi,e.nama as prodi, f.nama as fakultas,a.ref_pangkat_id,a.ref_jabatan_id,a.ref_jabatan_akademik_id,a.ref_prodi_id,e.ref_fakultas_id
			FROM dosen a
			inner join ref_pangkat b on a.ref_pangkat_id=b.id
			inner join ref_jabatan c on a.ref_jabatan_id=c.id 
			INNER JOIN ref_jabatan_akademik d on a.ref_jabatan_akademik_id=d.id
			INNER JOIN ref_prodi e on a.ref_prodi_id=e.id
			INNER JOIN ref_fakultas f on e.ref_fakultas_id=f.id
			WHERE a.id='$id_dosen' ");

		return $ambil->fetch_assoc();

	}
	function ubah_password($password_pasien,$id_pasien)
	{
		$this->koneksi->query("UPDATE data_pasien SET password_pasien='$password_pasien' WHERE id_dosen='$id_dosen'");
	}
	
}


class lit
{
	//membuat atribut untuk koneksi (nyambung) ke database
	function __construct($database,$userId)
	{
		//mengisi atribut koneksi dengan persyaratan koneksi databaase
		$this->koneksi = $database;
		$this->userId = $userId;
	}

	//membuat fungsi untuk menampilkan data penyakit
	function tampil_lit()
	{
		//mengambil data dari tabel penyakit
		$ambil = $this->koneksi->query("SELECT a.id as id_lit,a.judul_penelitian, a.document 
										FROM penelitian a 
										WHERE `type`='general'
										ORDER by a.updated_by desc");

		$rs = $ambil->fetch_all(MYSQLI_ASSOC);
		
		foreach($rs as $k => $v){
			$dosenlist = $this->koneksi->query("SELECT b.nama
										FROM penulis_penelitian a 
										inner join dosen b on a.dosen_id=b.id
										WHERE penelitian_id={$v['id_lit']}");
			$dosen = [];
			//data yang terambil diubah ke bentuk array dan diperulangan karena datanya lebih dari 1
			while ($data_array = $dosenlist->fetch_assoc())
			{
				//menampung data tiap perulangan ke dalam array multi
				$dosen[] = $data_array['nama'];
			}
			$dosen = implode(', ',$dosen);
			$rs[$k]['nama_penulis'] = $dosen;
		}

		return $rs;
	}

	function update_jml_lit($nama_penulis)
	{

		$this->koneksi->query("UPDATE data_pasien SET jml_lit = jml_lit + 1 WHERE nama_dosen = '$nama_penulis'");
		// $this->koneksi->query("UPDATE data_pasien dosen join  prodi p on p.nama_prodi = dosen.prodi SET p.jml_lit_prodi = p.jml_lit_prodi + 1 WHERE dosen.nama_dosen = '$nama_penulis'");
		// $this->koneksi->query("UPDATE data_pasien dosen join  prodi p on p.nama_prodi = dosen.prodi SET p.jml_lit_fakultas = p.jml_lit_fakultas + 1 WHERE dosen.nama_dosen = '$nama_penulis'");

	}

	function update_jml_author($nama_penulis)
	{

		$this->koneksi->query("UPDATE data_pasien SET jml_author = jml_author + 1 WHERE nama_dosen = '$nama_penulis'");
		

	}

	function update_jml_jurnal($nama_penulis)
	{

		$this->koneksi->query("UPDATE data_pasien SET jml_jurnal = jml_jurnal + 1 WHERE nama_dosen = '$nama_penulis'");
		
	}

	function get_nama_dosen($id_dosen)
	{
		$ids = implode(',',$id_dosen);
		$ambil= $this->koneksi->query("SELECT id_dosen FROM data_pasien where id_dosen in ({$ids})");

		return $ambil->fetch_all();
	}

	function update_jml_lit_prodi($nama_prodi)
	{
		$ambil= $this->koneksi->query("UPDATE prodi SET jml_lit_prodi = jml_lit_prodi + 1  where nama_prodi='$nama_prodi'");

	}

	function update_jml_lit_author($nama_prodi)
	{
		$ambil= $this->koneksi->query("UPDATE prodi SET jml_author_prodi = jml_author_prodi + 1  where nama_prodi='$nama_prodi'");

	}

	function update_jml_lit_jurnal($nama_prodi)
	{
		$ambil= $this->koneksi->query("UPDATE prodi SET jml_jurnal_prodi = jml_jurnal_prodi + 1  where nama_prodi='$nama_prodi'");

	}

	function update_jml_lit_fakultas($nama_fakultas)
	{
		$ambil= $this->koneksi->query("UPDATE fakultas SET jml_lit_fakultas = jml_lit_fakultas + 1 where nama_fakultas= '$nama_fakultas'");

	}

	function simpan_lit($dosenId,$judul,$author,$tahun,$jurnal,$otherPublisher,$issn,$terbit,$negara,$haveGoogle,$urlGoogle,$type,$publisherId,$prodiId,$fakultasId,$file)
	{
		if($jurnal=='none'){
			$tahun = '';
			$otherPublisher = '';
			$issn = '';
			$terbit = '1970-01-01';
			$negara = 0;
			$haveGoogle = 0;
			$urlGoogle = '';
			$publisherId = 0;
		}
		
		if($file['size']==0){
			echo 'silahkan upload file penelitian';
			exit;
		}

		$allowed = array('pdf');
		$filename = $file['name'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if (!in_array($ext, $allowed)) {
			echo 'file type not allowed';
			exit;
		}
		
		$filename = 'penelitian_' . date("YmdHis") . '.pdf';
        $targetPath = __SITE_PATH . '/upload/penelitian';
        $targetFile = str_replace('//', '/', $targetPath);

		move_uploaded_file($file['tmp_name'], $targetFile.'/'.$filename);

		$query = "INSERT INTO penelitian 
					(judul_penelitian,author,jurnal,tahun,other_publisher,issn,tanggal_terbit,ref_country_id,punya_google_scholar,google_scholar_link,
					`type`,ref_publisher_id,ref_prodi_id,ref_fakultas_id,created_by,document) VALUES 
					('$judul','$author','$jurnal','$tahun','$otherPublisher','$issn','$terbit','$negara','$haveGoogle','$urlGoogle',
					'$type','$publisherId','$prodiId','$fakultasId',$this->userId,'$filename')";
		
		$this->koneksi->query($query);

		//input dosen
		$sql= "INSERT INTO penulis_penelitian (penelitian_id,dosen_id) VALUES ";
		foreach($dosenId as $dId){
			$sql.="({$this->koneksi->insert_id},{$dId}), ";
		}
		$sql = rtrim($sql, ', ');
		$this->koneksi->query($sql);
	}
	
	function ubah_lit($dosenId,$judul,$author,$tahun,$jurnal,$otherPublisher,$issn,$terbit,$negara,$haveGoogle,$urlGoogle,$type,$publisherId,$prodiId,$fakultasId,$file,$id)
	{
		if($jurnal=='none'){
			$tahun = '';
			$otherPublisher = '';
			$issn = '';
			$terbit = '1970-01-01';
			$negara = 0;
			$haveGoogle = 0;
			$urlGoogle = '';
			$publisherId = 0;
		}

		$document = "";
		if($file['size']>0){
			$allowed = array('pdf');
			$filename = $file['name'];
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			if (!in_array($ext, $allowed)) {
				echo 'file type not allowed';
				exit;
			}
			
			$filename = 'penelitian_' . date("YmdHis") . '.pdf';
			$targetPath = __SITE_PATH . '/upload/penelitian';
			$targetFile = str_replace('//', '/', $targetPath);

			$ambil= $this->koneksi->query("SELECT document FROM penelitian where id=$id");

			$rsExist = $ambil->fetch_assoc();
			if($rsExist && file_exists($targetFile.'/'.$rsExist['document']))
			{
				unlink($targetFile.'/'.$rsExist['document']);
			}
	
			move_uploaded_file($file['tmp_name'], $targetFile.'/'.$filename);

			$document = ", document='$filename'";
		}

		$query = "UPDATE penelitian SET
					judul_penelitian='$judul',
					author='$author',
					jurnal='$jurnal',
					tahun='$tahun',
					other_publisher='$otherPublisher',
					issn='$issn',
					tanggal_terbit='$terbit',
					ref_country_id='$negara',
					punya_google_scholar='$haveGoogle',
					google_scholar_link='$urlGoogle',
					type='$type',
					ref_publisher_id='$publisherId',
					ref_prodi_id='$prodiId',
					ref_fakultas_id='$fakultasId',
					updated_by='$this->userId'
					$document
				WHERE id=$id
					";
		$this->koneksi->query($query);

		$this->koneksi->query("DELETE FROM penulis_penelitian WHERE penelitian_id={$id}");
		//input dosen
		$sql= "INSERT INTO penulis_penelitian (penelitian_id,dosen_id) VALUES ";
		foreach($dosenId as $dId){
			$sql.="({$id},{$dId}), ";
		}
		$sql = rtrim($sql, ', ');
		
		$this->koneksi->query($sql);	
	}
	
	function detail_lit($id_lit)
	{
		//mengambil data dari tabel penyakit
		$ambil = $this->koneksi->query("SELECT a.id as id_lit,a.judul_penelitian,a.author,a.tahun,
											case when a.jurnal='national_akreditasi' then 'National (Akreditasi)'
												when a.jurnal='national_nonakreditasi' then 'National (Non Akreditasi)'
												when a.jurnal='inter_reputasi' then 'International (Bereputasi)'
												when a.jurnal='inter_nonreputasi' then 'International (Non Reputasi)'
												else 'Tidak Ada' end as jurnal,a.jurnal as ref_jurnal,punya_google_scholar
											,a.ref_publisher_id,a.other_publisher,
											case when a.ref_publisher_id=17 then other_publisher else c.name end as publiser,
											a.issn,a.tanggal_terbit as terbit,d.name as negara,a.ref_country_id,a.google_scholar_link as `url`,
											a.ref_prodi_id,a.ref_fakultas_id,e.nama as ref_fakultas_name,ef.nama as ref_prodi_name
										FROM penelitian a 
										INNER JOIN ref_fakultas e on a.ref_fakultas_id=e.id
										INNER JOIN ref_prodi ef on a.ref_prodi_id=ef.id
										LEFT JOIN ref_publisher c on a.ref_publisher_id=c.id
										LEFT JOIN ref_country d on a.ref_country_id=d.id
										WHERE a.id=$id_lit");

		$rs = $ambil->fetch_assoc();
		$dosenlist = $this->koneksi->query("SELECT a.dosen_id, b.nama
									FROM penulis_penelitian a 
									inner join dosen b on a.dosen_id=b.id
									WHERE penelitian_id={$rs['id_lit']}");
		$dosen = [];
		$dosenIds = [];
		while ($data_array = $dosenlist->fetch_assoc())
		{
			$dosen[] = $data_array['nama'];
			$dosenIds[] = $data_array['dosen_id'];
		}
		
		$rs['id_penulis'] = $dosenIds;
		$dosen = implode(', ',$dosen);
		$rs['nama_penulis'] = $dosen;

		return $rs;
	}

	function detail_lit_aut($id_lit)
	{
		$ambil= $this->koneksi->query("SELECT author, COUNT(*) as 'total' FROM penyakit GROUP BY author");

		return $ambil->fetch_assoc();
	}

	function hapus_lit($id_lit)
	{
		$this->koneksi->query("DELETE FROM penelitian WHERE id='$id_lit' ");
	}
}

class pkm
{
	//membuat atribut untuk koneksi (nyambung) ke database
	function __construct($database)
	{
		//mengisi atribut koneksi dengan persyaratan koneksi databaase
		$this->koneksi = $database;
	}

	//membuat fungsi untuk menampilkan data penyakit
	function tampil_pkm()
	{
		//mengambil data dari tabel penyakit
		$ambil = $this->koneksi->query("SELECT a.id as id_pkm,a.judul_penelitian, b.nama as nama_penulis
					FROM penelitian a
					INNER JOIN dosen b on a.dosen_id=b.id
					WHERE `type`='pkm'
					ORDER by a.updated_by desc
					");

		// outputkan semuadata karena datanya akan dipakai oleh file lain
		return $ambil->fetch_all(MYSQLI_ASSOC);
	}

	function update_jml_pkm($nama_penulis)
	{

		$this->koneksi->query("UPDATE data_pasien SET jml_lit = jml_lit + 1 WHERE nama_dosen = '$nama_penulis'");
		// $this->koneksi->query("UPDATE data_pasien dosen join  prodi p on p.nama_prodi = dosen.prodi SET p.jml_lit_prodi = p.jml_lit_prodi + 1 WHERE dosen.nama_dosen = '$nama_penulis'");
		// $this->koneksi->query("UPDATE data_pasien dosen join  prodi p on p.nama_prodi = dosen.prodi SET p.jml_lit_fakultas = p.jml_lit_fakultas + 1 WHERE dosen.nama_dosen = '$nama_penulis'");

	}

	function update_jml_author_pkm($nama_penulis)
	{

		$this->koneksi->query("UPDATE data_pasien SET jml_author = jml_author + 1 WHERE nama_dosen = '$nama_penulis'");
		

	}

	function update_jml_jurnal_pkm($nama_penulis)
	{

		$this->koneksi->query("UPDATE data_pasien SET jml_jurnal = jml_jurnal + 1 WHERE nama_dosen = '$nama_penulis'");
		
	}



	// function simpan_lit($nama_penulis,$judul_penelitian,$author,$jurnal,$publiser,$issn,$terbit,$negara,$url)
	// {
	// 	//$nama_tahun=$tahun_penyakit['name'];
	// 	//$waktu=date("y-m-d-h-i-s");
	// 	//$rename= $waktu."_".$nama_tahun;

	// 	//$lokasi = $tahun_penyakit['tmp_name'];
	// 	//move_uploaded_file($lokasi, "../assets/img/penyakit/$rename");

	// 	$this->koneksi->query("INSERT INTO penyakit(nama_penulis,judul_penelitian,author,jurnal,publiser,issn,terbit,negara,url) VALUES ('$nama_penulis','$judul_penelitian','$author','$jurnal','$publiser','$issn','$terbit','$negara','$url')");
	// }

	function get_nama_dosen_pkm($id_dosen)
	{
		$ambil= $this->koneksi->query("SELECT * FROM data_pasien where id_dosen= '$id_dosen'");

		return $ambil->fetch_assoc();


		
	}

	function update_jml_pkm_prodi($nama_prodi)
	{
		$ambil= $this->koneksi->query("UPDATE prodi SET jml_lit_prodi = jml_lit_prodi + 1  where nama_prodi='$nama_prodi'");

	}

	function update_jml_pkm_author($nama_prodi)
	{
		$ambil= $this->koneksi->query("UPDATE prodi SET jml_author_prodi = jml_author_prodi + 1  where nama_prodi='$nama_prodi'");

	}

	function update_jml_pkm_jurnal($nama_prodi)
	{
		$ambil= $this->koneksi->query("UPDATE prodi SET jml_jurnal_prodi = jml_jurnal_prodi + 1  where nama_prodi='$nama_prodi'");

	}



	function update_jml_pkm_fakultas($nama_fakultas)
	{
		$ambil= $this->koneksi->query("UPDATE fakultas SET jml_lit_fakultas = jml_lit_fakultas + 1 where nama_fakultas= '$nama_fakultas'");

	}

	function detail_pkm($id_pkm)
	{
		$ambil= $this->koneksi->query("SELECT a.id as id_pkm,a.dosen_id,b.nama as nama_penulis,a.judul_penelitian,a.author,a.tahun,
						case when a.jurnal='national_akreditasi' then 'National (Akreditasi)'
							when a.jurnal='national_nonakreditasi' then 'National (Non Akreditasi)'
							when a.jurnal='inter_reputasi' then 'International (Bereputasi)'
							when a.jurnal='inter_nonreputasi' then 'International (Non Reputasi)'
							else 'Tidak Ada' end as jurnal,a.jurnal as ref_jurnal,punya_google_scholar
						,a.ref_publisher_id,a.other_publisher,
						case when a.ref_publisher_id=17 then other_publisher else c.name end as publiser,
						a.issn,a.tanggal_terbit as terbit,d.name as negara,a.ref_country_id,a.google_scholar_link as `url`
					FROM penelitian a
					INNER JOIN dosen b on a.dosen_id=b.id 
					INNER JOIN ref_publisher c on a.ref_publisher_id=c.id
					INNER JOIN ref_country d on a.ref_country_id=d.id
					WHERE a.id=$id_pkm
					");

		return $ambil->fetch_assoc();
	}

	function hapus_pkm($id_pkm)
	{
		$this->koneksi->query("DELETE FROM penelitian WHERE id='$id_pkm' ");
	}

	function ubah_pkm($nama_penulis,$judul_penelitian,$author,$jurnal,$publiser,$issn,$terbit,$negara,$url,$id_lit)

	{
			//mengubah data di DB
		$this->koneksi->query("UPDATE pkm SET nama_penulis='$nama_penulis', judul_penelitian='$judul_penelitian', author='$author', jurnal='$jurnal', publiser='$publiser', issn='$issn', terbit='$terbit', negara='$negara', url='$url' WHERE id_lit='$id_lit'");	
	}


}
	

class pimpinan
{
	//membuat atribut untuk koneksi (nyambung) ke database
	function __construct($database)
	{
		//mengisi atribut koneksi dengan persyaratan koneksi databaase
		$this->koneksi = $database;
	}

	//membuat fungsi untuk menampilkan data pimpinan
	function tampil_pimpinan()
	{
		//setiap berhubungan dengan database menggunakan $this->koneksi

		//mengambil data dari tabel pimpinan
		$ambil = $this->koneksi->query("SELECT a.id as id_pimpinan, a.name as nama_pimpinan, b.name as jabatan, c.name as pangkat,a.image_path as foto_pimpinan,nomor
					FROM pimpinan a 
					INNER JOIN ref_jabatan b on a.ref_jabatan_id=b.id
					INNER JOIN ref_pangkat c on a.ref_pangkat_id=c.id
				");
		$semuadata = [];
		//data yang terambil diubah ke bentuk array dan diperulangan karena datanya lebih dari 1
		while ($data_array = $ambil->fetch_assoc())
		{
			//menampung data tiap perulangan ke dalam array multi
			$semuadata[] = $data_array;
		}

		// outputkan semuadata karena datanya akan dipakai oleh file lain
		return $semuadata;
	}

	function simpan_pimpinan($nama_pimpinan, $jabatan,$pangkat,$nomor,$tahun_pimpinan)
	{
		$nama_tahun=$tahun_pimpinan['name'];
		$waktu=date("y-m-d-h-i-s");
		$rename= $waktu."_".$nama_tahun;

		$lokasi = $tahun_pimpinan['tmp_name'];
		move_uploaded_file($lokasi, "../assets/img/pimpinan/$rename");
		$this->koneksi->query("INSERT INTO pimpinan(nama_pimpinan, jabatan, pangkat, nomor, tahun_pimpinan) VALUES ('$nama_pimpinan','$jabatan','$pangkat','$nomor','$rename')");
	}
	
	function detail_pimpinan($id)
	{
		$ambil= $this->koneksi->query("SELECT a.image_path as foto_pimpinan,a.nomor, a.name as nama_pimpinan, b.name as jabatan, c.name as pangkat,a.ref_pangkat_id,a.ref_jabatan_id
				FROM pimpinan a 
				INNER JOIN ref_jabatan b on a.ref_jabatan_id=b.id
				INNER JOIN ref_pangkat c on a.ref_pangkat_id=c.id
				WHERE a.id=$id");
		
		return $ambil->fetch_assoc();
	}

	function hapus_pimpinan($id_pimpinan)
	{
		$this->koneksi->query("DELETE FROM pimpinan WHERE id_pimpinan='$id_pimpinan' ");
	}

	function ubah_pimpinan($nama_pimpinan,$pangkat,$jabatan,$nomor,$tahun_pimpinan,$id_pimpinan)
	{
		$nama_tahun = $tahun_pimpinan ['name'];
		$waktu = date("y-m-d-h-i-s");
		$rename = $waktu."_".$nama_tahun;
		$lokasi = $tahun_pimpinan['tmp_name'];
	
		//jika tidak kosong $lokasi, artinya tahun juga diubah
		if (!empty($lokasi))
		{
			// saat mengubah tahun, maka data lama harus diubah. caranya
			//1. mengambil data lama
			$detail_pimpinan = $this->detail_pimpinan($id_pimpinan);
			//2. mengambil tahun lamanya aja
			$tahun_lama = $detail_pimpinan['tahun_pimpinan'];
			//3. jika tahun lama ada di folder img, maka dihapus
			if(file_exists("../assets/img/pimpinan/$tahun_lama"))
			{
				unlink("../assets/img/pimpinan/$tahun_lama");
			}

			//setelah tahun lama dihapus, lalu kita mengupload tahun baru
			move_uploaded_file($lokasi, "../assets/img/pimpinan/$rename");

			//mengubah data di DB
			$this->koneksi->query("UPDATE pimpinan SET `name`='$nama_pimpinan', ref_pangkat_id='$pangkat', ref_jabatan_id='$jabatan',nomor='$nomor',image_path='$rename' WHERE id='$id_pimpinan'");
		}
		else
		{
			$this->koneksi->query("UPDATE pimpinan SET `name`='$nama_pimpinan', ref_pangkat_id='$pangkat', ref_jabatan_id='$jabatan', nomor='$nomor' WHERE id='$id_pimpinan'");
		} 
	}
}

class staff
{
	//membuat atribut untuk koneksi (nyambung) ke database
	function __construct($database,$userId)
	{
		//mengisi atribut koneksi dengan persyaratan koneksi databaase
		$this->koneksi = $database;

		$this->userId = $userId;
	}

	//membuat fungsi untuk menampilkan data pimpinan
	function tampil_staff()
	{
		//setiap berhubungan dengan database menggunakan $this->koneksi

		//mengambil data dari tabel pimpinan
		$stmt = $this->koneksi->query("SELECT a.id as id_staff, a.name as nama_staff, b.name as jabatan_staff, c.name as pangkat_staff
						FROM staff a 
						INNER JOIN ref_jabatan b on a.ref_jabatan_id=b.id
						INNER JOIN ref_pangkat c on a.ref_pangkat_id=c.id
						order by a.updated_at desc");

		// outputkan semuadata karena datanya akan dipakai oleh file lain
		return $stmt->fetch_all(MYSQLI_ASSOC);
	}

	function simpan_staff($nama_staff,$pangkat_staff,$jabatan_staff)
	{
		try{
			$query = "INSERT INTO staff(`name`, ref_jabatan_id, ref_pangkat_id, created_by) VALUES ('$nama_staff','$jabatan_staff','$pangkat_staff',$this->userId)";
			
			$this->koneksi->query($query);

			// if ($this->koneksi->query($sql) === TRUE) {
			// 	Helper::dump("mantap");
			// 	echo "New record created successfully";
			// } else {
			// 	Helper::dump("Error: " . $sql . "<br>" . $this->koneksi->error);
			// 	// echo "Error: " . $sql . "<br>" . $conn->error;
			// }

		}catch (mysqli_sql_exception $e) {
			Helper::dump($e);
			throw new \MySQLiQueryException($this->koneksi, $e->getMessage(), $e->getCode());
		}catch (\ErrorException $e) {
            Helper::dump($e);

            return false;
        } 
	}

	function hapus_staff($id_staff)
	{
		$this->koneksi->query("DELETE FROM staff WHERE id='$id_staff' ");
	}

	function ubah_staff($nama_staff,$pangkat_staff,$jabatan_staff,$id_staff)
		{
			//mengubah data di DB
			$this->koneksi->query("UPDATE staff SET `name`='$nama_staff', ref_pangkat_id='$pangkat_staff', ref_jabatan_id='$jabatan_staff',updated_by=$this->userId WHERE id='$id_staff'");
		}

	function detail_staff($id_staff)
	{
		$ambil= $this->koneksi->query("SELECT a.name as nama_staff, a.ref_pangkat_id,a.ref_jabatan_id FROM staff a WHERE id=$id_staff ");

		return $ambil->fetch_assoc();
	}
}


class kerma_dl
{
	//membuat atribut untuk koneksi (nyambung) ke database
	function __construct($database,$userId)
	{
		//mengisi atribut koneksi dengan persyaratan koneksi databaase
		$this->koneksi = $database;
		$this->userId = $userId;
	}

	//membuat fungsi untuk menampilkan data pimpinan
	function tampil_kermadl()
	{
		//setiap berhubungan dengan database menggunakan $this->koneksi

		//mengambil data dari tabel pimpinan
		$ambil = $this->koneksi->query("SELECT id as id_kerma, nama as nama_kerma, bidang as bidang_kerma, tanggal FROM kerma where type='dn' order by updated_at desc");
		$semuadata = [];
		//data yang terambil diubah ke bentuk array dan diperulangan karena datanya lebih dari 1
		while ($data_array = $ambil->fetch_assoc())
		{
			//menampung data tiap perulangan ke dalam array multi
			$semuadata[] = $data_array;
		}

		// outputkan semuadata karena datanya akan dipakai oleh file lain
		return $semuadata;
	}

	function simpan_kermadl($nama_kerma, $bidang_kerma,$tanggal)
	{
		$query = "INSERT INTO kerma(nama, bidang, tanggal,type,created_by) VALUES ('$nama_kerma','$bidang_kerma','$tanggal','dn',$this->userId)";
		
		$this->koneksi->query($query);
	}

	
	
	function hapus_kermadl($id_kerma)
	{
		$this->koneksi->query("DELETE FROM kerma WHERE id='$id_kerma' ");
	}

	function ubah_kermadl($nama_kerma,$bidang_kerma,$tanggal,$id_kerma)
	{
		//mengubah data di DB
		$this->koneksi->query("UPDATE kerma SET nama='$nama_kerma', bidang='$bidang_kerma', tanggal='$tanggal', updated_by=$this->userId WHERE id='$id_kerma'");
	}

	function detail_kermadl($id_kerma)
	{
		$ambil= $this->koneksi->query("SELECT id as id_kerma, nama as nama_kerma, bidang as bidang_kerma,tanggal FROM kerma WHERE id='$id_kerma' ");

		return $ambil->fetch_assoc();
	}
}

class kerma_ln
{
	//membuat atribut untuk koneksi (nyambung) ke database
	function __construct($database,$userId)
	{
		//mengisi atribut koneksi dengan persyaratan koneksi databaase
		$this->koneksi = $database;
		$this->userId = $userId;
	}

	//membuat fungsi untuk menampilkan data pimpinan
	function tampil_kermaln()
	{
		//setiap berhubungan dengan database menggunakan $this->koneksi

		//mengambil data dari tabel pimpinan
		$ambil = $this->koneksi->query("SELECT id as id_kermaln,nama as nama_kerma, bidang as bidang_kerma,tanggal FROM kerma where type='ln' order by updated_by desc");

		//data yang terambil diubah ke bentuk array dan diperulangan karena datanya lebih dari 1
		$semuadata = [];
		while ($data_array = $ambil->fetch_assoc())
		{
			//menampung data tiap perulangan ke dalam array multi
			$semuadata[] = $data_array;
		}

		// outputkan semuadata karena datanya akan dipakai oleh file lain
		return $semuadata;
	}

	function simpan_kermaln($nama_kerma, $bidang_kerma,$tanggal)
	{
		$this->koneksi->query("INSERT INTO kerma(nama, bidang, tanggal,type, created_by) VALUES ('$nama_kerma','$bidang_kerma','$tanggal','ln',$this->userId)");
	}
	
	function hapus_kermaln($id_kermaln)
	{
		$this->koneksi->query("DELETE FROM kerma WHERE id='$id_kermaln' ");
	}

	function ubah_kermaln($nama_kerma,$bidang_kerma,$tanggal,$id_kermaln)
	{
		//mengubah data di DB
		$query = "UPDATE kerma SET nama='$nama_kerma', bidang='$bidang_kerma', tanggal='$tanggal', updated_by=$this->userId WHERE id='$id_kermaln'";
		$this->koneksi->query($query);
	}

	function detail_kermaln($id_kermaln)
	{
		$ambil= $this->koneksi->query("SELECT id as id_kermaln,nama as nama_kerma, bidang as bidang_kerma,tanggal FROM kerma WHERE id='$id_kermaln' ");

		return $ambil->fetch_assoc();
	}
}

class laporan_dosen
{
	function __construct($database)
	{
		
		$this->koneksi = $database;
	}
	function tampil_laporan_dosen()

	{
		$semuadata = array();
		$ambil = $this->koneksi->query("SELECT * FROM laporan_dosen
			JOIN data_pasien ON laporan_dosen.id_dosen = data_pasien.id_dosen
			JOIN penyakit ON laporan_dosen.id_lit = penyakit.id_lit
			ORDER BY id_laporan_dosen DESC
			") or die(mysqli_error($this->koneksi));
		while ($data_array = $ambil->fetch_assoc())
		{
			$semuadata[] = $data_array;
		}
		return $semuadata;
	}
	function detail_laporan_dosen()
	{
		$ambil= $this->koneksi->query("SELECT*FROM laporan_dosen WHERE id_laporan_dosen='$id_laporan_dosen' ");

		return $ambil->fetch_assoc();
	}
}

class prodi
{
	function __construct($database)
	{
		$this->koneksi = $database;
	}
	function tampil_prodi()
	{
		$stmt = $this->koneksi->query("SELECT a.id as id_prodi, a.nama as nama_prodi,c.nama as nama_fakultas,
					(select count(id) from penelitian where ref_prodi_id=id_prodi and type='general') as jml_lit_prodi,
					(select count(distinct xx.dosen_id) 
						from penelitian x 
						inner join penulis_penelitian xx on x.id=xx.penelitian_id
						where type='general' and x.ref_prodi_id=id_prodi
					) as jml_dosen,
					(select count(id) from penelitian where ref_prodi_id=id_prodi and type='general' and jurnal<>'none') as jml_publish_prodi
				FROM ref_prodi a 
				INNER JOIN ref_fakultas c on a.ref_fakultas_id=c.id
				order by a.nama asc");
		
		$rs = $stmt->fetch_all(MYSQLI_ASSOC);
		
		return $rs;
	}

	function get_fakultas()
	{
		$stmt = $this->koneksi->query("SELECT a.id as id_fakultas,a.nama as nama_fakultas,
					(select count(distinct xx.dosen_id) 
						from penelitian x 
						inner join penulis_penelitian xx on x.id=xx.penelitian_id
						where type='general' and x.ref_fakultas_id=id_fakultas) as jml_dosen,
					(select count(id) from penelitian where ref_fakultas_id=id_fakultas and type='general') as jml_lit_fakultas,
					(select count(id) from penelitian where ref_fakultas_id=id_fakultas and type='general' and jurnal<>'none') as jml_publish
				FROM ref_fakultas a
		");
		$rs = $stmt->fetch_all(MYSQLI_ASSOC);
		
		return $rs;
	}

	function getMenuProdi()
	{
		$stmt = $this->koneksi->query("SELECT a.id,a.nama FROM ref_fakultas a order by nama");
		$rs = $stmt->fetch_all(MYSQLI_ASSOC);
		foreach($rs as $k => $v){
			$stmtProdi = $this->koneksi->query("SELECT a.id,a.nama FROM ref_prodi a where a.ref_fakultas_id=".$v['id']." order by nama");
			$rsProdi = $stmtProdi->fetch_all(MYSQLI_ASSOC);
			$rs[$k]['is_parent'] = 0;
			if(count($rsProdi)>0){
				$rs[$k]['is_parent'] = 1;
				$rs[$k]['child'] = $rsProdi;
			}
		}
		
		return $rs;
	}

	function get_name_fakultas($id)
	{
		$ambil = $this->koneksi->query("SELECT a.nama as nama_fakultas,
				(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id and author='tim') as jml_author_tim,
				(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id and author='mandiri') as jml_author_mandiri,
				(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id and author='mahasiswa') as jml_author_mahasiswa,
				(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id and jurnal='national_akreditasi') as jml_jurnal_nas_akr,
				(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id and jurnal='national_nonakreditasi') as jml_jurnal_nas_nonakr,
				(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id and jurnal='inter_reputasi') as jml_jurnal_inter_rep,
				(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id and jurnal='inter_nonreputasi') as jml_jurnal_inter_nonrep,
				(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id and punya_google_scholar='1') as jml_google,
				(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id and punya_google_scholar='0') as jml_nongoogle,
				(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id) as jml_lit,
				(select count(distinct x.dosen_id) from penelitian x where x.ref_fakultas_id=a.id) as jml_dosen
			FROM ref_fakultas a 
			WHERE a.id='$id'
		");

		return $ambil->fetch_assoc();
	}

	function get_name_prodi($id_prodi)
	{
		$ambil= $this->koneksi->query("SELECT * FROM prodi where id_prodi= '$id_prodi'");

		return $ambil->fetch_assoc();
	}

	function tampil_prodi_fakultas()
	{
		$ambil = $this->koneksi->query("SELECT distinct nama_fakultas, jml_lit_fakultas FROM prodi");
		$semuadata = [];
		while ($data_array = $ambil->fetch_assoc()) 
		{
			$semuadata[] = $data_array;
		}
		return $semuadata;
	}
	function jml_lit($nama_fakultas)
	{
		$ambil = $this->koneksi->query("SELECT jml_lit FROM prodi where name_fakultas = '$nama_fakultas'");
		$semuadata = [];
		while ($data_array = $ambil->fetch_assoc()) 
		{
			$semuadata[] = $data_array;
		}
		return $semuadata;
	}
	function detail_laporan_dosen()
	{
		$ambil= $this->koneksi->query("SELECT*FROM prodi WHERE id_prodi='$id_prodi' ");

		return $ambil->fetch_assoc();
	}

	function detail_prodi($id_prodi)
	{
		$ambil = $this->koneksi->query("SELECT a.nama as nama_prodi,b.nama as nama_fakultas,
				(select count(id) from penelitian where `type`='general' and ref_prodi_id=a.id) as jml_penelitian,
				(select count(id) from penelitian where `type`='general' and ref_prodi_id=a.id and jurnal<>'none') as jml_publikasi,
				(select count(distinct xx.dosen_id) 
					from penelitian x 
					inner join penulis_penelitian xx on x.id=xx.penelitian_id
					where type='general' and x.ref_prodi_id=a.id
				) as jml_dosen,
				(select count(id) from penelitian where `type`='general' and ref_prodi_id=a.id and author='tim') as jml_tim,
				(select count(id) from penelitian where `type`='general' and ref_prodi_id=a.id and author='mandiri') as jml_mandiri,
				(select count(id) from penelitian where `type`='general' and ref_prodi_id=a.id and author='mahasiswa') as jml_mahasiswa,
				(select count(id) from penelitian where `type`='general' and ref_prodi_id=a.id and jurnal='national_akreditasi') as jml_jurnal_nas_akr,
				(select count(id) from penelitian where `type`='general' and ref_prodi_id=a.id and jurnal='national_nonakreditasi') as jml_jurnal_nas_nonakr,
				(select count(id) from penelitian where `type`='general' and ref_prodi_id=a.id and jurnal='inter_reputasi') as jml_jurnal_inter_rep,
				(select count(id) from penelitian where `type`='general' and ref_prodi_id=a.id and jurnal='inter_nonreputasi') as jml_jurnal_inter_nonrep,
				(select count(id) from penelitian where `type`='general' and ref_prodi_id=a.id and punya_google_scholar='1') as jml_google,
				(select count(id) from penelitian where `type`='general' and ref_prodi_id=a.id and punya_google_scholar='0') as jml_nongoogle,
				(select count(id) from penelitian where `type`='general' and ref_prodi_id=a.id) as jml_lit_prodi
			FROM ref_prodi a 
			INNER JOIN ref_fakultas b on a.ref_fakultas_id=b.id
			WHERE a.id='$id_prodi'
		");

		$rsPublish = $ambil->fetch_assoc();
		
		$data = [
			[
				'label' => $rsPublish['nama_prodi'],
				'total' => '-',
			],
			[
				'label' => $rsPublish['nama_fakultas'],
				'total' => '-',
			],
			[
				'label' => '__________________________________________________',
				'total' => '-',
			],
			[
				'label' => 'JUMLAH PENELITIAN',
				'total' => $rsPublish['jml_penelitian'],
			],
			[
				'label' => 'JUMLAH PUBLIKASI ARTIKEL',
				'total' => $rsPublish['jml_publikasi'],
			],
			[
				'label' => 'JUMLAH DOSEN',
				'total' => $rsPublish['jml_dosen']
			],
			[
				'label' => '__________________________________________________',
				'total' => '-',
			],
			[
				'label' => 'JUMLAH JURNAL NASIONAL AKREDITASI',
				'total' => $rsPublish['jml_jurnal_nas_akr'],
			],
			[
				'label' => 'JUMLAH JURNAL NASIONAL NON AKREDITASI',
				'total' => $rsPublish['jml_jurnal_nas_nonakr'],
			],
			[
				'label' => 'JUMLAH JURNAL INTERNATIONAL REPUTASI',
				'total' => $rsPublish['jml_jurnal_inter_rep'],
			],
			[
				'label' => 'JUMLAH JURNAL INTERNATIONAL NON REPUTASI',
				'total' => $rsPublish['jml_jurnal_inter_nonrep'],
			],
			[
				'label' => 'AUTHOR / CORRESPONDING AUTHOR MANDIRI',
				'total' => $rsPublish['jml_mandiri']
	
			],
			[
				'label' => 'AUTHOR / CORRESPONDING AUTHOR TIM',
				'total' => $rsPublish['jml_tim']
	
			],
			[
				'label' => 'AUTHOR / CORRESPONDING AUTHOR MAHASISWA',
				'total' => $rsPublish['jml_mahasiswa']
	
			],
			[
				'label' => 'MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'total' => $rsPublish['jml_google'],
			],
			[
				'label' => 'TIDAK MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'total' => $rsPublish['jml_nongoogle'],
			],
		];

		return $data;
	}
}

class rekapitulasi
{
	function __construct($database)
	{
		$this->koneksi = $database;
	}
	function tampil_rekapitulasi()
	{
		$ambil = $this->koneksi->query("SELECT * FROM rekapitulasi");
		$semuadata = [];
		while ($data_array = $ambil->fetch_assoc()) 
		{
			$semuadata[] = $data_array;
		}
		return $semuadata;
	}
	function detail_rekapitulasi()
	{
		$ambil= $this->koneksi->query("SELECT*FROM rekapitulasi WHERE id_rekapitulasi='$id_rekapitulasi' ");

		return $ambil->fetch_assoc();
	}

	function getFIlterTahun()
	{
		$stmt= $this->koneksi->query("SELECT distinct tahun as id, tahun as `name` FROM penelitian order by tahun desc");

		return $stmt->fetch_all(MYSQLI_ASSOC);
	}

	function getRekapitulasi($tahun='all')
	{
		$andTahun = "";
		if($tahun<>'all'){
			$andTahun = " and tahun='$tahun'";
		}

		$query = "SELECT 
					(select count(id) from penelitian where `type`='general' $andTahun) as jml_penelitian,
					(select count(id) from penelitian where `type`='general' and jurnal<>'none' $andTahun) as jml_publikasi,
					(select count(distinct xx.dosen_id) 
						from penelitian x 
						inner join penulis_penelitian xx on x.id=xx.penelitian_id
						where type='general' $andTahun) as jml_dosen,
					(select count(id) from penelitian where `type`='general' and author='tim') as jml_tim,
					(select count(id) from penelitian where `type`='general' and author='mahasiswa') as jml_mahasiswa,
					(select count(id) from penelitian where `type`='general' and author='mandiri') as jml_mandiri,
					(select count(id) from penelitian where `type`='general' and jurnal='national_akreditasi' $andTahun) as jml_jurnal_nas_akr,
					(select count(id) from penelitian where `type`='general' and jurnal='national_nonakreditasi' $andTahun) as jml_jurnal_nas_nonakr,
					(select count(id) from penelitian where `type`='general' and jurnal='inter_reputasi' $andTahun) as jml_jurnal_inter_rep,
					(select count(id) from penelitian where `type`='general' and jurnal='inter_nonreputasi' $andTahun) as jml_jurnal_inter_nonrep,
					(select count(id) from penelitian where `type`='general' and punya_google_scholar='1' $andTahun) as jml_google,
					(select count(id) from penelitian where `type`='general' and punya_google_scholar='0' $andTahun) as jml_nongoogle";
		
		$stmtPublish = $this->koneksi->query($query);
		$rsPublish = $stmtPublish->fetch_assoc();
		
		$data = [
			[
				'label' => 'JUMLAH PENELITIAN',
				'total' => $rsPublish['jml_penelitian'],
			],
			[
				'label' => 'JUMLAH PUBLIKASI ARTIKEL',
				'total' => $rsPublish['jml_publikasi'],
			],
			[
				'label' => 'JUMLAH DOSEN',
				'total' => $rsPublish['jml_dosen']
			],
			[
				'label' => '__________________________________________________',
				'total' => '-',
			],
			[
				'label' => 'JUMLAH JURNAL NASIONAL AKREDITASI',
				'total' => $rsPublish['jml_jurnal_nas_akr'],
			],
			[
				'label' => 'JUMLAH JURNAL NASIONAL NON AKREDITASI',
				'total' => $rsPublish['jml_jurnal_nas_nonakr'],
			],
			[
				'label' => 'JUMLAH JURNAL INTERNATIONAL REPUTASI',
				'total' => $rsPublish['jml_jurnal_inter_rep'],
			],
			[
				'label' => 'JUMLAH JURNAL INTERNATIONAL NON REPUTASI',
				'total' => $rsPublish['jml_jurnal_inter_nonrep'],
			],
			[
				'label' => 'AUTHOR / CORRESPONDING AUTHOR MANDIRI',
				'total' => $rsPublish['jml_mandiri']
	
			],
			[
				'label' => 'AUTHOR / CORRESPONDING AUTHOR TIM',
				'total' => $rsPublish['jml_tim']
	
			],
			[
				'label' => 'AUTHOR / CORRESPONDING AUTHOR MAHASISWA',
				'total' => $rsPublish['jml_mahasiswa']
	
			],
			[
				'label' => 'MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'total' => $rsPublish['jml_google'],
			],
			[
				'label' => 'TIDAK MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'total' => $rsPublish['jml_nongoogle'],
			],
		];
		
		return $data;
	}

	function getDetailFakultas($id,$tahun='all')
	{
		$andTahun = "";
		if($tahun<>'all'){
			$andTahun = " and tahun='$tahun'";
		}

		$query = "SELECT a.nama as nama_fakultas,
					(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id $andTahun) as jml_penelitian,
					(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id $andTahun and jurnal<>'none') as jml_publikasi,
					(select count(distinct xx.dosen_id) 
						from penelitian x 
						inner join penulis_penelitian xx on x.id=xx.penelitian_id
						where type='general' and x.ref_fakultas_id=a.id $andTahun
					) as jml_dosen,
					(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id $andTahun and author='tim') as jml_tim,
					(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id $andTahun and author='mandiri') as jml_mandiri,
					(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id $andTahun and author='mahasiswa') as jml_mahasiswa,
					(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id $andTahun and jurnal='national_akreditasi') as jml_jurnal_nas_akr,
					(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id $andTahun and jurnal='national_nonakreditasi') as jml_jurnal_nas_nonakr,
					(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id $andTahun and jurnal='inter_reputasi') as jml_jurnal_inter_rep,
					(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id $andTahun and jurnal='inter_nonreputasi') as jml_jurnal_inter_nonrep,
					(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id $andTahun and punya_google_scholar='1') as jml_google,
					(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id $andTahun and punya_google_scholar='0') as jml_nongoogle,
					(select count(id) from penelitian where `type`='general' and ref_fakultas_id=a.id $andTahun) as jml_lit_prodi
				FROM ref_fakultas a 
				WHERE a.id='$id'
			";
		
		$stmtPublish = $this->koneksi->query($query);
		$rsPublish = $stmtPublish->fetch_assoc();
		
		$data = [
			[
				'label' => $rsPublish['nama_fakultas'],
				'total' => '-',
			],
			[
				'label' => '__________________________________________________',
				'total' => '-',
			],
			[
				'label' => 'JUMLAH PENELITIAN',
				'total' => $rsPublish['jml_penelitian'],
			],
			[
				'label' => 'JUMLAH PUBLIKASI ARTIKEL',
				'total' => $rsPublish['jml_publikasi'],
			],
			[
				'label' => 'JUMLAH DOSEN',
				'total' => $rsPublish['jml_dosen']
			],
			[
				'label' => '__________________________________________________',
				'total' => '-',
			],
			[
				'label' => 'JUMLAH JURNAL NASIONAL AKREDITASI',
				'total' => $rsPublish['jml_jurnal_nas_akr'],
			],
			[
				'label' => 'JUMLAH JURNAL NASIONAL NON AKREDITASI',
				'total' => $rsPublish['jml_jurnal_nas_nonakr'],
			],
			[
				'label' => 'JUMLAH JURNAL INTERNATIONAL REPUTASI',
				'total' => $rsPublish['jml_jurnal_inter_rep'],
			],
			[
				'label' => 'JUMLAH JURNAL INTERNATIONAL NON REPUTASI',
				'total' => $rsPublish['jml_jurnal_inter_nonrep'],
			],
			[
				'label' => 'AUTHOR / CORRESPONDING AUTHOR MANDIRI',
				'total' => $rsPublish['jml_mandiri']
	
			],
			[
				'label' => 'AUTHOR / CORRESPONDING AUTHOR TIM',
				'total' => $rsPublish['jml_tim']
	
			],
			[
				'label' => 'AUTHOR / CORRESPONDING AUTHOR MAHASISWA',
				'total' => $rsPublish['jml_mahasiswa']
	
			],
			[
				'label' => 'MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'total' => $rsPublish['jml_google'],
			],
			[
				'label' => 'TIDAK MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'total' => $rsPublish['jml_nongoogle'],
			],
		];
		
		return $data;
	}

	function getDetailProdi($id,$tahun='all')
	{
		$andTahun = "";
		if($tahun<>'all'){
			$andTahun = " and tahun='$tahun'";
		}

		$query = "SELECT 
					(select count(id) from penelitian where `type`='general' and ref_prodi_id=$id $andTahun) as jml_penelitian,
					(select count(id) from penelitian where `type`='general' and ref_prodi_id=$id and jurnal<>'none' $andTahun) as jml_publikasi,
					(select count(id) from penelitian where `type`='general' and ref_prodi_id=$id and jurnal='national_akreditasi' $andTahun) as jml_jurnal_nas_akr,
					(select count(id) from penelitian where `type`='general' and ref_prodi_id=$id and jurnal='national_nonakreditasi' $andTahun) as jml_jurnal_nas_nonakr,
					(select count(id) from penelitian where `type`='general' and ref_prodi_id=$id and jurnal='inter_reputasi' $andTahun) as jml_jurnal_inter_rep,
					(select count(id) from penelitian where `type`='general' and ref_prodi_id=$id and jurnal='inter_nonreputasi' $andTahun) as jml_jurnal_inter_nonrep,
					(select count(id) from penelitian where `type`='general' and ref_prodi_id=$id and punya_google_scholar='1' $andTahun) as jml_google,
					(select count(id) from penelitian where `type`='general' and ref_prodi_id=$id and punya_google_scholar='0' $andTahun) as jml_nongoogle";
		
		$stmtPublish = $this->koneksi->query($query);
		$rsPublish = $stmtPublish->fetch_assoc();
		
		$data = [
			[
				'label' => 'JUMLAH PENELITIAN',
				'total' => $rsPublish['jml_penelitian'],
			],
			[
				'label' => 'JUMLAH PUBLIKASI ARTIKEL',
				'total' => $rsPublish['jml_publikasi'],
			],
			[
				'label' => 'JUMLAH JURNAL NASIONAL AKREDITASI',
				'total' => $rsPublish['jml_jurnal_nas_akr'],
			],
			[
				'label' => 'JUMLAH JURNAL NASIONAL NON AKREDITASI',
				'total' => $rsPublish['jml_jurnal_nas_nonakr'],
			],
			[
				'label' => 'JUMLAH JURNAL INTERNATIONAL REPUTASI',
				'total' => $rsPublish['jml_jurnal_inter_rep'],
			],
			[
				'label' => 'JUMLAH JURNAL INTERNATIONAL NON REPUTASI',
				'total' => $rsPublish['jml_jurnal_inter_nonrep'],
			],
			[
				'label' => 'MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'total' => $rsPublish['jml_google'],
			],
			[
				'label' => 'TIDAK MEMPUNYAI AKUN GOOGLE SCHOLAR',
				'total' => $rsPublish['jml_nongoogle'],
			],
		];
		
		return $data;
	}
}


class gejala
{
	
	function __construct($database)
	{
		$this->koneksi = $database;
	}

	function tampil_gejala()
	{
		$ambil = $this->koneksi->query("SELECT * FROM gejala");
		$semuadata = [];
		while ($data_array = $ambil->fetch_assoc())
		{
			$semuadata[] = $data_array;
		}
		return $semuadata;
	}

	function simpan_gejala($nama_gejala)
	{
		$this->koneksi->query("INSERT INTO gejala(nama_gejala) VALUES ('$nama_gejala')");
	}

	function hapus_gejala($id_gejala)
	{
		$this->koneksi->query("DELETE FROM gejala WHERE id_gejala='$id_gejala' ");
	}	

	function detail_gejala($id_gejala)
	{
		$ambil= $this->koneksi->query("SELECT*FROM gejala WHERE id_gejala='$id_gejala' ");

		return $ambil->fetch_assoc();
	}
	function ubah_gejala ($nama_gejala,$id_gejala)
	{
		$this->koneksi->query("UPDATE gejala SET nama_gejala= '$nama_gejala' WHERE id_gejala= '$id_gejala' " );
	}

	
}

class obat
{
	
	function __construct($database)
	{
		$this->koneksi = $database;
	}

	function tampil_obat()
	{
		$ambil = $this->koneksi->query("SELECT * FROM obat");
		$semuadata = [];
		while ($data_array = $ambil->fetch_assoc())
		{
			$semuadata[] = $data_array;
		}
		return $semuadata;
	}
	function simpan_obat($nama_obat,$Pangkat,$Jabatan)
	{
		$this->koneksi->query("INSERT INTO obat(nama_obat, pangkat, jabatan) VALUES ('$nama_obat',$pangkat',$jabatan')");
	}
	function hapus_obat($id_obat)
	{
		$this->koneksi->query("DELETE FROM obat WHERE id_obat='$id_obat' ");
	}	
	function detail_obat($id_obat)
	{
		$ambil= $this->koneksi->query("SELECT*FROM obat WHERE id_obat='$id_obat' ");

		return $ambil->fetch_assoc();
	}
	
	function ubah_obat($id_obat)
	{
		$this->koneksi->query("UPDATE obat SET nomor= '$nomor' WHERE id_obat='$id_obat' " );
	}
}

class pengetahuan
{
	
	function __construct($database)
	{
		$this->koneksi=$database;
	}

	function tampil_pengetahuan()
	{
		$ambil = $this->koneksi->query("SELECT * FROM pengetahuan
			
			JOIN gejala ON pengetahuan.id_gejala=gejala.id_gejala
			JOIN penyakit ON pengetahuan.id_penyakit=penyakit.id_penyakit") or die(mysqli_error($this->koneksi));
		$semuadata = [];
		while ($data_array = $ambil->fetch_assoc())
		{
			$semuadata[] = $data_array;
		}
		return $semuadata;
	}

	function simpan_pengetahuan($id_penyakit,$id_gejala,  $mb, $md)
	{
		$this->koneksi->query("INSERT INTO pengetahuan (MB,MD, id_gejala,id_penyakit) 
			VALUES ('$mb','$md','$id_gejala','$id_penyakit')") or die(mysqli_error($this->koneksi));
	}
	function hapus_pengetahuan($id_pengetahuan)
	{

		$this->koneksi->query("DELETE FROM pengetahuan WHERE id_pengetahuan='$id_pengetahuan' ");
	}

	function ubah_pengetahuan( $id_penyakit,$id_gejala, $md,$mb, $id_pengetahuan)

	{
			//mengubah data di DB
		$this->koneksi->query("UPDATE pengetahuan SET MB='$mb', MD='$md', id_gejala='$id_gejala', id_penyakit='$id_penyakit'WHERE id_pengetahuan='$id_pengetahuan'");	
	}

	function detail_pengetahuan($id_pengetahuan)

	{
		$ambil= $this->koneksi->query("SELECT*FROM pengetahuan WHERE id_pengetahuan='$id_pengetahuan' ");

		return $ambil->fetch_assoc();

	}

	function cek_tambah_pengetahuan($id_penyakit,$id_gejala)
	{
		$ambil = $this->koneksi->query("SELECT * FROM pengetahuan WHERE id_penyakit='$id_penyakit' AND id_gejala='$id_gejala'");
		$hitung = $ambil->num_rows;

		if ($hitung > 0) 
		{
			return "ada";
		}
		else
		{
			return "belum  ada ";
		}
	}

	function cek_ubah_pengetahuan ($id_penyakit,$id_gejala,$id_pengetahuan)
	
	{
		$ambil = $this->koneksi->query("SELECT * FROM pengetahuan WHERE id_penyakit='$id_penyakit' AND id_gejala='$id_gejala' AND id_pengetahuan!='$id_pengetahuan'");
		$hitung = $ambil->num_rows;

		if ($hitung > 0) 
		{
			return "ada";
		}
		else
		{
			return "belum  ada ";
		}
	}

	function ambil_pengetahuan_gejala($id_gejala)
	{
		$semuadata=array();
		$ambil = $this->koneksi->query("SELECT * FROM pengetahuan WHERE id_gejala='$id_gejala' ");
		while ($data_array = $ambil->fetch_assoc()) 
		{
			$semuadata[] = $data_array ;
		}

		return $semuadata;
	}

	function ambil_pengetahuan_gejala_penyakit($id_gejala, $id_penyakit)
	{
		$ambil = $this->koneksi->query("SELECT * FROM pengetahuan WHERE id_gejala='$id_gejala' AND id_penyakit='$id_penyakit'");
		$pecah = $ambil->fetch_assoc();
		return $pecah;
	}
}

/**
* 
*/

class perhitungan extends pengetahuan
{
	public $koneksi;

	function __construct($database)
	{
		$this->koneksi = $database;
	}



	function hitung($data_gejala)
	{
		foreach ($data_gejala as $key => $value) 
		{
			$id_gejala = $value	;
			$pengetahuan_gejala = $this->ambil_pengetahuan_gejala($id_gejala);
			foreach ($pengetahuan_gejala as $key_pengetuahuan => $value_pengetuahuan) 
			{
				$penyakit_gejala[$value_pengetuahuan['id_penyakit']][]= $value_pengetuahuan['id_gejala'];
			}
		}

		foreach ($penyakit_gejala as $id_penyakit => $value_penyakit_gejala)
		{
			$jumlah_gejala_penyakit[$id_penyakit] = count($value_penyakit_gejala);
		}



			// Menghitung CF tiap gejala di penyakit
		foreach ($penyakit_gejala as $id_penyakit => $value_penyakit_gejala)
		{
			foreach ($value_penyakit_gejala as $key_penyakit_gejala => $id_gejala)
			{
				$pengetahuan_terambil = $this->ambil_pengetahuan_gejala_penyakit($id_gejala, $id_penyakit);
				$hitung[$id_penyakit][$id_gejala] = $pengetahuan_terambil['MB'] - $pengetahuan_terambil['MD'];
				
			}
		}


				// Mengubah id gejala menjadi key biasa
		foreach ($hitung as $id_penyakit => $isi_hitung)
		{
			foreach ($isi_hitung as $id_gejala => $nilai)
			{
				$hitung2[$id_penyakit][] = $nilai;
			}
		}



		foreach ($hitung2 as $id_penyakit => $isi_hitung)
		{
			foreach ($isi_hitung as $key_biasa => $nilai)
			{
				if($jumlah_gejala_penyakit[$id_penyakit]==1)
				{
					$cf[$id_penyakit] = $hitung2[$id_penyakit][$key_biasa];
				}
				else
				{

					// Menghitung CF kombinasi
					$cfkombinasi[$id_penyakit][1] = $this->rumus_cfkombinasi($hitung2[$id_penyakit][0], $hitung2[$id_penyakit][1]);
					$tampil_rumus[$id_penyakit][1] = $this->tampil_rumus($hitung2[$id_penyakit][0], $hitung2[$id_penyakit][1]);

					$no_kombinasi = 2;
					for($i=2;$i<=$jumlah_gejala_penyakit[$id_penyakit]-1;$i++)
					{
						$cfkombinasi[$id_penyakit][$no_kombinasi] = $this->rumus_cfkombinasi($cfkombinasi[$id_penyakit][$no_kombinasi-1], $hitung2[$id_penyakit][$i]);
						
						$tampil_rumus[$id_penyakit][$no_kombinasi] = $this->tampil_rumus($cfkombinasi[$id_penyakit][$no_kombinasi-1], $hitung2[$id_penyakit][$i]);


						$no_kombinasi+=1;

					}

					$cf[$id_penyakit] = end($cfkombinasi[$id_penyakit]);
				}
			}
		}


		$nilai_cf = max($cf);


		$data_akhir['jumlah_gejala_penyakit'] = $jumlah_gejala_penyakit;
		$data_akhir['hitung'] = $hitung;
		$data_akhir['hitung2'] = $hitung2;
		$data_akhir['cf_tiap_penyakit'] = $cf;
		$data_akhir['cf_kombinasi'] = $cfkombinasi;
		$data_akhir['tampil_rumus'] = $tampil_rumus;
		$data_akhir['nilai_cf'] = $nilai_cf;
		
		
		return $data_akhir;
	}


	function rumus_cfkombinasi($cfbaru, $cflama)
	{
		if($cfbaru > 0 AND $cflama > 0 )
		{
			$rumus = $cflama+$cfbaru*(1-$cflama);
		}
		elseif($cfbaru < 0 AND $cflama < 0 )
		{
			$rumus = $cflama+$cfbaru*(1+$cflama);
		}
		else
		{
			$rumus = ($cflama+$cfbaru)/(1-min($cflama,$cfbaru));
		}
		return $rumus;
	}

	function tampil_rumus($cfbaru, $cflama)
	{
		if($cfbaru > 0 AND $cflama > 0 )
		{
			// $rumus = "CFlama+CFbaru*(1-CFlama)";
			$rumus = $cflama." + ".$cfbaru." * ".'('."1"." - ".$cflama.')';
		}
		elseif($cfbaru < 0 AND $cflama < 0 )
		{
			$rumus = $cflama." + ".$cfbaru." * ".'('."1"."+".$cflama.')';
		}
		else
		{
			$rumus = "(".$cflama." + ".$cfbaru.")"." / "."(".'1'.' - '.'min'.'('.$cflama.','.$cfbaru.')'.')';
		}
		return $rumus;
	}
}

class admin
{
	
	function __construct($database)
	{
		$this->koneksi = $database;
	}

	function login_admin($username,$pass)
	{
		$ps = sha1($pass);
		$ambil = $this->koneksi->query("SELECT * FROM admin WHERE username_admin='$username' AND password_admin='$pass'");

		$hitung = $ambil->num_rows;
		if ($hitung == 1) 
		{
			$akun = $ambil->fetch_assoc();
			$_SESSION['admin'] = $akun;

			return "sukses";
		}
		else
		{

			$data =$this->koneksi->query ("SELECT * FROM data_pasien WHERE nik='$username' AND password_dosen ='$ps'");

			$hasil_cari = mysqli_num_rows($data);
			if ($hasil_cari == 1)
			{
				$data_mhs_lengkap =$data->fetch_assoc();
				$_SESSION['dosen'] =$data_mhs_lengkap;

				return "dosen";

			}

			else
			{
				return "gagal";
			}

		}

	}
}

class rekam_medis
{
	public $koneksi;

	function __construct($database)
	{
		$this->koneksi = $database;
	}

	function tampil_rekam_medis()
	{
		$semuadata = array();
		$ambil = $this->koneksi->query("SELECT * FROM rekam_medis 
			JOIN data_pasien ON rekam_medis.id_dosen = data_pasien.id_dosen
			JOIN penyakit ON rekam_medis.id_penyakit = penyakit.id_penyakit
			ORDER BY id_rekam_medis DESC
			") or die(mysqli_error($this->koneksi));
		while ($data_array = $ambil->fetch_assoc())
		{
			$semuadata[] = $data_array;
		}
		return $semuadata;
	}

	function tampil_rekam_medis_dosen($id_dosen)
	{
		$semuadata = array();
		$ambil = $this->koneksi->query("SELECT * FROM rekam_medis 
			JOIN data_pasien ON rekam_medis.id_dosen = data_pasien.id_dosen
			JOIN penyakit ON rekam_medis.id_penyakit = penyakit.id_penyakit
			WHERE rekam_medis.id_dosen='$id_dosen'
			ORDER BY id_rekam_medis DESC
			") or die(mysqli_error($this->koneksi));
		while ($data_array = $ambil->fetch_assoc())
		{
			$semuadata[] = $data_array;
		}
		return $semuadata;
	}

	function detail_rekam_medis($id_rekam_medis)
	{
		$ambil = $this->koneksi->query("SELECT * FROM rekam_medis 
			JOIN data_pasien ON rekam_medis.id_dosen = data_pasien.id_dosen
			JOIN penyakit ON rekam_medis.id_penyakit = penyakit.id_penyakit
			WHERE id_rekam_medis='$id_rekam_medis'
			") or die(mysqli_error($this->koneksi));
		return $ambil->fetch_assoc();
	}

	

	function ambil_gejala_dipilih($id_rekam_medis)
	{
		$semuadata = array();
		$ambil = $this->koneksi->query("SELECT * FROM gejala_dipilih 
			JOIN gejala ON gejala_dipilih.gejala_yg_dipilih = gejala.id_gejala
			WHERE id_rekam_medis='$id_rekam_medis'
			") or die(mysqli_error($this->koneksi));
		while ($data_array = $ambil->fetch_assoc())
		{
			$semuadata[] = $data_array;
		}
		return $semuadata;
	}


	function simpan_diagnosa($id_dosen, $id_penyakit, $cf)
	{
		$tgl = date("y-m-d");
		$this->koneksi->query("INSERT INTO rekam_medis (id_dosen, id_penyakit, cf_penyakit, tgl_rekam_medis) VALUES ('$id_dosen', '$id_penyakit', '$cf', '$tgl') ");
		return mysqli_insert_id($this->koneksi);
	}

	function simpan_gejala_dipilih($id_gejala, $id_rekam_medis)
	{
		$this->koneksi->query("INSERT INTO gejala_dipilih (id_rekam_medis, gejala_yg_dipilih) VALUES ('$id_rekam_medis', '$id_gejala' ) ") or die(mysqli_error($this->koneksi));
	}

	function simpan_penyakit_rekam_medis($id_penyakit, $cf_penyakit, $id_rekam_medis)
	{
		$this->koneksi->query("INSERT INTO penyakit_rekam_medis (id_rekam_medis, id_hasil_penyakit, nilai_cf_penyakit) VALUES ('$id_rekam_medis', '$id_penyakit', '$cf_penyakit' ) ") or die(mysqli_error($this->koneksi));
	}
}

class BantuanAkademik
{
	//membuat atribut untuk koneksi (nyambung) ke database
	function __construct($database,$userId)
	{
		//mengisi atribut koneksi dengan persyaratan koneksi databaase
		$this->koneksi = $database;
		$this->userId = $userId;
	}

	function tampil_bantuan_akademik()
	{
		$query = "SELECT a.id as id_bantuan, a.nama, a.judul_penelitian, b.nama as prodi, c.nama as fakultas, a.period as periode,a.tanggal,a.tahun_lulus,a.jumlah_dana as jumlah
					FROM bantuan_akademik a
					INNER JOIN ref_prodi b on a.ref_prodi_id=b.id
					INNER JOIN ref_fakultas c on a.ref_fakultas_id=c.id";
	
		$stmt = $this->koneksi->query($query);
		$rs = $stmt->fetch_all(MYSQLI_ASSOC);

		foreach($rs as $k => $v){
			$rs[$k]['jumlah'] = Helper::FormatRp($v['jumlah']);
		}

		return $rs;
	}

	function detail_bantuan_akademik($id)
	{
		$query = "SELECT a.id as id_bantuan, a.nama, a.judul_penelitian, b.nama as prodi, c.nama as fakultas, a.period as periode,a.tanggal,a.tahun_lulus,a.jumlah_dana as jumlah,
					a.jumlah_dana,a.ref_prodi_id,a.ref_fakultas_id
					FROM bantuan_akademik a
					INNER JOIN ref_prodi b on a.ref_prodi_id=b.id
					INNER JOIN ref_fakultas c on a.ref_fakultas_id=c.id
					WHERE a.id=$id
					";
	
		$stmt = $this->koneksi->query($query);
		$rs = $stmt->fetch_assoc();
		$rs['jumlah'] = Helper::FormatRp($rs['jumlah']);

		return $rs;
	}

	function simpan_bantuan_akademik($nama,$judul,$prodi,$fakultas,$period,$tanggal,$tahunLulus,$jumlah)
	{
		$query = "INSERT INTO bantuan_akademik (nama,judul_penelitian,ref_prodi_id,ref_fakultas_id,period,tanggal,tahun_lulus,jumlah_dana,created_by)
					values ('$nama','$judul','$prodi','$fakultas','$period','$tanggal','$tahunLulus','$jumlah',$this->userId)";
		
		$stmt = $this->koneksi->query($query);
	}
	
	function ubah_bantuin_akademik($nama,$judul,$prodi,$fakultas,$period,$tanggal,$tahunLulus,$jumlah,$id)
	{
		$query = "UPDATE bantuan_akademik SET 
					nama='$nama',
					judul_penelitian='$judul',
					ref_prodi_id=$prodi,
					ref_fakultas_id=$fakultas,
					period='$period',
					tanggal='$tanggal',
					tahun_lulus='$tahunLulus',
					jumlah_dana='$jumlah',
					updated_by = $this->userId
					WHERE id=$id";
		
		$stmt = $this->koneksi->query($query);
	}

	function hapus_bantuan($id)
	{
		$query = "DELETE FROM bantuan_akademik 
					WHERE id=$id";
	
		$stmt = $this->koneksi->query($query);
	}
}

class Auth
{
	//membuat atribut untuk koneksi (nyambung) ke database
	function __construct($database)
	{
		//mengisi atribut koneksi dengan persyaratan koneksi databaase
		$this->koneksi = $database;
	}

	function login($email)
	{
		$query = "SELECT id, `password`,`type` FROM users WHERE email='$email' OR username='$email'";
		$stmt = $this->koneksi->query($query);

		return $stmt->fetch_assoc();
	}
}

$rekam_medis = new rekam_medis($database);
//menjadilan class dosen sbgai objek
$laporan_dosen = new laporan_dosen($database);
$prodi = new prodi($database);
$rekapitulasi = new rekapitulasi($database);
$dosen = new dosen($database,$userId);
$pkm = new pkm($database);
$lit = new lit($database,$userId);
$staff = new staff($database,$userId);
$gejala = new gejala($database);
$kerma_dl = new kerma_dl($database,$userId);
$kerma_ln = new kerma_ln($database,$userId);
$pimpinan = new pimpinan($database);
$obat = new obat($database);
$pengetahuan = new pengetahuan($database);
$admin = new admin($database);
$perhitungan = new perhitungan($database);
$rekam_medis = new rekam_medis($database);
$refData = new RefData($database);
$grafik = new Grafik($database);
$bantuan_akademik = new BantuanAkademik($database,$userId);
$auth = new Auth($database);
?>

