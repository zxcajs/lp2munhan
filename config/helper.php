<?php 

class Helper
{
	function __construct()
	{
		//
	}

	public static function dump($param)
	{
		echo "<pre>";
        print_r($param);
        echo "</pre>";
        exit;
	}

	public static function FormatRp($bil)
    {
        return number_format($bil, 0, '', '.');
    }
	
	public static function validate($data)
	{
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
 
        return $data;
    }

	public static function checkPage()
	{
		if($_SESSION['type']=='admin'&&!in_array($_GET['halaman'],Helper::allowedAdmin())) {
			http_response_code(403);
			        echo "<html>
					<head>
						<title>403 Forbidden</title>
					</head>
					<body>
					<p>Directory access is forbidden.</p>
					</body>
					</html>";exit;
		}
 
        return true;
    }

	public static function allowedAdmin() 
	{
		return [
			"tampil_penelitian",
			"tambah_penelitian",
			"detail_penelitian",
			"hapus_penelitian",
			"ubah_penelitian",
			"tampil_pkm",
			"tambah_pkm",
			"detail_pkm",
			"hapus_pkm",
			"ubah_pkm",
			"dashbord",
			"download_lit",
		];
	}

	public static function baseUrl()
	{
		$httpType = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] === 443 ? 'https' : 'http';
		$httpRoot = $_SERVER['HTTP_HOST'];
		
		$appRoot = strlen(APP_ROOT)>0 ? APP_ROOT.'/' : '';

        return $httpType . '://' . $httpRoot . '/' .$appRoot;
    }

	public static function saveFile($file, $path, $fileName)
	{	
		$pathPermission = substr($path, 0, strrpos( $path, '/'));
		
		return $file->move($path, $fileName);
	}
}
?>