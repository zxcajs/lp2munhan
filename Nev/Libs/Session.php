<?php

namespace Nev;
use \Nev\Model;
use \Nev\Http;
use \SessionHandlerInterface;
use PDOException;
use PDO;
class Session extends Model implements SessionHandlerInterface  {
    // session-lifetime
    var $lifeTime;
    // mysql-handle
    var $dbHandle;
    
    function __construct($registry) {
        parent::__construct($registry);
    }
    function open($savePath, $sessName) {
       // get session-lifetime
        $this->lifeTime = get_cfg_var("session.gc_maxlifetime");
        // open database-connection
         return true;
    }
    function close() {
        $this->gc(ini_get('session.gc_maxlifetime'));

    }
    function read($sessID) {
        // fetch session-data
        try {
            $this->registry->log->error('read :'.$sessID.' :');
        // var_dump($sessID);
          $query="SELECT session_data AS d FROM ws_sessions WHERE session_id = '$sessID' AND session_expires > ".time();
          $stmt=$this->registry->db->prepare($query);
          $stmt->execute();
          $rs=$stmt->fetchAll(PDO::FETCH_ASSOC);
          return (count($rs)>0)?$rs[0]['d']:'';
        } catch (PDOException $e) {

             $this->registry->log->error('module  session/read :'.$e->getMessage());
          return '';
        }
    }
    function write($sessID,$sessData) {
        // new session-expire-time

       $this->registry->log->error('write :'.$sessID.' :'.$sessData);
        $newExp = time() + $this->lifeTime;
        // is a session with this id in the database?
        $fd=0;
         try {
            $sql="SELECT count(0) as found FROM ws_sessions
                            WHERE session_id =:sessionID";
             $stmt=$this->registry->db->prepare($sql);
             $stmt->bindValue(':sessionID',$sessID,PDO::PARAM_STR);
             $stmt->execute();
             $rs=$stmt->fetchAll(PDO::FETCH_ASSOC);
             $fd=$rs[0]['found'];
         } catch (PDOException $e) {
           $fd=0;
            $this->registry->log->error('module  session /write :'.$e->getMessage());
         }

        
        if($fd>0)
        {
             if(!empty($sessData)){
                 try {
                   $sql="UPDATE ws_sessions
                           SET session_expires =:sessionExpired,
                           session_data =:sessionData
                           WHERE session_id =:sessionID";

                     $stmt=$this->registry->db->prepare($sql); 
                     $stmt->bindValue(':sessionID',$sessID,PDO::PARAM_STR);
                     $stmt->bindValue(':sessionExpired',$newExp,PDO::PARAM_INT);
                     $stmt->bindValue(':sessionData',$sessData,PDO::PARAM_STR);
                     $stmt->execute();
                     $resUpdate=true;   
                 } catch (PDOException $e) {
                   $this->registry->log->error('module  session /write :'.$e->getMessage());
                     $resUpdate=false;
                 }
                  return $resUpdate;
             }else
             {
                 try {
                    $sql="delete from ws_sessions WHERE session_id =:sessionID";
                    $stmt=$this->registry->db->prepare($sql);
                    $stmt->bindValue(':sessionID',$sessID,PDO::PARAM_STR);
                    $stmt->execute();
                    $resultDelete=true;
                 } catch (PDOException $e) {
                   $this->registry->log->error('module  session /write :'.$e->getMessage());
                    $resultDelete=false;
                 }
                 
                return $resultDelete;
             }
        }else
        {
             $resultInsert=false;
                       
              if(!empty($sessData)){
              try {
                $sql="INSERT INTO ws_sessions (
                             session_id,
                             session_expires,
                             session_data)
                             VALUES(
                             :sessionID,
                             :sessionExpired,
                             :sessionData)";
                $stmt=$this->registry->db->prepare($sql);
                $stmt->bindValue(':sessionID',$sessID,PDO::PARAM_STR);
                $stmt->bindValue(':sessionExpired',$newExp,PDO::PARAM_INT);
                $stmt->bindValue(':sessionData',$sessData,PDO::PARAM_STR);
                $stmt->execute();
                $resultInsert=true;
              } catch (PDOException $e) {
                 $this->registry->log->error('module  session /write :'.$e->getMessage());
                $resultInsert=false;
              }
              
              }
              return $resultInsert;
        }


        return false;
    }
    function destroy($sessID) {
      
        try {
            
                 $sql="DELETE FROM ws_sessions WHERE session_id = '$sessID'";
                 $stmt=$this->registry->db->prepare($sql);
                 $stmt->execute();
                 
                       return true;
        } catch (PDOException $exc) {
           $this->registry->log->error('module  session /write :'.$e->getMessage());
           return false;
        }      
    }
    function gc($sessMaxLifeTime) {

        
        try{

           
            /**
             * sql dibawah ini digunakan untuk ambil session id yang sudah expired dan update data  user session nya jika menggunakan sesion user
             */
            $sql="select session_id from ws_sessions WHERE session_expires < ".time();
            $stmt=$this->registry->db->prepare($sql);
            $stmt->execute();
            $rs=$stmt->fetchAll(PDO::FETCH_ASSOC);


              $sql="DELETE FROM ws_sessions WHERE session_expires < ".time();
              $stmt=$this->registry->db->prepare($sql);
              $stmt->execute();
            return true;
        }  catch (PDOException $e)
        {
           $this->registry->log->error('module  session /write :'.$e->getMessage());
            return false;
        }
         
        
    }
} 


?>
