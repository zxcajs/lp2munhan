<?php

namespace Nev;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Log
{
    private static $env = null;
    private static $errorLog = null;
    private static $accessLog = null;
    private static $mobileAccessLog = null;
    private static $commitAttendance=null;
    public function __construct($env)
    {
        self::$env = $env;
        self::$errorLog = new Logger('errorLog');
        self::$errorLog->pushHandler(new StreamHandler(__SITE_PATH.'/log/Error-'.date('Y-m-d').'.log', Logger::ERROR));

        self::$accessLog = new Logger('accessLog');
        self::$accessLog->pushHandler(new StreamHandler(__SITE_PATH.'/log/Access-'.date('Y-m-d').'.log', Logger::INFO));
        
        self::$mobileAccessLog = new Logger('mobile_accessLog');
        self::$mobileAccessLog->pushHandler(new StreamHandler(__SITE_PATH.'/log/MobileAccess-'.date('Y-m-d').'.log', Logger::INFO));
    }

    public function error($message, $data = array())
    {
        $data = $this->convertToArray($data);
        $ip = $this->getIp();
        $message = $ip.' '.$message;
            if (count($data) > 0) {
                self::$errorLog->addError($message, $data);
            } else {
                self::$errorLog->addError($message);
            }
    }

    public function access($message, $data = array())
    {
        $data = $this->convertToArray($data);
        $ip = $this->getIp();
        $message = $ip.' '.$message;
        if (self::$env === '1') {
            if (count($data) > 0) {
                self::$accessLog->addInfo($message, $data);
            } else {
                self::$accessLog->addInfo($message);
            }
        }
    }

    public function customAccess($fileName,$message, $data = array())
    {
  
            $ip = $this->getIp();
            $message = $ip.' '.$message;
            if (count($data) > 0) {
                $newLog= new Logger('custom-success-log');
                $newLog->pushHandler(new StreamHandler(__SITE_PATH.'/log/'.$fileName.'-'.date('Y-m-d').'.log', Logger::INFO));
                $data = $this->convertToArray($data);
                $newLog->addInfo($message, $data);
            } else {
                $newLog->addInfo($message);
            }
 
    }

    public function customError($fileName,$message,$data=array())

    {
           $newLog= new Logger('custom-error-log');
           $newLog->pushHandler(new StreamHandler(__SITE_PATH.'/log/'.$fileName.'-'.date('Y-m-d').'.log', Logger::ERROR));
           $data = $this->convertToArray($data);
                $ip = $this->getIp();
                $message = $ip.' '.$message;
                    if (count($data) > 0) {
                        $newLog->addError($message, $data);
                    } else {
                        $newLog->addError($message);
                    }

    }

    private function getIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    private function convertToArray($data)
    {
        if (is_object($data)) {
            $jtext = json_encode($data);
            $arr = json_decode($jtext, true);

            return $arr;
        } else {
            if (is_array($data)) {
                return $data;
            } else {
                return array();
            }
        }
    }

    public function mobileAccess($message, $data = array())
    {
        $data = $this->convertToArray($data);
        $ip = $this->getIp();
        $message = $ip.' '.$message;
            if (count($data) > 0) {
                self::$mobileAccessLog->addInfo($message, $data);
            } else {
                self::$mobileAccessLog->addInfo($message);
            }
    }
}
