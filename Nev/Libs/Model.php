<?php
namespace Nev;

use Nev\Database\Connections;
use PDO;

class Model
{
    use Logdb {
        insertLog as protected ;
    }
    protected $registry;
    protected $query;
    protected $param = array();
    private $stmt;
    private $db;
    public function __construct($registry)
    {
        $this->registry = $registry;

        $this->registry->db = Connections::getInstance($this->registry->config->host,
            $this->registry->config->db, $this->registry->config->socket,
            $this->registry->config->user, $this->registry->config->password, $this->registry->config->dbms);

    }

    public function ConnectToOracle()
    {
        try {
            $host = $this->registry->config->hostOracle;
            $db   = $this->registry->config->dbOracle;
            $user = $this->registry->config->userOracle;
            $pass = $this->registry->config->passwordOracle;
            // die($user.$pass);
            $this->registry->dbOracle = new PDO("oci:dbname=//$host:1521/$db;", "$user", "$pass", array(
                PDO::ATTR_TIMEOUT => 10,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ));
            $this->registry->dbOracle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->registry->dbOracle->setAttribute(PDO::ATTR_CASE, PDO::CASE_NATURAL);

        } catch (\PDOException $e) {
            die("Sorry, an error has occured. Please try your request \n");
        }

    }

    protected function beginTransaction()
    {
        $this->registry->db->beginTransaction();
    }
    protected function commit()
    {
        $this->registry->db->commit();
    }
    protected function roolBack()
    {
        $this->registry->db->rollBack();
    }

    protected function sendResponse($moreInfo, $messages, $status)
    {
        return array(
            "moreInfo"  => $moreInfo,
            "messages"  => $messages,
            "status"    => $status,
        );
    }
}
