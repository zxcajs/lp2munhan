<?php
namespace Nev\Database;

use PDO;
use PDOException;

class Connections
{
    private static $instance = null;
    private static $instancePMA = NULL;

    private function __construct()
    {
        //
    }

    public static function getInstance($host, $db, $socket, $user, $pass, $dbms)
    {
        if (!self::$instance) {
            try
            {
                switch ($dbms) {
                    case 'mysql':
                        // self::$instance = new PDO("mysql:host=$host;dbname=$db;unix_socket=$socket;port=4867;", "$user","$pass");
                        self::$instance = new PDO("mysql:host=$host;dbname=$db;unix_socket=$socket;", "$user", "$pass");
                        break;
                    case 'oracle':
                        self::$instance = new PDO("oci:host=$host;dbname=$db;", "$user", "$pass");
                        break;
                    case 'pgsql':
                        self::$instance = new PDO("pgsql:host=$host;dbname=$db;", "$user", "$pass");
                        break;
                    case 'sqlite':
                        break;
                        self::$instance = new PDO("sqlite:$db;");

                }
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$instance->setAttribute(PDO::ATTR_CASE, PDO::CASE_NATURAL);
                // self::$instance->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

                return self::$instance;
            } catch (PDOException $e) {
                self::showerror("Sorry, an error has occured. Please try your request \n" . $e->getMessage());
                die();
            }
        } else {
            return self::$instance;
        }

    }

    public static function exceptionHandler($e)
    {
        set_exception_handler('exceptionHandler');
        self::showerror("Sorry, the site under maintenance \n");
    }

    public static function showerror($m)
    {
        echo "<h2>Error</h2>";
        echo nl2br(htmlspecialchars($m));
    }
    /**
     *
     *
     * Like the constructor, we make __clone private
     * so nobody can clone the instance
     *
     */
    private function __clone()
    {}

}
