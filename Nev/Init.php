<?php

    include __SITE_PATH.'/Nev/Error.php';

    define('__ADDTIMEEXPIRED',1296000);

    /*
    *  create object registry
    */
    $registry = new \Nev\Registry();

    /*
    * load variable config to registry
    */
    $registry->config = json_decode(json_encode(parse_ini_file(__SITE_PATH.'/Nev/includes/'.'config.ini')));

    /*
    * set server address dari file config
    */
    define('__SERVERADDR', $registry->config->server_address);

    /*
    * set time zone area application
    */
    date_default_timezone_set($registry->config->time_zone);

    /*
        Set Controller Name
    */

    /* set log aplikasi */
    $registry->log = new \Nev\Log($registry->config->log);
?>