<?php require_once 'layout/detail/header.php' ?>
<?php require_once 'main/sim/menu/menu_sim_detail.php'; ?>
<?php require_once 'layout/detail/body.php' ?>
<?php
	$data_pimpinan = $pimpinan->tampil_pimpinan();
?>
<div class="inner">	 		
			<div class=" text-center inner" >
				<div class=" panel panel-primary">
					<?php foreach ($data_pimpinan as $key => $value) : ?>
					<div class="panel-body">
						<div class="col-md-4">
							<img src="assets/img/pimpinan/<?php echo $value['foto_pimpinan'] ?>" class="img-responsive" width="250">
						</div>
						<div class="col-md-6">
							<table class="table table-striped">	
								<tr>
									<th>NAMA PIMPINAN</th>
									<td class="text-left">: <?php echo $value['nama_pimpinan']; ?></td>
								</tr>
								<tr>
									<th>NIP/NRP</th>
									<td class="text-left">: <?php echo $value['nomor']; ?></td>	
								</tr>
								<tr>
									<th>PANGKAT</th>
									<td class="text-left">: <?php echo $value ['pangkat']; ?></td>		 			
								</tr>
								<tr>
									<th>JABATAN</th>
									<td class="text-left">: <?php echo $value ['jabatan']; ?></td>		 		
								</tr>				 	
							</table>		
						</div>
					</div>
						<?php endforeach ?>	
				</div>
			</div>
		</div>			 
<?php include 'layout/detail/footer.php' ?>