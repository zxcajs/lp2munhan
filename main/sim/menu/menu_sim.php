<?php 
$setMenu = '<div class="container main-menu">
                <div class="row align-items-center justify-content-between d-flex">
                    <div id="logo">
                        <a href="'.Helper::baseUrl().'"><img src="assets/img/penyakit/kecil.png" alt="" title="" /></a>
                    </div>
                    <nav id="nav-menu-container">
                        <ul class="nav-menu">
                        <li><a href="'.Helper::baseUrl().'">Home</a></li>
                        <li class="menu-has-children"><a href="">Tentang LPPM</a>
                            <ul>
                            <li><a href="'.Helper::baseUrl().'sim-organisasi">Organisasi</a></li>
                            <li><a href="'.Helper::baseUrl().'sim-pimpinan">Pimpinan</a></li>
                            <li><a href="'.Helper::baseUrl().'sim-staff">Staff</a></li>
                            </ul>
                        </li>
                        <li class="menu-has-children"><a href="">Kerja Sama LPPM</a>
                            <ul>
                            <li><a href="'.Helper::baseUrl().'sim-kermadn">Dalam Negeri</a></li>
                            <li><a href="'.Helper::baseUrl().'sim-kermaln">Luar Negeri</a></li>
                            </ul>
                        </li>
                        </ul>
                    </nav><!-- #nav-menu-container -->					      		  
                </div>
            </div>';
setMenuHeader($setMenu); 
?>