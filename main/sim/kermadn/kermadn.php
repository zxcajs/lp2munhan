<?php require_once 'layout/detail/header.php' ?>
<?php require_once 'main/sim/menu/menu_sim_detail.php'; ?>
<?php require_once 'layout/detail/body.php' ?>
<?php
	$data_kermadl = $kerma_dl->tampil_kermadl(); 
?>
<div class="panel-body">
	<div class="inner">					 
		<table class="table table-bordered table-striped " id="datatables">
			<thead class="thead-light">
				<tr>
					<th scope="col" class="text-center w-70">NO</th>
					<th scope="col" class="text-left">INSTANSI/ KEMENTRIAN </th>
					<th scope="col" class="text-left">BIDANG KERJA SAMA</th>
					<th scope="col" class="text-center">TANGGAL</th>
					
				</tr>
			</thead>
			<tbody>
				<?php foreach ($data_kermadl as $key => $value) : ?>
					<tr>
						<td class="text-center"><?php echo $key+1 ?></td>
						<td class="text-left"><?php echo $value['nama_kerma']; ?></td>
						<td class="text-left"><?php echo $value['bidang_kerma']; ?></td>
						<td class="text-center w-100x"><?php echo $value ['tanggal'];?></td>
					
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>					
</div>			 
<?php include 'layout/detail/footer.php' ?>