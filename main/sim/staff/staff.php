<?php require_once 'layout/detail/header.php' ?>
<?php require_once 'main/sim/menu/menu_sim_detail.php'; ?>
<?php require_once 'layout/detail/body.php' ?>
<?php
	$data_staff = $staff->tampil_staff(); 
?>
<div class="inner">					 
	<div class=" text-center inner" >
		<table id="datatables" class="table table-bordered table-striped"  >
			<thead class="thead-light" >
				<tr>
					<th scope="col" class="text-center w-70">No</th>
					<th scope="col" class="text-left">NAMA </th>
					<th scope="col" class="text-left">PANGKAT</th>
					<th scope="col" class="text-left">JABATAN</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($data_staff as $key => $value) : ?>
					<tr>
						<td class="text-center"><?php echo $key+1 ?></td>
						<td class="text-left"><?php echo $value['nama_staff']; ?></td>
						<td class="text-left"><?php echo $value['pangkat_staff']; ?></td>
						<td class="text-left"><?php echo $value ['jabatan_staff'];?></td>
					
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>			 
<?php include 'layout/detail/footer.php' ?>