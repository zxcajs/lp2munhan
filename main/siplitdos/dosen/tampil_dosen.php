<?php require_once 'layout/detail/header.php' ?>
<?php require_once 'main/siplitdos/menu/menu_litdos_detail.php'; ?>
<?php require_once 'layout/detail/body.php' ?>
<?php
	$data_laporan_dosen = $dosen->tampil_dosen();
?>
<div class=" text-center inner">
	<table class="table table-bordered table-striped " id="datatables">
		<thead class="thead-light">
			<tr>
				<th scope="col" class="text-center w-70">No</th>
				<th scope="col" class="text-left">NAMA DOSEN</th>
				<th scope="col" class="text-left">FAKULTAS</th>
				<th scope="col" class="text-left">PRODI</th>
				<th scope="col" class="text-center">JUMLAH PENELITIAN</th>
				<th scope="col" class="text-center w-70">Opsi</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($data_laporan_dosen as $key => $value) : ?>
				<tr>
					<td class="text-center"><?php echo $key+1 ?></td>
					<td class="text-left"><?php echo $value['nama_dosen']; ?></td>
					<td class="text-left"><?php echo $value['fakultas']; ?></td>
					<td class="text-left"><?php echo $value ['prodi'];?></td>
					<td><?php echo $value ['jml_lit'];?></td>
					<td><a href="<?php echo Helper::baseUrl()?>litdos-detail-penelitian-dosen?id=<?php echo $value['id_dosen']; ?>" class="btn btn-xs btn-info"><i class="fa fa-eye"> Detail</a></td>
				
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	
</div>
					 
<?php include 'layout/detail/footer.php' ?>