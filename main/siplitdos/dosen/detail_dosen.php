<?php require_once 'layout/detail/header.php' ?>
<?php require_once 'main/siplitdos/menu/menu_litdos_detail.php'; ?>
<?php require_once 'layout/detail/body.php' ?>
<?php
    $id = $_GET['id'];
    $datadosen = $dosen->detail_dosen($id);
?>
<div class="col-md-15">
 	<table class="table table-striped">
		 <tr>
			<th>NAMA</th>
			<th><?php echo $datadosen['nama_dosen']; ?></th>
		</tr>
		<tr>
			<th>NRP/NIP/NIDN</th>
			<th><?php echo $datadosen['nrp']; ?></th>
		</tr>
		<tr>
			<th>PANGKAT</th>
			<th><?php echo $datadosen['pangkat']; ?></th>
		</tr>
		<tr>
			<th>JABATAN</th>
			<th><?php echo $datadosen ['jabatan']; ?></th>
		</tr>
		<tr>
			<th>JABATAN AKADEMIK</th>
			<th><?php echo $datadosen['jabatan_akademik']; ?></th>
		</tr>
		<tr>
			<th>KEILMUAN</th>
			<th><?php echo $datadosen['keilmuan']; ?></th>
		</tr>
		<tr>
			<th>POSISI</th>
			<th><?php echo $datadosen['posisi']; ?></th>
		</tr>
		<tr>
			<th>PRODI</th>
			<th><?php echo $datadosen['prodi']; ?></th>
		</tr>
		<tr>
			<th>FAKULTAS</th>
			<th><?php echo $datadosen['fakultas']; ?></th>
		</tr>
 	</table>
 	</div>
     <a href="<?php echo Helper::baseUrl()?>litdos-data-penelitian-dosen" class="btn btn-xs btn-danger"><i class="fa fa-backward"></i> Kembali</a>	
<?php include 'layout/detail/footer.php' ?>