<?php require_once 'layout/detail/header.php' ?>
<?php require_once 'main/siplitdos/menu/menu_litdos_detail.php'; ?>
<?php require_once 'layout/detail/body.php' ?>
<?php
    $id_lit = $_GET['id'];
    $data_lit = $lit->detail_lit($id_lit);
?>
<div class="col-md-15">
 	<table class="table table-striped">
 		<tr>
 			<th>Nama Penulis</th>
 			<th><?php echo $data_lit['nama_penulis']; ?></th>
 		</tr>
 		<tr>
 			<th>Judul Penelitian</th>
 			<th><?php echo $data_lit['judul_penelitian']; ?></th>
 		</tr>
 		<tr>
 			<th>Author/Responding Author</th>
 			<th><?php echo $data_lit['author']; ?></th>
 		</tr>
		 <tr>
 			<th>Prodi</th>
 			<th><?php echo $data_lit['ref_prodi_name']; ?></th>
 		</tr>
		 <tr>
 			<th>Fakultas</th>
 			<th><?php echo $data_lit['ref_fakultas_name']; ?></th>
 		</tr>
 		<tr>
 			<th>Jurnal </th>
 			<th><?php echo $data_lit['jurnal']; ?></th>
 		</tr>
		 <tr>
			<th>Tahun </th>
			<th><?php echo $data_lit['tahun']; ?></th>
		</tr>
		 <?php if($data_lit['jurnal']<>'Tidak Ada') { ?>
			<tr>
				<th>Publisher  </th>
				<th><?php echo $data_lit['publiser']; ?></th>
			</tr>
			<tr>
				<th>E-ISSN  </th>
				<th><?php echo $data_lit['issn']; ?></th>
			</tr>
			<tr>
				<th>Jadwal terbit </th>
				<th><?php echo $data_lit['terbit']; ?></th>
			</tr>
			<tr>
				<th>Negara </th>
				<th><?php echo $data_lit['negara']; ?></th>
			</tr>
			<tr>
				<th>URL </th>
				<th><?php echo $data_lit['url']; ?></th>
			</tr>
		<?php } ?>
 	</table>
 	</div>
     <a href="<?php echo Helper::baseUrl()?>litdos-data-penelitian" class="btn btn-xs btn-danger"><i class="fa fa-backward"></i> Kembali</a>
					 
<?php include 'layout/detail/footer.php' ?>