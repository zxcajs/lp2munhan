<?php require_once 'layout/detail/header.php' ?>
<?php require_once 'main/siplitdos/menu/menu_litdos_detail.php'; ?>
<?php require_once 'layout/detail/body.php' ?>
<?php
	$data_lit = $lit->tampil_lit(); 
?>
<div class=" text-center inner">
	<table class="table table-bordered table-striped  " id="datatables">
		<thead class="thead-light">
			<tr>
				<th scope="col" class="text-center w-70">NO</th>
				<th scope="col" class="text-left">JUDUL PENELITIAN</th>
				<th scope="col" class="text-left">NAMA PENULUS</th>
				<th scope="col" class="text-center w-220"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($data_lit as $key => $value) : ?>
				<tr>
					<td><?php echo $key+1 ?></td>
					<td class="text-left"><?php echo $value['judul_penelitian']; ?></td>
					<td class="text-left"><?php echo $value ['nama_penulis'];?></td>
					<td>
						<?php if(!empty($value['document'])){ ?>
							<a target="_blank" href="<?php echo Helper::baseUrl().'admin/upload/penelitian/'.$value['document']; ?>" class="btn btn-xs btn-primary"><i class="fa fa-download"></i> Download</a>
						<?php }else{ ?>
							<a href="#0" disabled="disabled" class="btn btn-xs btn-primary"><i class="fa fa-download"></i> Download</a>
						<?php } ?>
						<a href="<?php echo Helper::baseUrl()?>litdos-detail-penelitian?id=<?php echo $value ['id_lit']; ?>" class="btn btn-xs btn-info"><i class="fa fa-eye"></i> Detail</a>										
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
					 
<?php include 'layout/detail/footer.php' ?>