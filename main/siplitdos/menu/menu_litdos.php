<?php 
$menus = $prodi->getMenuProdi();
$listMenuProdi = "";
foreach($menus as $k => $v){ 
    $listMenuProdi .= '<li class="menu-has-children"><a href="'.Helper::baseUrl().'/main/fakultas/detail_fakultas.php?id='. $v["id"].'">'. $v["nama"].'</a><ul>';
    $subProd = "";
    foreach($v['child'] as $kP => $vP){
        $subProd .='<li><a href="'.Helper::baseUrl().'/main/prodi/detail_prodi.php?id='.$vP['id'].'">'.$vP['nama'].'</a></li>';
    }
    $listMenuProdi .= $subProd.'</ul></li>';
}
$setMenu = '<div class="container main-menu">
                <div class="row align-items-center justify-content-between d-flex">
                    <div id="logo">
                        <a href="'.Helper::baseUrl().'"><img src="assets/img/penyakit/kecil.png" alt="" title="" /></a>
                    </div>
                    <nav id="nav-menu-container">
                        <ul class="nav-menu">
                            <li><a href="'.Helper::baseUrl().'">Home</a></li>
                            <li class="menu-has-children"><a href="'.Helper::baseUrl().'litdos-data-penelitian">DATA PENELITIAN</a>	</li>
                            <li class="menu-has-children"><a href="#0">DATA REKAPITULASI</a>
                                <ul>
                                    <li><a href="'.Helper::baseUrl().'litdos-data-penelitian-dosen"> DATA DOSEN </a></li>
                                    <li class="menu-has-children"><a href="#0">DATA PRODI</a>
                                        <ul>
                                            '.$listMenuProdi.'
                                        </ul>
                                    </li>
                                    <li><a href="'.Helper::baseUrl().'litdos-rekap-all"> DATA REKAPITULASI </a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav><!-- #nav-menu-container -->					      		  
                </div>
            </div>';
setMenuHeader($setMenu); 
?>