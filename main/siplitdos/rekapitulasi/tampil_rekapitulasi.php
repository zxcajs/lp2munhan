<?php require_once 'layout/detail/header.php' ?>
<?php require_once 'main/siplitdos/menu/menu_litdos_detail.php'; ?>
<?php require_once 'layout/detail/body.php' ?>
<?php
	$prodi = $prodi->get_fakultas();
?>
<div class="">
	<a type="button" class="btn btn-xs btn-primary btn-flat" href="rekap_all.php">
	<i class="fa "></i> JUMLAH DATA REKEPITULASI</a> 
</div>
<div class=" text-center inner">
	
	<table class="table table-bordered table-striped " id="datatables">
		<thead>
			<tr>
				<th class="text-center w-70">NO </th>			
				<th>FAKULTAS</th>
				<th class="text-center">JUMLAH DOSEN/PENULIS</th>
				<th class="text-center">JUMLAH PENELITIAN</th>
				<th class="text-center">JUMLAH PUBLIKASI ARTIKEL</th>
				<th class="text-center w-70">Opsi</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($prodi as $key => $value) :?>
			<tr>
				<td class="text-center"><?php echo $key+1 ?></td>
				<td><?php echo $value['nama_fakultas'] ?></td>
				<td class="text-center"><?php echo $value['jml_dosen'] ?></td>
				<td class="text-center"><?php echo $value['jml_lit_fakultas'] ?></td>
				<td class="text-center"><?php echo $value['jml_publish'] ?></td>
				<td class="text-center">
					<a href="detail_rekapitulasi.php?id=<?php echo $value['id_fakultas']; ?>" class="btn btn-xs  btn-info"><i class="fa fa-eye"></i> Detail</a>	
					<!-- <a href="index.php?halaman=hapus_rekapitulasi&id_rekapitulasi=<?php echo $value['id_fakultas']; ?>" class="btn btn-xs btn-danger" onclick="return confirm('hapus data ?')" ><i class="fa fa-trash"></i> Hapus</a> -->
				</td>
				
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	
</div>
					 
<?php include 'layout/detail/footer.php' ?>