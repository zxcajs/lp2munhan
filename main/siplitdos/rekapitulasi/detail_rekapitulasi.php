<?php require_once 'layout/detail/header.php' ?>
<?php require_once 'main/siplitdos/menu/menu_litdos_detail.php'; ?>
<?php require_once 'layout/detail/body.php' ?>
<?php
    $id = $_GET['id'];
    $data = $prodi->get_name_fakultas($id);
	$dataGrafik = $grafik->grafikPenelitianByFakultas($id);
?>
<style>
		.highcharts-figure,
		.highcharts-data-table table {
			min-width: 310px;
			max-width: 800px;
			margin: 1em auto;
		}

		#container {
			height: 400px;
		}

		.highcharts-data-table table {
			font-family: Verdana, sans-serif;
			border-collapse: collapse;
			border: 1px solid #ebebeb;
			margin: 10px auto;
			text-align: center;
			width: 100%;
			max-width: 500px;
		}

		.highcharts-data-table caption {
			padding: 1em 0;
			font-size: 1.2em;
			color: #555;
		}

		.highcharts-data-table th {
			font-weight: 600;
			padding: 0.5em;
		}

		.highcharts-data-table td,
		.highcharts-data-table th,
		.highcharts-data-table caption {
			padding: 0.5em;
		}

		.highcharts-data-table thead tr,
		.highcharts-data-table tr:nth-child(even) {
			background: #f8f8f8;
		}

		.highcharts-data-table tr:hover {
			background: #f1f7ff;
		}
	</style>
	
<div class="col-md-15">
	<div class="col-md-6">
		<table class="table table-striped">
			<tr>
				<tr>
					
					<th class="text-center"><?php echo $data['nama_fakultas']; ?></th>
				</tr>
				<tr>
					
					<th>_____________________________________________________________</th>
				</tr>
				<tr>
					<th>Responding Author Tim</th>
					<th class="text-right"><?php echo $data['jml_author_tim']; ?></th>

				</tr>
				<tr>
					<th>Responding Author Mandiri</th>
					<th class="text-right"><?php echo $data['jml_author_mandiri']; ?></th>
				</tr>
				<tr>
					<th>Responding Author Bersama Mahasiswa</th>
					<th class="text-right"><?php echo $data['jml_author_mahasiswa']; ?></th>
				</tr>
				<tr>
					<th>Jurnal Nasional Akreditasi</th>
					<th class="text-right"><?php echo $data['jml_jurnal_nas_akr']; ?></th>
				</tr>
				<tr>
					<th>Jurnal Nasional Non Akreditasi</th>
					<th class="text-right"><?php echo $data['jml_jurnal_nas_nonakr']; ?></th>
				</tr>
				<tr>
					<th>Jurnal International Reputasi</th>
					<th class="text-right"><?php echo $data['jml_jurnal_inter_rep']; ?></th>
				</tr>
				<tr>
					<th>Jurnal International Non Reputasi</th>
					<th class="text-right"><?php echo $data['jml_jurnal_inter_nonrep']; ?></th>
				</tr>
				<tr>
					<th>Mempunyai Akun Google Scholar</th>
					<th class="text-right"><?php echo $data['jml_google']; ?></th>
				</tr>
				<tr>
					<th>Tidak Mempunyai Akun Google Scholar</th>
					<th class="text-right"><?php echo $data['jml_nongoogle']; ?></th>
				</tr>
				<!-- <tr>
					<th>Tahun</th>
					<th></th>
				</tr> -->
				<tr>
					<th>__________________</th>
					<th class="text-right">_____________________________________</th>
				</tr>
				<tr>
					<th>JUMLAH PENULIS/ DOSEN</th>
						<th class="text-right"><?php echo $data['jml_dosen']; ?></th>
				</tr>
				<tr>
					<th>JUMLAH KESELURUHAN FAKULTAS</th>
					<th class="text-right"><?php echo $data['jml_lit']; ?></th>
				</tr>
			</tr>
		</table>
	</div>
	<div class="col-md-6">
		<figure class="highcharts-figure">
			<div id="container"></div>
		</figure>
	</div>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
	Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Perkembangan Per Prodi'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: <?php echo json_encode($dataGrafik['prodi']); ?>,
        crosshair: true
    },
    yAxis: {
        title: {
            useHTML: true,
            text: 'Jumlah'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: <?php echo json_encode($dataGrafik['series']); ?>
});
</script>
<!-- <a href="tampil_dosen.php" class="btn btn-xs btn-danger"><i class="fa fa-backward"></i> Kembali</a>		  -->
<?php include 'layout/detail/footer.php' ?>