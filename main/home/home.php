<?php 
$setBanner = '
<div class="container pb-5">   
	<div class="row py-5 align-items-center">
		<div class="col-lg-6">
			<h5 class="display-4 mb-4 font-weight-bold text-white"></h5>
			<h1 class="display-4 mb-4 font-weight-bold text-white"> SISTEM INFORMASI LPPM</h1>
			<h2 class="text-white">LEMBAGA PENELITIAN DAN PENGABDIAN KEPADA MASYARAKAT </h2>
			<h2> </h2>
			<h6 class="text-white"> UNIVERSITAS PERTAHANAN REPUBLIK INDONESIA</h6>
			<p class="h5 aa mb-4 pb-3 text-white-50">       
				<div class="d-flex star-rating mb-5"> 
					<a href=""><i class="lni lni-star-filled"></i></a> 
					<a href=""><i class="lni lni-star-filled"></i></a> 
				</div>
			</p>
			<p class="h5 aa mb-4 pb-3 text-white-50"></p>
			<h3 class="text-white mb-5"> </h3>
			<h3 class="text-white mb-5"> </h3>
			<p class="h5 aa mb-4 pb-3 text-white-50"></p>
		</div>
		<div class="col-lg-6 text-lg-right text-center mt-5 mt-lg-0">
			<div class="banner-phone-image"> <img src="assets/img/penyakit/UNHAN_FIX.png"> </div>
		</div>';
setBannerInfo($setBanner); ?>
<?php 
$setMenu = '<div class="container main-menu">
	<div class="row align-items-center justify-content-between d-flex">
		<div id="logo">
			<a href="'.Helper::baseUrl().'"><img src="assets/img/penyakit/kecil.png" alt="" title="" /></a>
		</div>
		<nav id="nav-menu-container">
			<ul class="nav-menu">
				<li><a href="'.Helper::baseUrl().'">Home</a></li>
				<li class="menu-has-children"><a href="'.Helper::baseUrl().'sim">SIMLPPM</a></li>
				<li ><a href="'.Helper::baseUrl().'litdos">SIPLITDOS</a></li>
				<li><a href="'.Helper::baseUrl().'pkm">SIPKM</a></li>
				<li><a href="'.Helper::baseUrl().'sibana">SIBANA</a></li>
			</ul>
		</nav><!-- #nav-menu-container -->					      		  
	</div>
</div>';
setMenuHeader($setMenu); 
?>
<?php include 'layout/body.php'; ?>
<?php include 'layout/section.php'; ?>
<?php include 'layout/footer.php'; ?>