
<body>
<style type="text/css">
@import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap');
body {
    font-family: 'Roboto', sans-serif;
}
.btn {
    padding: 14px 26px;
    font-weight: 700;
    font-size: 13px;
    letter-spacing: 1px;
    text-transform: uppercase;
}
.btn-danger {
    background-color: #e34c43;
    border-color: #e34c43;
}
.banner-image{
    background: linear-gradient(rgba(29, 38, 113, 0.8), rgba(195, 55, 100, 0.8)), url(assets/img/penyakit/pkm.jpg);
    background-size: cover;
    background-position: center;
}
.banner-phone-image img{
    width:300px;
}
.star-rating a{
color:rgba(255,255,255,0.5);
margin-right:10px;
}
.star-rating a:hover{
color:#fff;
}
.aa{
line-height:26px;    
}
</style>
 
 
<div class="banner-image">
<p class="h5 aa mb-4 pb-3 text-white-50"></p>
    <h3 class="text-white mb-5"> </h3>
    <h3 class="text-white mb-5"> </h3>
    <p class="h5 aa mb-4 pb-3 text-white-50"></p>
    <h3 class="text-white mb-5"> </h3>
     <h3 class="text-white mb-5"> </h3>
	<div class="container pb-5">
    	<div class="row py-5 align-items-center">
      	<div class="col-lg-6">
        <h5 class="display-4 mb-4 font-weight-bold text-white"></h5>
        <h1 class="display-4 mb-4 font-weight-bold text-white"> SIPKM</h1>
		<h2 class="text-white">SISTEM INFORMASI PENGABDIAN KEPADA MASYARAKAT
</h2>
		<h2> </h2>
		  <p class="h5 aa mb-4 pb-3 text-white-50">       
	         	<div class=""> 
	        	<a href="">
	        	<i class=""></i>
	        	</a> 
	        	<a href="">
	        	<i class=""></i>
	        	</a> 
	        	<a href="">
	        	<i class=""></i>
	        	</a> 
	        	<a href=""><i class=""></i><h6 class="text-white"> LEMBAGA PENELITIAN DAN PENGABDIAN KEPADA MASYARAKAT </h6></a> 
	        	</div>
	    	</p>
	    	<p class="h5 aa mb-4 pb-3 text-white-50">       
	         	<div class=""> 
	        	<a href="">
	        	<i class=""></i>
	        	</a> 
	        	<a href="">
	        	<i class=""></i>
	        	</a> 
	        	<a href="">
	        	<i class=""></i>
	        	</a> 
	        
	        	</div>
	    	</p>
		
						
	      
        <p class="h5 aa mb-4 pb-3 text-white-50"></p>
        <h3 class="text-white mb-5"> </h3>
        <h3 class="text-white mb-5"> </h3>
        <p class="h5 aa mb-4 pb-3 text-white-50"></p>  
      </div>
      <div class="col-lg-6 text-lg-right text-center mt-5 mt-lg-0">
        <div class="banner-phone-image"> <img src="assets/img/penyakit/UNHAN_FIX.png"> </div>
      </div>
    </div>
  </div>
</div>
 
<!-- Optional JavaScript --> 
<!-- jQuery first, then Popper.js, then Bootstrap JS --> 
 <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" ></script>
</body>
</html>
	
	<!DOCTYPE html>
	<html lang="zxx" lang="en" class="no-js">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
		<link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet">
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>SIPKM LPPM UNHAN RI</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="css/linearicons.css">
			<link rel="stylesheet" href="css/font-awesome.min.css">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="css/magnific-popup.css">
			<link rel="stylesheet" href="css/jquery-ui.css">				
			<link rel="stylesheet" href="css/nice-select.css">							
			<link rel="stylesheet" href="css/animate.min.css">
			<link rel="stylesheet" href="css/owl.carousel.css">				
			<link rel="stylesheet" href="css/main.css">
		</head>
		<body>	
			<header id="header">
				<div class="header-top">
					<div class="container">
			  		<div class="row align-items-center">
			  			<div class="col-lg-6 col-sm-6 col-6 header-top-left">
			  				<ul>
			  					<li><a href="#">home</a></li>
			  					<!-- <li><a href="login.php">login</a></li> -->
			  				</ul>			
			  			</div>
			  			<div class="col-lg-6 col-sm-6 col-6 header-top-right">
							<div class="header-social">
								<a href="https://id-id.facebook.com/unhanhumas.idu.ac.id" target="_blank"><i class="fa fa-facebook"></i></a>

								<a href="https://twitter.com/Unhan_RI" target="_blank"><i class="fa fa-twitter"></i></a>

								<a href="https://www.instagram.com/unhan_ri" target="_blank"><i class="fa fa-instagram"></i></a>

								<a href="https://www.youtube.com/c/universitaspertahananofficial" target="_blank"><i class="fa fa-youtube"></i></a>

							</div>
			  			</div>
			  		</div>			  					
					</div>
				</div>
				<div class="container main-menu">
					<div class="row align-items-center justify-content-between d-flex">
				      <div id="logo">
				        <a href="index.php"><img src="assets/img/penyakit/kecil.png" alt="" title="" /></a>
				      </div>
				      <nav id="nav-menu-container">
				        <ul class="nav-menu">
				          <li><a href="index.php">Home</a></li>
				           <li class="menu-has-children"><a href="index_sim.php">DOSEN</a>	</li>
	      				   <li class="menu-has-children"><a href="">DATA PKM</a>
				           <ul>
				            <li><a href="elements.php">DATA PKM </a></li>
							    <li class="menu-has-children"><a href="">FAKULTAS </a>
								    <ul>
										<li class="menu-has-children"><a href="">DOKTORAL ILMU PERTAHANAN (S3)</a>
											<ul>
												<li class="menu-has-children"><a href="">KONSENTRASI STRATEGI PERTAHANAN</a>
												</li>
												<li><a href="#">KONSENTRASI MANAJEMEN PERTAHANAN</a></li>
												<li><a href="#">KONSENTRASI KEAMANAN NASIONAL</a></li>
												<li><a href="#">KONSENTRASI TEKNOLOGI PERTAHANAN</a></li>
												
								   			</ul>
										</li>
										<li><a href="#">FAKULTAS STRATEGI PERTAHANAN (S2)</a>
											<ul>
												<li><a href="#">PRODI DIPLOMASI PERTAHANAN</a></li>
												<li><a href="#">PRODI STRATEGI PERANG SEMESTA</a></li>
												<li><a href="#">PRODI STRATEGI PERANG LAUT</a></li>
												<li><a href="#">PRODI STRATEGI PERANG UDARA</a></li>
								   			</ul>
										</li>
										<li><a href="#">FAKULTAS MANAJEMEN PERTAHANAN (S2)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI MANAJEMEN PERTAHANAN</a>
												</li>
												<li><a href="#">PRODI EKONOMI PERTAHANAN</a></li>
												<li><a href="#">PRODI KETAHANAN ENERGI</a></li>
								   			</ul>
										</li>
										<li><a href="#">FAKULTAS KEAMANAN NASIONAL (S2)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI MANAJEMEN BENCANA</a>
												</li>
												<li><a href="#">PRODI DAMAI DAN RESOLUSI KONFLIK</a></li>
												<li><a href="#">PRODI KEAMANAN MARITIM</a></li>
								   			</ul>
										</li>
										<li class="menu-has-children"><a href="">FAKULTAS TEKNOLOGI PERTAHANAN (S2)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI TEKNOLOGI PENGINDERAAN</a>
												</li>
												<li><a href="#">PRODI INDUSTRI PERTAHANAN</a></li>
												<li><a href="#">PRODI TEKNOLOGI PERSENJATAAN</a></li>
												<li><a href="#">PRODI TEKNOLOGI DAYA GERAK</a></li>
												<li><a href="#">PRODI TEKNOLOGI PERTAHANAN CYBER</a></li>
								   			</ul>
										</li>
										<li class="menu-has-children"><a href="">FAKULTAS KEDOKTERAN MILITER (S1)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI SARJANA PENDIDIKAN KEDOKTERAN</a>
												</li>
												<li><a href="#">PRODI PROFESI KEDOKTERAN</a></li>
								   			</ul>
										</li>
											<li class="menu-has-children"><a href="">FAKULTAS MIPA MILITER (S1)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI SARJANA BIOLOGI</a>
												</li>
												<li><a href="#">PRODI SARJANA KIMIA</a></li>
												<li><a href="#">PRODI SARJANA FISIKA</a></li>
												<li><a href="#">PRODI SARJANA MATEMATIKA</a></li>
												
								   			</ul>
										</li>
										<li class="menu-has-children"><a href="">FAKULTAS TEKNIK MILITER (S1)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI TEKNOLOGI PENGINDERAAN</a>
												</li>
												<li><a href="#">PRODI SARJANA TEKNIK INFORMATIKA</a></li>
												<li><a href="#">PRODI SARJANA TEKNIK KONSTRUKSI BANGUNANA MILITER</a></li>
												<li><a href="#">PRODI SARJANA TEKNIK ELEKTRO</a></li>
												<li><a href="#">PRODI SARJANA TEKNIK MESIN</a></li>
								   			</ul>
										</li>
											<li class="menu-has-children"><a href="">FAKULTAS FARMASI MILITER (S1)</a>
											<ul>
												<li class="menu-has-children"><a href="">PRODI SARJANA ILMU FARMASI</a>
												
								   			</ul>
										</li>



										
								    </ul>
							    </li>					                		
				           </ul>
				          </li>	
				     			          					          		          
				         
				        </ul>
				      </nav><!-- #nav-menu-container -->					      		  
					</div>
				</div>
			</header><!-- #header -->
			
			
				
			<!-- Start blog Area -->
			<section class="recent-blog-area section-gap">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-60 col-lg-9">
							<div class="title text-center">
								<h1 class="mb-10">LPPM UNHAN RI</h1>
								<p>Berperan aktif dalam beberapa acara khususnya dalam penelitian dan pengabdian masyarakat</p>
							</div>
						</div>
					</div>							
					<div class="row">
						<div class="active-recent-blog-carusel">
							<div class="single-recent-blog-post item">
								<div class="thumb">
									<img class="img-fluid" src="img/pks/3.jpg" alt="">
								</div>
								<div class="details">
									<div class="tags">
										
									</div>
									<a href="#"><h4 class="title">Penelitian Titik Temu Sumber Air</h4></a>
									<p>
										LPPM telah melakukan beberapa penelitian di berbagai wilayah
									</p>
									<h6 class="date">25 September,2022</h6>
								</div>	
							</div>
							<div class="single-recent-blog-post item">
								<div class="thumb">
									<img class="img-fluid" src="img/pks/7.jpg" alt="">
								</div>
								<div class="details">
									<div class="tags">
										
									</div>
									<a href="#"><h4 class="title">Creative Outdoor Ads</h4></a>
									<p>
										Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer.
									</p>
									<h6 class="date">31st January,2018</h6>
								</div>	
							</div>
							<div class="single-recent-blog-post item">
								<div class="thumb">
									<img class="img-fluid" src="img/pks/5.jpg" alt="">
								</div>
								<div class="details">
									<div class="tags">
									
									</div>
									<a href="#"><h4 class="title">It's Classified How To Utilize Free</h4></a>
									<p>
										Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer.
									</p>
									<h6 class="date">31st January,2018</h6>
								</div>	
							</div>	
							<div class="single-recent-blog-post item">
								<div class="thumb">
									<img class="img-fluid" src="img/pks/8.jpeg" alt="">
								</div>
								<div class="details">
									<div class="tags">
										
									</div>
									<a href="#"><h4 class="title">Low Cost Advertising</h4></a>
									<p>
										Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer.
									</p>
									<h6 class="date">31st January,2018</h6>
								</div>	
							</div>
							<div class="single-recent-blog-post item">
								<div class="thumb">
									<img class="img-fluid" src="img/pks/1.jpeg" alt="">
								</div>
								<div class="details">
									<div class="tags">
									
									</div>
									<a href="#"><h4 class="title">Creative Outdoor Ads</h4></a>
									<p>
										Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer.
									</p>
									
								</div>	
							</div>
							<div class="single-recent-blog-post item">
								<div class="thumb">
									<img class="img-fluid" src="img/pks/10.jpeg" alt="">
								</div>
								<div class="details">
									<div class="tags">
										
									</div>
									<a href="#"><h4 class="title">It's Classified How To Utilize Free</h4></a>
									<p>
										Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer.
									</p>
									<h6 class="date">31st January,2018</h6>
								</div>	
							</div>														

						</div>
					</div>
				</div>	
			</section>
			<!-- End recent-blog Area -->			

			<!-- start footer Area -->		
			<footer class="footer-area section-gap">
				<div class="container">

					<div class="row">
						<div class="col-lg-3  col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>LPPM Unhan RI</h6>
								<p>
									Merupakan lembaga...
								</p>
							</div>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>Kantor LPPM UNHAN RI</h6>
								<div class="row">
									<div class="col">
										<ul>
											<li><a href="#"> UNIVERSITAS PERTAHANAN RI</a></li>
											<li><a href="#"> PROGRAM STUDI</a></li>
											<li><a href="#"> PASCASARJANA & DOKTORAL </a></li>
											<li>
												<a>
													<a  href="https://goo.gl/maps/FjcfbZjpXkSCMLL87">Jl. Salemba Raya No.3, RT.1/RW.3, Paseban, Jakarta, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10440</a>
												</a>
												
											</li>
										
											
										
										</ul>
									</div>
																	
								</div>							
							</div>
						</div>							
					
							<div class="col-lg-3  col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>Email</h6>
								<p>
									More information about LPPM UNHAN
								</p>								
								<div id="mc_embed_signup">
									<form target="_blank" action="">
										<div class="input-group d-flex flex-row">
											<input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
											<button class="btn bb-btn"><span class="lnr lnr-location"></span></button>		
										</div>									
										<div class="mt-10 info"></div>
									</form>
								</div>
							</div>
						</div>

					</div>

					<div class="row footer-bottom d-flex justify-content-between align-items-center">
						<p class="col-lg-8 col-sm-12 footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | UNIVERSITAS PERTAHANAN REPUBLIK INDONESIA  <a href="https://www.idu.ac.id/" target="_blank">www.idu.ac.id</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
						<div class="col-lg-4 col-sm-12 footer-social">
							<a href="https://id-id.facebook.com/unhanhumas.idu.ac.id" target="_blank"><i class="fa fa-facebook"></i></a>
							<a href="https://twitter.com/Unhan_RI" target="_blank"><i class="fa fa-twitter"></i></a>
							<a href="https://www.instagram.com/unhan_ri" target="_blank"><i class="fa fa-instagram"></i></a>
							<a href="https://www.youtube.com/c/universitaspertahananofficial" target="_blank"><i class="fa fa-youtube"></i></a>
						</div>
					</div>
				</div>
			</footer>
			<!-- End footer Area -->	

			<script src="js/vendor/jquery-2.2.4.min.js"></script>
			<script src="js/popper.min.js"></script>
			<script src="js/vendor/bootstrap.min.js"></script>			
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>		
 			<script src="js/jquery-ui.js"></script>					
  			<script src="js/easing.min.js"></script>			
			<script src="js/hoverIntent.js"></script>
			<script src="js/superfish.min.js"></script>	
			<script src="js/jquery.ajaxchimp.min.js"></script>
			<script src="js/jquery.magnific-popup.min.js"></script>						
			<script src="js/jquery.nice-select.min.js"></script>					
			<script src="js/owl.carousel.min.js"></script>							
			<script src="js/mail-script.js"></script>	
			<script src="js/main.js"></script>	
		</body>
	</html>