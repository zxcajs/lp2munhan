<?php
    include '../../config/konek_database.php';
    include '../../config/app.config.php';
    include '../../config/helper.php';
    
    if (!empty($_POST["email"])&&!empty($_POST["password"])) {
        $uname = Helper::validate($_POST['email']);
        $pass = Helper::validate($_POST['password']);
    
        $username = filter_var($uname, FILTER_SANITIZE_STRING);
        $password = filter_var($pass, FILTER_SANITIZE_STRING);
        
        // $passwordHash = password_hash($password, PASSWORD_DEFAULT);

        $isLoggedIn = $auth->login($username);
        if (! $isLoggedIn) {
            $_SESSION["errorMessage"] = "email/username salah !";
            $HeadTo=Helper::baseUrl().'admin/index.php';

            header("Location: ".$HeadTo);
            exit();
        }else{
            if (password_verify($password, $isLoggedIn['password'])) {
                unset($_SESSION["errorMessage"]);
                $_SESSION["user_id"] = $isLoggedIn['id'];
                $_SESSION["type"] = $isLoggedIn['type'];

                $HeadTo=Helper::baseUrl().'admin/index.php';
                header("Location:".$HeadTo);
                exit();
            } else {
                $_SESSION["errorMessage"] = "password salah !";
                
                $HeadTo=Helper::baseUrl().'admin/index.php';
                header("Location: ".$HeadTo);
                exit();
            }
        }
    }
?>