<?php include '../../config/app.config.php'; ?>
<?php include '../../config/helper.php'; ?>
<!DOCTYPE HTML>
<html lang="zxx">

<head>
	<title>SIM LPPM UNHAN RI</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<meta name="keywords" content="Latest Login Form Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- Meta tag Keywords -->

	<!-- css files -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->

	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	 rel="stylesheet">
	<!-- //web-fonts -->
	<style>
		/* Create two equal columns that floats next to each other */
		.column {
			float: left;
			width: 50%;
		}

		/* Clear floats after the columns */
		.row:after {
			content: "";
			display: table;
			clear: both;
		}
		.tooltip {
			position: relative;
			display: inline-block;
			border-bottom: 1px dotted black; /* If you want dots under the hoverable text */
		}

		/* Tooltip text */
		.tooltip .tooltiptext {
			visibility: hidden;
			width: 120px;
			background-color: black;
			color: #fff;
			text-align: center;
			/* padding: 5px 0; */
			border-radius: 6px;
			
			/* Position the tooltip text - see examples below! */
			position: absolute;
			z-index: 1;
		}

		/* Show the tooltip text when you mouse over the tooltip container */
		.tooltip:hover .tooltiptext {
			visibility: visible;
		}
		</style>
</head>

<body>
	<div class="main-bg">
		<!-- title -->
		<h1>SIM LPPM UNHAN RI</h1>
		<!-- //title -->
		<!-- content -->
		<div class="sub-main-w3">
			<div class="bg-content-w3pvt">
				<div class="top-content-style">
					<img src="images/unhan.png" style="width:30%" alt="" />
				</div>
				<form action="login-action.php" method="post" id="frmLogin" onSubmit="return validate();">
					<p class="legend">Login Disini<span class="fa fa-hand-o-down"></span></p>
					<?php 
						if(isset($_GET['error'])) {
					?>
					<div class="error-message"><?php  echo $_GET['error']; ?></div>
					<?php 
						unset($_GET['error']);
					} 
					?>
					<div class="input">
						<input type="text" placeholder="Email/Username" name="email" required />
						<span class="fa fa-envelope"></span>
					</div>
					<div class="input">
						<input type="password" placeholder="Password" name="password" required />
						<span class="fa fa-unlock"></span>
					</div>
					<div class="row">
						<div class="column">
							<button onclick="location.href='<?php echo Helper::baseUrl() ?>';" class="btn submit">
								<span class="fa fa-home"></span>
							</button>
						</div>
						<div class="column">
							<button type="submit" class="btn submit">
								<span class="fa fa-sign-in"></span>
							</button>
						</div>
					</div>
				</form>
				<a href="#" class="bottom-text-w3ls"></a>
			</div>
		</div>
		<!-- //content -->
		<!-- copyright -->
		<div class="copyright">
			<h2>&copy; Copyright ©2022 All rights reserved | UNIVERSITAS PERTAHANAN REPUBLIK INDONESIA
				<a href="https://www.idu.ac.id/" target="_blank">www.idu.ac.id</a>
			</h2>
		</div>
		<!-- //copyright -->
	</div>

	<script>
		function validate() {
			var $valid = true;
			document.getElementById("user_info").innerHTML = "";
			document.getElementById("password_info").innerHTML = "";

			var userName = document.getElementById("user_name").value;
			var password = document.getElementById("password").value;
			if (userName == "") {
				document.getElementById("user_info").innerHTML = "required";
				$valid = false;
			}
			if (password == "") {
				document.getElementById("password_info").innerHTML = "required";
				$valid = false;
			}
			return $valid;
		}
	</script>
</body>

</html>