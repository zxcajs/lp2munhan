<?php
	Helper::checkPage();
	$data_bantuan_akademik = $bantuan_akademik->tampil_bantuan_akademik();
?>

<section class="content-header">
	<h1>
		Data Bantuan Akademik LPPM Unhan RI
		<small> </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
			<div class="add-table">

               <a type="button" class="btn btn-xs btn-primary btn-flat" href="index.php?halaman=tambah_bantuan">
                <i class="fa fa-plus-square"></i> Tambah Data</a> 
              </div>
				<div class="panel-body">
					<div class="inner">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center w-70">No</th>
									<th>Nama </th>
									<th>Judul Penelitian</th>
									<th>Prodi</th>
									<th>Fakultas</th>
									<th class="text-center">Periode</th>
									<th class="text-center">Tanggal</th>
									<th class="text-center">Tahun Kelulusan</th>
									<th class="text-center">Jumlah Dana</th>
									<th class="text-center w-150">OPSI</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data_bantuan_akademik as $key => $value) : ?>
									<tr>
										<td class="text-center"><?php echo $key+1 ?></td>
										<td><?php echo $value['nama']; ?></td>
										<td><?php echo $value['judul_penelitian']; ?></td>
										<td><?php echo $value ['prodi'];?></td>
										<td><?php echo $value ['fakultas']; ?></td>
										<td class="text-center"><?php echo $value ['periode'];?></td>
										<td class="text-center"><?php echo $value ['tanggal'];?></td>
										<td class="text-center"><?php echo $value ['tahun_lulus'];?></td>
										<td class="text-center"><?php echo $value ['jumlah'];?></td>

										<td class="text-center">
											<a href="index.php?halaman=ubah_bantuan&id_bantuan=<?php echo $value['id_bantuan']; ?>" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Ubah</a>
											<a href="index.php?halaman=hapus_bantuan&id_bantuan=<?php  echo $value['id_bantuan'] ?>" class="btn btn-xs btn-danger" onclick="return confirm('hapus data ?')" ><i class="fa fa-trash"></i> Hapus</a>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						
					</div>
				</div>
			</div>
		</div>
	</section>
