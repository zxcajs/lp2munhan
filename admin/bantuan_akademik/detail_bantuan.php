<!DOCTYPE html>
<html>
<head>
	<title>detail</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>

<?php Helper::checkPage();?>
<?php 
$id_bantuan = $_GET['id_bantuan'];
$data_bantuan = $bantuan->detail_bantuan_akademik($id_bantuan);

 ?>


 <div class="inner">
 <div class="panel panel-primary">
 <div class="panel-heading text-center">
 <h2>Data Mahasiswa</h2>
 </div>

 <div class="panel-body">
 	<div class="col-md-6">
 	<table class="table table-striped">
 		<tr>
 			<th>Nama </th>
 			<th><?php echo $data_bantuan['nama']; ?></th>
 		</tr>
 		<tr>
 			<th>Judul Penelitian</th>
 			<th><?php echo $data_bantuan['judul_penelitian'];?></th>
 		</tr>
 		<tr>
 			<th>Prodi</th>
 			<th><? echo $data_bantuan['prodi'];?></th>
 		</tr>
 		<tr>
 			<th>Fakultas</th>
 			<th><? echo $data_bantuan['fakultas'];?></th>
 		</tr>
 		<tr>
 			<th>Periode</th>
 			<th><? echo $data_bantuan['periode'];?></th>
 		</tr>
 		<tr>
 			<th>Tanggal</th>
 			<th><? echo $data_bantuan['tanggal'];?></th>
 		</tr>
 		<tr>
 			<th>Tahun Lulus</th>
 			<th><? echo $data_bantuan['tahun_lulus'];?></th>
 		</tr>
 		<tr>
 			<th>Jumlah</th>
 			<th><? echo $data_bantuan['jumlah'];?></th>
 		</tr>
 	
 	</table>
 	</div>
 </div>
 </div>
 	<a href="index.php?halaman=tampil_pimpinan" class="btn btn-danger">Kembali</a>
 </div>
</body>
</html>