<?php Helper::checkPage();?>
<section class="content-header">
	<h1>
	Tambah Data 
		<small> | Data Bantuan  </small>
	</h1>
</section>
<?php 
	// objek pasien menjalankan fungsi tampil_pasien
	$refProdi = $refData->getProdiList();
 ?>
<section class="inner">
	<div class="form-body">
		<div class="box">
				<div class="panel-body">
					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nama </label>
							<input type="" name="nama" class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Judul Penelitian </label>
							<input type="" name="judul_penelitian" class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Prodi </label>
							<select onchange="getval(this);" class="form-control js-example-basic-single" name="prodi" id="prodi">
								<option value="">pilih</option>
								<?php 
									foreach ($refProdi as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
						<div class="form-group">
							<label>Fakultas </label>
							<input type="hidden" maxlength="4" name="fakultas" class="form-control" required="">
							<input type="text" readonly maxlength="4" name="fakultas_name" class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Periode </label>
							<input type="text" maxlength="4" name="periode" class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Tanggal </label>
							<input type="date" value="<?php date('Y-m-d')?>" name="tanggal" class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Tahun Lulus </label>
							<input type="text" maxlength="4" name="tahun_lulus" class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Jumlah </label>
							<input type="number" name="jumlah" class="form-control" required="">
						</div>
												
						<a href="index.php?halaman=tampil_bantuan" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
						<button type="submit" name="simpan" class="btn btn-xs btn-primary btn-flat "> <i class="fa fa-save"></i> Simpan</button>
					</form>
				</div>
			</div>
		</div>
	</section>

	<?php 
	if (isset($_POST['simpan'])) 
	{
		$bantuan_akademik->simpan_bantuan_akademik(
			$_POST['nama'],
			$_POST['judul_penelitian'],
			$_POST['prodi'],
			$_POST['fakultas'],
			$_POST['periode'],
			$_POST['tanggal'],
			$_POST['tahun_lulus'],
			$_POST['jumlah']
		);
		echo "<script>alert('data tersimpan')</script>";
		echo "<script>location='index.php?halaman=tampil_bantuan'</script>";
	}
	?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>
			function getval(sel)
			{
				$.ajax({
					type: "POST",
					url: 'ref_data/refdata.php',
					data: {
						'id': sel.value,
						'action': 'getFakultasId'
					},
					dataType: 'json',
					success: function(response)
					{
						$("[name='fakultas']").val(response.fakultas_id);
						$("[name='fakultas_name']").val(response.fakultas_name);
					}
				});
			}
		</script>