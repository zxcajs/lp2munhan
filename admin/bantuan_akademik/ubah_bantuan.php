<?php Helper::checkPage();?>
<section class="content-header">
	<h1>
		Ubah 
		<small> |  Data Bantuan </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
				<div class="panel-body">
					<?php 
					$id_bantuan = $_GET['id_bantuan'];
					$data_bantuan = $bantuan_akademik->detail_bantuan_akademik($id_bantuan);
					$refProdi = $refData->getProdiList();
					
					?>

					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label> Nama  </label>
							<input type="" class="form-control" name="nama" value="<?php echo $data_bantuan['nama'] ?>">
						</div>
						<div class="form-group">
							<label> Judul_Penelitian </label>
							<input type="" class="form-control" name="judul_penelitian" value="<?php echo $data_bantuan['judul_penelitian'] ?>">
						</div>
						<div class="form-group">
							<label>Prodi </label>
							<select onchange="getval(this);" class="form-control js-example-basic-single" name="prodi" id="prodi">
								<option value="">pilih</option>
								<?php 
									foreach ($refProdi as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
						<div class="form-group">
							<label>Fakultas </label>
							<input type="hidden" maxlength="4" name="fakultas" class="form-control" value="<?php echo $data_bantuan['ref_fakultas_id'] ?>" required="">
							<input type="text" readonly maxlength="4" name="fakultas_name" class="form-control" value="<?php echo $data_bantuan['fakultas'] ?>" required="">
						</div>
						<div class="form-group">
							<label>Periode </label>
							<input type="text" maxlength="4" name="periode" class="form-control" required="" value="<?php echo $data_bantuan['periode'] ?>">
						</div>
						<div class="form-group">
							<label>Tanggal </label>
							<input type="date" name="tanggal" class="form-control" required="" value="<?php echo $data_bantuan['tanggal'] ?>">
						</div>
						<div class="form-group">
							<label>Tahun Lulus </label>
							<input type="text" maxlength="4" name="tahun_lulus" class="form-control" value="<?php echo $data_bantuan['tahun_lulus'] ?>" required="">
						</div>
						<div class="form-group">
							<label>Jumlah </label>
							<input type="number" name="jumlah" class="form-control" value="<?php echo $data_bantuan['jumlah_dana'] ?>" required="">
						</div>
						<a href="index.php?halaman=tampil_bantuan" class="btn btn-xs btn-danger"><i class="fa fa-backward"></i> Kembali</a>
						<button class="btn btn-xs btn-primary" name="simpan"><i class="fa fa-save"></i> Simpan</button>

					</form>
				</div>
			</div>
		</div>
	</section>
	<?php if (isset($_POST['simpan'])) 
	{
		$bantuan_akademik->ubah_bantuin_akademik(
			$_POST['nama'],
			$_POST['judul_penelitian'],
			$_POST['prodi'],
			$_POST['fakultas'],
			$_POST['periode'],
			$_POST['tanggal'],
			$_POST['tahun_lulus'],
			$_POST['jumlah'],
			$_GET['id_bantuan']
		);

	echo "<script>alert('data tersimpan')</script>";
		echo "<script>location='index.php?halaman=tampil_bantuan'</script>";
	}

	?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>
		function getval(sel)
		{
			$.ajax({
				type: "POST",
				url: 'ref_data/refdata.php',
				data: {
					'id': sel.value,
					'action': 'getFakultasId'
				},
				dataType: 'json',
				success: function(response)
				{
					$("[name='fakultas']").val(response.fakultas_id);
					$("[name='fakultas_name']").val(response.fakultas_name);
				}
			});
		}
	</script>
	<script>
		$('select[name=prodi]').val(<?php echo $data_bantuan['ref_prodi_id'] ?>).change();
	</script>