<?php Helper::checkPage();?>
<!DOCTYPE html>
<html>
<head>
	<title>detail</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>

<?php 
//mengambil id laporan dosen dari url
$id_laporan_dosen = $_GET['id_laporan_dosen'];
$datadosen = $dosen->detail_dosen($id_dosen);
 ?>
 
 
<div class="inner">
	<div class="panel panel-primary">
 	<div class="panel-heading text-center">
 		<h2>Detail DATA DOSEN</h2>
 	</div>
 	<div class="panel-body">
 	
 	<div class="col-md-50">
 		<table class="table table-striped">
 			<tr>
 				<th>NAMA  DOSEN</th>
 				<th><?php echo $datadosen['nama_dosen']; ?></th>
 			</tr>
 			<tr>
 				<th>JABATAN</th>
 				<th><?php echo $datadosen['nrp']; ?></th>
 			</tr>
 			<tr>
 				<th>POSISI</th>
 				<th><?php echo $datadosen['pangkat']; ?></th>
 			</tr>
 			<tr>
 				<th>PRODI</th>
 				<th><?php echo $datadosen ['jabatan']; ?></th>
 			</tr>
 			<tr>
 				<th>FAKULTAS/th>
 				<th><?php echo $datadosen['jabatan_akademik']; ?></th>
 			</tr>
 			<tr>
 				<th>JUDUL PENELITIAN</th>
 				<th><?php echo $datalit['judul_penelitian']; ?></th>
 			</tr>
 			<tr>
 				<th>AUTHOR/RESPONDING AUTHOR</th>
 				<th><?php echo $datalit['author']; ?></th>
 			</tr>
 			<tr>
 				<th>PUBLISHER</th>
 				<th><?php echo $datalit['publiser']; ?></th>
 			</tr>
 			<tr>
 				<th>TAHUN TERBIT</th>
 				<th><?php echo $datadosen['terbit']; ?></th>
 			</tr>	
 			<tr>
 				<th>AKUN GOOGLE SCHOLAR</th>
 				<th><?php echo $datadosen['url']; ?></th>
 			</tr>
 			<tr>
 				<th>JUMLAH PENELITIAN</th>
 				<th><?php echo $datadosen['jumlah_dosen']; ?></th>
 			</tr>
 			
 		</table>
 	</div>
 		
 	</div>
 </div>
<a href="index.php?halaman=dosen" class="btn btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
</div>
 </body>
</html>