<?php Helper::checkPage();?>
<?php
$data_laporan_dosen = $dosen->tampil_dosen();
?>

<section class="content-header">
	<h1>
	Data Laporan Dosen
		<small> </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
				<div class="panel-body">
					<div class="inner">
						<table class="table table-bordered" id="thetable">
							<thead>
								<tr>
								
									<th class="text-center w-70">No</th>
									<th>Nama Dosen</th>
									<th>Fakultas</th>
									<th>Prodi</th>
									<th class="text-center">Jumlah_Penelitian</th>
									<th class="text-center">Jumlah_Publikasi</th>
									<th class="text-center w-70">Opsi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data_laporan_dosen as $key => $value) : ?>
									<tr>
										<td class="text-center"><?php echo $key+1 ?></td>
										<td><?php echo $value['nama_dosen'] ?></td>
										<td><?php echo $value['fakultas']; ?></td>
										<td><?php echo $value['prodi'] ?></td>
										<td class="text-center"><?php echo $value['jml_lit']; ?></td>
										<td class="text-center"><?php echo $value['jml_publish']; ?></td>
										<td class="text-center">
											
											<a href="index.php?halaman=detail_dosen&id_dosen=<?php echo $value['id_dosen']; ?>" class="btn btn-xs btn-info"><i class="fa fa-eye"></i> Detail</a>
										</td>
										
										
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						
					</div>
				</div>
			</div>
		</div>
	</section>
