<!DOCTYPE html>
<html lang="en">
<head>
<title>SIM LPPM UNHAN RI</title>
<!-- Meta tag Keywords -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="SIM LPPM UNHAN RI" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Meta tag Keywords -->
<!-- css files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="css/font-awesome.css"> <!-- Font-Awesome-Icons-CSS -->
<!-- //css files -->
<!-- online-fonts -->
<!-- <link href="//fonts.googleapis.com/css?family=Cedarville+Cursive" rel="stylesheet"> -->
<!-- <link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext" rel="stylesheet"> -->
<!--//online-fonts -->
</head>
<body>
<div class="header">
	<!-- <h1>SIM LPPM UNHAN RI</h1> -->
</div>
<div class="w3-main">
	<div class="agile-info">
		<!-- <h2>404</h2> -->
		<h3>ooops!</h3>
		<p>Halaman yang anda cari tidak ditemukan.</p>
		<div id="walker"></div>
	</div>
	
	
</div>
<div class="footer-w3l">
	<p>&copy; 2022 All rights reserved | UNIVERSITAS PERTAHANAN REPUBLIK INDONESIA
				<a href="https://www.idu.ac.id/" target="_blank">www.idu.ac.id</a></p>
</div>
<!--/animation scripts -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script  src="js/index.js"></script>
<!--//animation scripts -->
</body>
</html>