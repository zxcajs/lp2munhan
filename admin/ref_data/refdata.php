<?php 
include '../../config/konek_database.php';
include '../../config/helper.php';

if(isset($_POST['action'])){
    
    switch ($_POST['action'])
    {
        case 'getFakultasId':
            $prodiId = $_POST['id'];
            $id = $refData->getFakultasId($prodiId);
            $name = $refData->getFakultasName($id);
            $response['fakultas_id'] = $id;
            $response['fakultas_name'] = $name;
        break;
        case 'getDataRekapitulasi':
            $tahun = $_POST['tahun'];
            $response['data'] = $rekapitulasi->getRekapitulasi($tahun);
            $response['dataGrafik'] = $grafik->grafikPenelitianAll($tahun);
            $response['dataGrafikByTahun'] = $grafik->grafikPenelitianAllByTahun($tahun);
            $response['dataGrafikByProdi'] = $grafik->grafikPenelitianAllByProdi($tahun);
        break;
    }
    
    echo json_encode($response);
}
?>