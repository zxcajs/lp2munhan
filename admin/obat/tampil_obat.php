<!DOCTYPE html>
<html>
<head>
	<title>obat</title>
</head>


<body>

<section class="content-header">
 	<h1>
 		Data Staff LPPM 
 		<small> </small>
 	</h1>
 </section>
 <section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
			<div class="add-table">

               <a type="button" class="btn btn-primary btn-flat" href="index.php?halaman=tambah_obat">
               		 <i class="fa fa-plus-square"></i> Tambah Data </a> 
              			</div>
 							<div class="panel-body">


										<?php 
											$data_obat = $obat->tampil_obat();
										?>

									<table class="table table-bordered">
									<thead>
											<tr>
												<th>No</th>
												<th>Nama </th>
												<th>Pangkat </th>
												<th>Jabatan </th>
												<th>Opsi</th>
											</tr>
										</thead>
									<tbody>
											<?php foreach ($data_obat as $key => $value) : ?>
												<tr>
													<td><?php echo $key+1 ?></td>
													<td><?php echo $value['nama_obat'] ?></td>
													<td><?php echo $value['pangkat'] ?></td>
													<td><?php echo $value ['jabatan']?></td>
												<td><a href="index.php?halaman=ubah_obat&id_obat=<?php echo $value['id_obat'] ?>" class="btn btn-warning">Ubah</a>
													<a href="index.php?halaman=hapus_obat&id_obat=<?php echo $value['id_obat'] ?>" class="btn btn-danger" onclick="return confirm('hapus data ?')" >Hapus</a>
												</td>
												</tr>
											<?php endforeach  ?>
									</tbody>
								</table>
	
						</div>
				</div>
			</div>
		</section>
</body>
</html>