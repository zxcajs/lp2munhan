<?php 
$site_path = realpath(dirname(__FILE__));

/*
     Mendefenisikan lokasi dari aplikasi
 */
define('__SITE_PATH', $site_path);
?>
<?php include '../config/konek_database.php'; ?>
<?php include '../config/app.config.php' ?>
<?php include '../config/helper.php' ?>
<?php 

if(empty($_SESSION["user_id"])) {
	if(isset($_SESSION["errorMessage"])&&!empty($_SESSION["errorMessage"])){
		$param = "?error=".$_SESSION["errorMessage"];
	}
	
	$HeadTo=Helper::baseUrl().'admin/login/login.php'.$param;

	Header("Location: ".$HeadTo);
}

?>

<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> SIM LPPM UNHAN RI</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../dist/css/admin.css">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
</head>

<style type="text/css" media="screen">
	.sidebar-menu {
		margin-top: -2%;
	}
	.text-center {
		text-align:center;
	}
	.text-right {
		text-align:right;
	}
	section > h1 {
		text-transform: uppercase;
	}
	tr > th {
		text-transform: uppercase;
	}
	.w-150 {
		width:150px;
	}
	.w-70 {
		width:70px;
	}
	.w-100 {
		width:100px;
	}
	.w-220 {
		width:220px;
	}
	.w-290 {
		width:290px;
	}
</style>
<body class="hold-transition skin-yellow sidebar-mini ">

	<div class="wrapper">

		<header class="main-header">

			<nav class="navbar navbar-default ">

				<div class="navbar-header">

					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" 
					data-target=".sidebar-collapse" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">		
					<li class="text-center">
						<a href="#" class="logo">
						<b>_______   LPPM UNHAN RI  ________  </b>
						</a>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<aside class="sidebar sidebar-collapse">
		<section class="sidebar">
			<br>
			<div class="text-center">
				<img src="../assets/img/penyakit/W4.png" class="img-square" alt="User Image" class ="img-responsive" width="150">
			</div>		
			<br>
			<ul class="sidebar-menu">
				<?php include 'menu.php' ?>
			</ul>
		</section>
	</aside>
	<section class="content ">
		<div class="inner">
			<!---- percabangan halaman ---->
			<?php include 'routing.php' ?>
		</div>
	</section>

</div>



<script src="../dist/js/jquery.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../dist/js/admin.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
		// $('#thetable').DataTable();
	} );
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
	$(document).ready(function() {
		$('.js-example-basic-multiple').select2();
		$('.js-example-basic-single').select2();
	});
</script>


</body>
</html>