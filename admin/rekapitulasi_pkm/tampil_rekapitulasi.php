
 <?php Helper::checkPage();?>
 <!DOCTYPE html>
<html>
<head>
	<title> REKAPITULASI</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>
<div class="inner">
	<div class="panel panel-primary">
 	<div class="panel-heading text-center">
 		<h2> DATA REKAPITULASI </h2>
 	</div>
 	<div class="panel-body">
 	
 	<div class="col-md-6">
 		<table class="table table-striped" id="thetable" >
 			

 			<tr>
 				<th>TAHUN </th>
 				<th><select class="form-control" name="combo1" id="combo1">
					<option value="">Pilih</option>
					<option value="Nama Provinsi 1">keseluruhan</option>
					<option value="Nama Provinsi 1">2022</option>
					<option value="Nama Provinsi 2">2021</option>
					<option value="Nama Provinsi 3">2020</option>
					<option value="Nama Provinsi 4">2019</option>
					</select>
				</th>
 			</tr>
 			<tr>
 				<th>JUMLAH PENELITIAN</th>
 				<th></th>
 			</tr>
 			<tr>
 				<th>JUMLAH PUBLIKASI ARTIKEL</th>
 				<th></th>
 			</tr>
 			<tr>
 				<th>JUMLAH JURNAL NASIONAL AKREDITASI</th>
 				<th></th>
 			</tr>
 			<tr>
 				<th>JUMLAH JURNAL NASIONAL NON AKREDITASI</th>
 				<th></th>
 			</tr>
 			<tr>
 				<th>JUMLAH JURNAL INTERNATIONAL REPUTASI </th>
 				<th></th>
 			</tr>
 			<tr>
 				<th>JUMLAH JURNAL INTERNATIONAL NON REPUTASI </th>
 				<th></th>
 			</tr>
 			<tr>
 				<th>MEMPUNYAI AKUN GOOGLE SCHOLAR</th>
 				<th></th>
 			</tr>
 			<tr>
 				<th>TIDAK MEMPUNYAI AKUN GOOGLE SCHOLAR</th>
 				<th></th>
 			</tr>
 			
 		</table>
 	</div>
 	
 	</div>
 </div>
<a href="index.php?halaman=tampil_prodi" class="btn btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
</div>

</body>
</html>