<?php Helper::checkPage();?>
<!DOCTYPE html>
<html>
<head>
	<title>DETAIL DATA REKAPITULASI</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>

<?php 
//mengambil id pasien dari url
$id_dosen = $_GET['id_rekapitulasi'];
$datadosen = $prodi->get_name_fakultas($id_dosen);
$koneksi = mysqli_connect("localhost", "root", "","cff");
$jml_lit = mysqli_query($koneksi, "SELECT jml_lit_prodi FROM prodi");
$name_prod = mysqli_query($koneksi, "SELECT id_prodi FROM prodi");
 ?>
 
 
<div class="inner">
	
	<div class="panel panel-primary">
 	<div class="panel-heading text-center">
 		<h2>DATA FAKULTAS</h2>
 	</div>
 	<div class="panel-body">
 		
		<div class="col-md-6">
	 		<table class="table table-striped">
	 			<tr>
		 			<tr>
		 				
		 				<th class="text-center"><?php echo $datadosen['nama_fakultas']; ?></th>
		 			</tr>
		 			<tr>
		 				
		 				<th>_____________________________________________________________</th>
		 			</tr>
		 			
		 		
		 			<tr>
		 				<th>Responding Author Tim</th>
		 			
		 			</tr>
		 			<tr>
		 				<th>Responding Author Mandiri</th>
		 				
		 			</tr>
		 			<tr>
		 				<th>Responding Author Mahasiswa</th>
		 			</tr>
		 			<tr>
		 				<th>Jurnal Nasional Terakreditasi</th>
		 		
		 			</tr>
		 			<tr>
		 				<th>Jurnal Nasional Non Akreditasi</th>
		 		
		 			</tr>
		 			<tr>
		 				<th>Jurnal International Bereputasi</th>
		 			
		 			</tr>	
		 			<tr>
		 				<th>Jurnal International Non Reputasi</th>
		 			
		 			</tr>
		 				<tr>
		 				<th>Mempunyai Akun Google Scholar</th>
		 		
		 			</tr>
		 			<tr>
		 				<th>Tidak Mempunyai Akun Google Scholar</th>
		 			
		 			</tr>
		 			<tr>
		 				<th>Tahun</th>
		 				<th></th>
		 			</tr>
		 			<tr>
		 				<th>__________________</th>
		 				<th>_____________________________________</th>
		 			</tr>
		 			<tr>
		 				<th>JUMLAH PENULIS/ DOSEN</th>
		 			
		 			</tr>
		 			<tr>
		 				<th>JUMLAH KESELURUHAN FAKULTAS</th>
		 			
		 			</tr>							
			 	</tr>
	 		</table>
 		</div>

 		<div class="col-md-6">
	 		<canvas id="myPieChart"></canvas>
				<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3"></script>
				<script>
				  var mychart = document.getElementById("myPieChart").getContext('2d');
				  let round_graph = new Chart(mychart, {
				    type: 'bar',
				    data: {
				      labels: [<?php while ($b = mysqli_fetch_array($name_prod)) { echo '"' . $b['id_prodi'] . '",';}?>],
				          datasets: [
					          	{
					            label: "Data Penlitian Dosen",
					            data: [<?php while ($p = mysqli_fetch_array($jml_lit)) { echo '"' . $p['jml_lit_prodi'] . '",';}?>],
						            backgroundColor: [
						              '#29B0D0',
						              '#2A516E',
						              '#F07124',
						              '#CBE0E3',
						              '#979193'
						            ]
					          	}
				          	]
				    			}
				  			})
				</script>
		</div>
 		
 	</div>
 </div>
<a href="index.php?halaman=dosen" class="btn btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
</div>
 </body>
</html>

