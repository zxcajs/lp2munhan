
<?php Helper::checkPage();?>
<!DOCTYPE html>
<html>
<head>
	<title> REKAPITULASI</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>

<section class="content-header">
 	<h1>
 		DATA KESELURUHAN
 		<small> </small>
 	</h1>
 </section>
 <section class="inner">
	<div class="form-body wow fadeIn animated">
	<div class="box">
	<div class="add-table">

              <a type="button" class="btn btn-primary btn-flat" href="index.php?halaman=tampil_rekapitulasi_1">
                <i class="fa "></i> JUMLAH DATA REKEPITULASI</a> 
              </div>
 		<div class="panel-body">


<?php 
// objek pasien menjalankan fungsi tampil_pasien
$prodi = $prodi->get_fakultas();
 ?>



	<table class="table table-bordered">
	<thead>
		<tr>
			<th>NO </th>			
			<th>FAKULTAS</th>
			<th>JUMLAH DOSEN/PENULIS</th>
			<th>JUMLAH PENELITIAN</th>
			<th>JUMLAH PUBLIKASI ARTIKEL</th>
			<th>Opsi</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($prodi as $key => $value) :?>
		<tr>
			<td><?php echo $key+1 ?></td>
		
			<td><?php echo $value['nama_fakultas'] ?></td>
			
			<td></td>
				<td><?php echo $value['jml_lit_fakultas'] ?></td>
			<td>										</td>
			

			<td>
			
			<a href="index.php?halaman=detail_rekapitulasi&id_rekapitulasi=<?php echo $value['id_fakultas']; ?>" class="btn btn-primary">Detail</a>	

			<a href="index.php?halaman=hapus_rekapitulasi&id_rekapitulasi=<?php echo $value['id_fakultas']; ?>" class="btn btn-danger" onclick="return confirm('hapus data ?')" >Hapus</a>
			</td>
			
		</tr>
		<?php endforeach ?>
	</tbody>
</table>

	
</div>
</div>
</div>
</section>
</body>
</html>