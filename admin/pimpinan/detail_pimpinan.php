<?php Helper::checkPage();?>
<!DOCTYPE html>
<html>
<head>
	<title>detail</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>

<?php 
$id_pimpinan = $_GET['id_pimpinan'];
$data_pimpinan = $pimpinan->detail_pimpinan($id_pimpinan);

 ?>


 <div class="inner">
 <div class="panel panel-primary">
 <div class="panel-heading text-center">
 <h2>Detail Pimpinan</h2>
 </div>

 <div class="panel-body">
 	<div class="col-md-6">
 		<img src="/assets/img/pimpinan/<?php echo $data_pimpinan ['foto_pimpinan'] ?>" class="img-responsive" width="250">
 	</div>

 	<div class="col-md-6">
 	<table class="table table-striped">
 		<tr>
 			<th>Nama Pimpinan</th>
 			<th><?php echo $data_pimpinan['nama_pimpinan']; ?></th>
 		</tr>
 		<tr>
 			<th>NIP/NRP</th>
 			<th><?php echo $data_pimpinan['nomor']; ?></th>
 		</tr>
 		<tr>
 			<th>Pangkat</th>
 			<th><?php echo $data_pimpinan['pangkat']; ?></th>
 		</tr>
 		<tr>
 			<th>Jabatan</th>
 			<th><?php echo $data_pimpinan['jabatan']; ?></th>
 		</tr>
 		
 	</table>
 	</div>
 </div>
 </div>
 	<a href="index.php?halaman=tampil_pimpinan" class="btn btn-xs btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
 </div>
</body>
</html>