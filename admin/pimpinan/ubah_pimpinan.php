<?php Helper::checkPage();?>
<section class="content-header">
	<h1>
		Ubah 
		<small> |  Data Dosen </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
			
				<div class="panel-body">
					<?php 
					$id_pimpinan = $_GET['id_pimpinan'];
					$data_pimpinan = $pimpinan->detail_pimpinan($id_pimpinan);
					$refPangkat = $refData->getPangkatList();
					$refJabatan = $refData->getJabatanList();
					?>

					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label> Nama Pimpinan </label>
							<input type="" class="form-control" name="nama_pimpinan" value="<?php echo $data_pimpinan['nama_pimpinan'] ?>">
						</div>
						<div class="form-group">
							<label> Pangkat </label>
							<select class="form-control js-example-basic-single" name="pangkat" id="pangkat">
								<option value="">pilih</option>
								<?php 
									foreach ($refPangkat as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
						<div class="form-group">
							<label> Jabatan </label>
							<select class="form-control js-example-basic-single" name="jabatan" id="jabatan">
								<option value="">pilih</option>
								<?php 
									foreach ($refJabatan as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
						<div class="form-group">
							<label> NIDN/NRP </label>
							<input type="" class="form-control" name="nomor" value="<?php echo $data_pimpinan['nomor']?>">
						</div>
						<div class="form-group">
							<label>Foto</label>
							<img src="../assets/img/pimpinan/<?php echo $data_pimpinan['foto_pimpinan'];  ?>" width="120 px">
							<input type="file" class="form-control" name="foto_pimpinan">
						</div>


						<a href="index.php?halaman=tampil_pimpinan" class="btn btn-xs btn-danger"><i class="fa fa-close"></i> Batal</a>
						<button class="btn btn-xs btn-primary" name="simpan"><i class="fa fa-save"></i> Simpan</button>

					</form>
				</div>
			</div>
		</div>
	</section>
	<?php if (isset($_POST['simpan'])) 
	{
		$pimpinan->ubah_pimpinan($_POST['nama_pimpinan'],$_POST['pangkat'],$_POST['jabatan'], $_POST['nomor'], $_FILES['foto_pimpinan'],$id_pimpinan);

	//echo "<script>alert('data pasien tersimpan')</script>";
		echo "<script>location='index.php?halaman=tampil_pimpinan'</script>";
	}

	?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>
		$('select[name=pangkat]').val(<?php echo $data_pimpinan['ref_pangkat_id'] ?>).change();
		$('select[name=jabatan]').val(<?php echo $data_pimpinan['ref_jabatan_id'] ?>).change();
	</script>