<?php Helper::checkPage();?>
<?php
$data_pimpinan = $pimpinan->tampil_pimpinan(); 
?>


<section class="content-header">
	<h1>
		Data Pimpinan
		<small> </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
				<div class="panel-body">
					<div class="inner">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center w-70">No</th>
									<th>Nama Pimpinan</th>
									<th>Jabatan</th>
									<th>Pangkat</th>
									<th class="text-center w-150">Opsi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data_pimpinan as $key => $value) : ?>
									<tr>
										<td class="text-center"><?php echo $key+1 ?></td>
										<td><?php echo $value['nama_pimpinan']; ?></td>
										<td><?php echo $value ['jabatan']; ?></td>
										<td><?php echo $value ['pangkat']; ?></td>
										<td class="text-center">
											<a href="index.php?halaman=detail_pimpinan&id_pimpinan=<?php echo $value ['id_pimpinan']; ?>" class="btn btn-xs btn-info"><i class="fa fa-eye"></i>&nbsp;Detail</a>
											<a href="index.php?halaman=ubah_pimpinan&id_pimpinan=<?php echo $value['id_pimpinan']; ?>" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>&nbsp;Ubah</a>
											
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						
					</div>
				</div>
			</div>
		</div>
	</section>
