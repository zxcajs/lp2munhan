<?php Helper::checkPage();?>
<section class="content-header">
	<h1>
	Tambah Data 
		<small> | Data Pimpinan  </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body">
		<div class="box">
			
				<div class="panel-body">


					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nama Pimpinan</label>
							<input type="" name="nama_pimpinan" class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Pangkat</label>
							<textarea class="form-control"  name="deskripsi_pimpinan" required="" ></textarea>

						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<textarea class="form-control"  name="pencegahan_pimpinan" required="" ></textarea>

						</div>
						<div class="form-group">
							<label>NIDN/NRP</label>
							<textarea class="form-control"  name="nama_obat" required="" ></textarea>

						</div>
						<div class="form-group">
							<label>Foto</label>
							<input type="file" name="foto_pimpinan" class="form-control" required="">
						</div>
						<button class="btn btn-primary" name="simpan">simpan</button>
					</form>
				</div>
			</div>
		</div>
	</section>

	<?php 
	if (isset($_POST['simpan'])) 
	{
		$pimpinan->simpan_pimpinan($_POST['nama_pimpinan'],$_POST['deskripsi_pimpinan'],$_POST['pencegahan_pimpinan'],$_POST['nama_obat'],$_FILES['foto_pimpinan']);

		echo "<script>alert('pimpinan tersimpan')</script>";
		echo "<script>location='index.php?halaman=tampil_pimpinan'</script>";
	}
	?>