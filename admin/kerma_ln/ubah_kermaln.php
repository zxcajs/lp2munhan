<?php Helper::checkPage();?>
<section class="content-header">
	<h1>
		 Kerjasama Luar Negeri
		<small> |  Merubah Data  </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
			
				<div class="panel-body">
					<?php 
					$id_kermaln = $_GET['id_kermaln'];
					$data_kermaln = $kerma_ln->detail_kermaln($id_kermaln);
					?>

					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label> Institusi/Kementrian  </label>
							<input type="" class="form-control" name="nama_kerma" value="<?php echo $data_kermaln['nama_kerma'] ?>">
						</div>
						<div class="form-group">
							<label> Bidang </label>
							<input type="" class="form-control" name="bidang_kerma" value="<?php echo $data_kermaln['bidang_kerma'] ?>">
						</div>
						<div class="form-group">
							<label> Tanggal </label>
							<input type="date" class="form-control" name="tanggal" value="<?php echo $data_kermaln['tanggal']?>"  value="<?date('Y-m-d')?>">
						</div>
						
						<a href="index.php?halaman=tampil_kermaln" class="btn btn-xs btn-danger"><i class="fa fa-backward"></i> Kembali</a>
						<button class="btn btn-xs btn-primary" name="simpan"><i class="fa fa-save"></i> Simpan</button>

					</form>
				</div>
			</div>
		</div>
	</section>
	<?php if (isset($_POST['simpan'])) 
	{
		$kerma_ln->ubah_kermaln($_POST['nama_kerma'],$_POST['bidang_kerma'],$_POST['tanggal'],$id_kermaln);

	echo "<script>alert('data tersimpan')</script>";
		echo "<script>location='index.php?halaman=tampil_kermaln'</script>";
	}

	?>