<?php Helper::checkPage();?>
<?php
$data_kermaln = $kerma_ln->tampil_kermaln(); 
?>


<section class="content-header">
	<h1>
		Kerja Sama LPPM UNHAN RI
		<small> Luar Negeri </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
			<div class="add-table">

               <a type="button" class="btn btn-xs btn-primary btn-flat" href="index.php?halaman=tambah_kermaln">
                <i class="fa fa-plus-square"></i> Tambah Data</a> 
              </div>
				<div class="panel-body">
					<div class="inner">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Instansi/ Kementrian</th>
									<th>Bidang Kerja Sama</th>
									<th>Tanggal</th>
									<th>Opsi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data_kermaln as $key => $value) : ?>
									<tr>
										<td class="text-center w-70"><?php echo $key+1 ?></td>
										<td><?php echo $value['nama_kerma']; ?></td>
										<td><?php echo $value['bidang_kerma']; ?></td>
										<td class="text-center w-100"><?php echo $value ['tanggal'];?></td>
										<td class="text-center w-150">
											
											<a href="index.php?halaman=ubah_kermaln&id_kermaln=<?php echo $value['id_kermaln']; ?>" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Ubah</a>
											<a href="index.php?halaman=hapus_kermaln&id_kermaln=<?php  echo $value['id_kermaln'] ?>" class="btn btn-xs btn-danger" onclick="return confirm('hapus data ?')" ><i class="fa fa-trash"></i> Hapus</a>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						
					</div>
				</div>
			</div>
		</div>
	</section>
