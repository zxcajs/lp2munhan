
 <?php Helper::checkPage();?>
 <!DOCTYPE html>
<html>
<head>
	<title> Dosen</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>

<section class="content-header">
 	<h1>
 		Data Prodi
 		<small> </small>
 	</h1>
 </section>
 <section class="inner">
	<div class="form-body wow fadeIn animated">
	<div class="box">
	<div class="panel-body">


<?php 
// objek pasien menjalankan fungsi tampil_pasien
$data_prodi = $prodi->tampil_prodi();
 ?>



	<table class="table table-bordered" id="thetable">
	<thead>
		<tr>
			<th>NO </th>			
			<th>PRODI</th>
			<th>FAKULTAS</th>
			<th>JUMLAH PENELITIAN</th>
			<th>JUMLAH PUBLIKSI ARTIKEL</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($data_prodi as $key => $value) :?>
		<tr>
			<td><?php echo $key+1 ?></td>
		
			<td><?php echo $value['nama_prodi'] ?></td>
			<td><?php echo $value['nama_fakultas'] ?></td>
			<td><?php echo $value['jml_lit_prodi'] ?></td>
			<td></td>

			<td>
			
			<a href="index.php?halaman=detail_prodi&id_prodi=<?php echo $value['id_prodi']; ?>" class="btn btn-primary">Detail</a>	
			<a href="index.php?halaman=hapus_prodi&id_prodi=<?php echo $value['id_prodi']; ?>" class="btn btn-danger" onclick="return confirm('hapus data ?')" >Hapus</a>
			</td>
			
		</tr>
		<?php endforeach ?>
	</tbody>
</table>

	
</div>
</div>
</div>
</section>
</body>
</html>