<?php 
    // $me = '/admin';
    // //For get URL PATH
    // $urlpath = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    // switch ($urlpath) {
    //     case $me.'/' :
    //         include "home.php";
    //         break;
    //     case $me.'/organisasi' :
    //         include "organisasi.php";
    //         break;
    //     case $me.'/dosen' :
    //         include 'pasien/pasien.php';
    //         break;
    //     case $me.'/dosen-detail' :
    //         include 'pasien/detail_pasien.php';
    //         break;
    //     case $me.'/dosen-add' :
    //         include 'pasien/tambah_pasien.php';
    //         break;
    //     case $me.'/dosen-edit' :
    //         include 'pasien/ubah_pasien.php';
    //         break;
    //     case $me.'/dosen-delete' :
    //         include 'pasien/hapus_pasien.php';
    //         break;
    //     default:
    //         http_response_code(404);
    //         echo "404";
    //         break;
    // }
    //jika di url tidak ada parameter halaman data
    if(!isset($_GET['halaman']))
    {
            //panggil file home
        include 'home.php';
    }

        //selain itu jika ada parameter halaman
    elseif ($_GET['halaman']=='dashbord') 
    {
        include 'dashbord.php';
    }


    elseif ($_GET['halaman']=='dosen')
    {
        include 'pasien/pasien.php';
    }
    elseif ($_GET['halaman']=='ubah_dosen')
    {
        include 'pasien/ubah_pasien.php';
    }
    elseif ($_GET['halaman']=='tambah_dosen') 
    {
        include 'pasien/tambah_pasien.php';
    }
    elseif ($_GET['halaman']=='hapus_dosen')
    {
        include 'pasien/hapus_pasien.php';
    }
    elseif ($_GET['halaman']=='detail_dosen') 
    {
        include 'pasien/detail_pasien.php';
    }

    elseif ($_GET['halaman']=='tampil_penelitian')
    {
        include 'penyakit/tampil_penyakit.php';	
    }
    elseif ($_GET['halaman']=='tambah_penelitian')
    {
        include 'penyakit/tambah_penyakit.php';
    }
    elseif ($_GET['halaman']=='detail_penelitian') 
    {
        include 'penyakit/detail_penyakit.php';
    }
    elseif ($_GET['halaman']=='hapus_penelitian')
    {
        include 'penyakit/hapus_penyakit.php';
    }
    elseif ($_GET['halaman']=='ubah_penelitian') 
    {
        include 'penyakit/ubah_penyakit.php';
    }
    elseif ($_GET['halaman']=='download_lit') 
    {
        include 'penyakit/download.php';
    }

    elseif ($_GET['halaman']=='tampil_pkm')
    {
        include 'pkm/tampil_pkm.php';	
    }
    elseif ($_GET['halaman']=='tambah_pkm')
    {
        include 'pkm/tambah_pkm.php';
    }
    elseif ($_GET['halaman']=='detail_pkm') 
    {
        include 'pkm/detail_pkm.php';
    }
    elseif ($_GET['halaman']=='hapus_pkm')
    {
        include 'pkm/hapus_pkm.php';
    }
    elseif ($_GET['halaman']=='ubah_pkm') 
    {
        include 'pkm/ubah_pkm.php';
    }


    elseif ($_GET['halaman']=='tampil_pimpinan')
    {
        include 'pimpinan/tampil_pimpinan.php';	
    }
    elseif ($_GET['halaman']=='tambah_pimpinan')
    {
        include 'pimpinan/tambah_pimpinan.php';
    }
    elseif ($_GET['halaman']=='detail_pimpinan') 
    {
        include 'pimpinan/detail_pimpinan.php';
    }
    elseif ($_GET['halaman']=='hapus_pimpinan')
    {
        include 'pimpinan/hapus_pimpinan.php';
    }
    elseif ($_GET['halaman']=='ubah_pimpinan') 
    {
        include 'pimpinan/ubah_pimpinan.php';
    }


    elseif ($_GET['halaman']=='tampil_staff')
    {
        include 'staff/tampil_staff.php';	
    }
    elseif ($_GET['halaman']=='tambah_staff')
    {
        include 'staff/tambah_staff.php';
    }
    elseif ($_GET['halaman']=='detail_staff') 
    {
        include 'staff/detail_staff.php';
    }
    elseif ($_GET['halaman']=='hapus_staff')
    {
        include 'staff/hapus_staff.php';
    }
    elseif ($_GET['halaman']=='ubah_staff') 
    {
        include 'staff/ubah_staff.php';
    }


    elseif ($_GET['halaman']=='tampil_kermadl')
    {
        include 'kerma_dl/tampil_kermadl.php';	
    }
    elseif ($_GET['halaman']=='tambah_kermadl')
    {
        include 'kerma_dl/tambah_kermadl.php';
    }
    elseif ($_GET['halaman']=='detail_kermadl') 
    {
        include 'kerma_dl/detail_kermadl.php';
    }
    elseif ($_GET['halaman']=='hapus_kermadl')
    {
        include 'kerma_dl/hapus_kermadl.php';
    }
    elseif ($_GET['halaman']=='ubah_kermadl') 
    {
        include 'kerma_dl/ubah_kermadl.php';
    }


    elseif ($_GET['halaman']=='tampil_kermaln')
    {
        include 'kerma_ln/tampil_kermaln.php';	
    }
    elseif ($_GET['halaman']=='tambah_kermaln')
    {
        include 'kerma_ln/tambah_kermaln.php';
    }
    elseif ($_GET['halaman']=='detail_kermaln') 
    {
        include 'kerma_ln/detail_kermaln.php';
    }
    elseif ($_GET['halaman']=='hapus_kermaln')
    {
        include 'kerma_ln/hapus_kermaln.php';
    }
    elseif ($_GET['halaman']=='ubah_kermaln') 
    {
        include 'kerma_ln/ubah_kermaln.php';
    }


    elseif ($_GET['halaman']=='tampil_laporan_dosen')
    {
        include 'laporan_dosen/tampil_laporan_dosen.php';	
    }
    
    elseif ($_GET['halaman']=='detail_laporan_dosen') 
    {
        include 'laporan_dosen/detail_laporan_dosen.php';
    }
    


    elseif ($_GET['halaman']=='tampil_prodi')
    {
        include 'prodi/tampil_prodi.php';	
    }
    
    elseif ($_GET['halaman']=='detail_prodi') 
    {
        include 'prodi/detail_prodi.php';
    }
    elseif ($_GET['halaman']=='hapus_prodi') 
    {
        include 'prodi/hapus_prodi.php';
    }
    
    
    elseif ($_GET['halaman']=='tampil_rekapitulasi')
    {
        include 'rekapitulasi/tampil_rekapitulasi_fakultas.php';	
    }
    elseif ($_GET['halaman']=='tampil_rekapitulasi_1')
    {
        include 'rekapitulasi/tampil_rekapitulasi.php';	
    }
    
    elseif ($_GET['halaman']=='detail_rekapitulasi') 
    {
        include 'rekapitulasi/detail_rekapitulasi.php';
    }
    elseif ($_GET['halaman']=='hapus_rekapitulasi') 
    {
        include 'rekapitulasi/hapus_rekapitulasi.php';
    }


    elseif ($_GET['halaman']=='tampil_laporan_dosen')
    {
        include 'laporan_dosen/tampil_laporan_dosen.php';	
    }
    
    elseif ($_GET['halaman']=='detail_laporan_dosen') 
    {
        include 'laporan_dosen/detail_laporan_dosen.php';
    }
    


    elseif ($_GET['halaman']=='tampil_prodi_pkm')
    {
        include 'prodi_pkm/tampil_prodi_pkm.php';	
    }
    
    elseif ($_GET['halaman']=='detail_prodi_pkm') 
    {
        include 'prodi_pkm/detail_prodi_pkm.php';
    }
    elseif ($_GET['halaman']=='hapus_prodi_pkm') 
    {
        include 'prodi_pkm/hapus_prodi_pkm.php';
    }
    
    
    elseif ($_GET['halaman']=='tampil_rekapitulasi_pkm')
    {
        include 'rekapitulasi_pkm/tampil_rekapitulasi_fakultas_pkm.php';	
    }
    elseif ($_GET['halaman']=='tampil_rekapitulasi_1_pkm')
    {
        include 'rekapitulasi_pkm/tampil_rekapitulasi_pkm.php';	
    }
    
    elseif ($_GET['halaman']=='detail_rekapitulasi_pkm') 
    {
        include 'rekapitulasi_pkm/detail_rekapitulasi_pkm.php';
    }
    elseif ($_GET['halaman']=='hapus_rekapitulasi_pkm') 
    {
        include 'rekapitulasi_pkm/hapus_rekapitulasi_pkm.php';
    }

        elseif ($_GET['halaman']=='tampil_laporan_dosen_pkm')
    {
        include 'laporan_dosen_pkm/tampil_laporan_dosen_pkm.php';	
    }
    
    elseif ($_GET['halaman']=='detail_laporan_dosen_pkm') 
    {
        include 'laporan_dosen_pkm/detail_laporan_dosen_pkm.php';
    }


    elseif ($_GET['halaman']=='chart_contoh') 
    {
        include 'chart_penelitian/chart_dosen_2.php';
    }

    

    elseif ($_GET['halaman']=='tampil_gejala')
    {
        include 'gejala/tampil_gejala.php';
    }
    elseif ($_GET['halaman']=='tambah_gejala')
    {
        include 'gejala/tambah_gejala.php';
    }
    elseif ($_GET['halaman']=='hapus_gejala') 
    {
        include 'gejala/hapus_gejala.php';
    }
    elseif ($_GET['halaman']=='ubah_gejala') 
    {
        include 'gejala/ubah_gejala.php';
    }



    elseif ($_GET['halaman']=='tampil_obat')
    {
        include 'obat/tampil_obat.php';
    }
    elseif ($_GET['halaman']=='tambah_obat') 
    {
        include 'obat/tambah_obat.php';
    }
    elseif ($_GET['halaman']=='hapus_obat') 
    {
        include 'obat/hapus_obat.php';
    }
    elseif ($_GET['halaman']=='ubah_obat') 
    {
        include 'obat/ubah_obat.php';
    }

    elseif ($_GET['halaman']=='tampil_pengetahuan') 
    {
        include 'pengetahuan/tampil_pengetahuan.php';
    }
    elseif ($_GET['halaman']=='tambah_pengetahuan') 
    {
        include 'pengetahuan/tambah_pengetahuan.php';
    }
    elseif ($_GET['halaman']=='hapus_pengetahuan') 
    {
        include 'pengetahuan/hapus_pengetahuan.php';
    }
    elseif ($_GET['halaman']=='ubah_pengetahuan') 
    {
        include 'pengetahuan/ubah_pengetahuan.php';
    }
    elseif ($_GET['halaman']=='detail_pengetahuan') 
    {
        include 'pengetahuan/detail_pengetahuan.php';
    }

    elseif ($_GET['halaman']=='proses_diagnosa') 
    {
        include 'proses/proses_diagnosa.php';				
    }
    elseif ($_GET['halaman']=='hitung') 
    {
        include 'proses/hitung.php';
    }

    elseif ($_GET['halaman']=='tampil_rekam_medis')
    {
        include 'rekam_medis/tampil_rekam_medis.php';
    }
    elseif ($_GET['halaman']=='tampil_tabel') 
    {
        include  'rekam_medis/tampil_tabel.php';
    }

    elseif ($_GET['halaman']=='tampil_bantuan')
    {
        include 'bantuan_akademik/tampil_bantuan.php';	
    }
    elseif ($_GET['halaman']=='tambah_bantuan')
    {
        include 'bantuan_akademik/tambah_bantuan.php';
    }
    elseif ($_GET['halaman']=='detail_bantuanb') 
    {
        include 'bantuan_akademik/detail_bantuan.php';
    }
    elseif ($_GET['halaman']=='hapus_bantuan')
    {
        include 'bantuan_akademik/hapus_bantuan.php';
    }
    elseif ($_GET['halaman']=='ubah_bantuan') 
    {
        include 'bantuan_akademik/ubah_bantuan.php';
    }

    elseif ($_GET['halaman']=='login-process') 
    {
        include 'login/login-action.php';
    }elseif ($_GET['halaman']=='logout') 
    {
        $HeadTo=Helper::baseUrl().'/admin/logout.php';

	    Header("Location: ".$HeadTo);
    }else {
        $HeadTo='/admin/404/index.php';

	    Header("Location: ".$HeadTo);
    }

?>