
 <?php Helper::checkPage();?>
 <!DOCTYPE html>
<html>
<head>
	<title> REKAPITULASI</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>

<section class="content-header">
 	<h1>
 		DATA KESELURUHAN FAKULTAS
 		<small> </small>
 	</h1>
 </section>
 <section class="inner">
	<div class="form-body wow fadeIn animated">
	<div class="box">
	<div class="add-table">

              <a type="button" class="btn btn-xs btn-primary btn-flat" href="index.php?halaman=tampil_rekapitulasi_1">
                <i class="fa "></i> JUMLAH DATA REKEPITULASI</a> 
              </div>
 		<div class="panel-body">


<?php 
// objek pasien menjalankan fungsi tampil_pasien
$prodi = $prodi->get_fakultas();
 ?>



	<table class="table table-bordered">
	<thead>
		<tr>
			<th class="text-center w-70">NO </th>			
			<th>FAKULTAS</th>
			<th class="text-center">JUMLAH DOSEN/PENULIS</th>
			<th class="text-center">JUMLAH PENELITIAN</th>
			<th class="text-center">JUMLAH PUBLIKASI ARTIKEL</th>
			<th class="text-center w-70">Opsi</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($prodi as $key => $value) :?>
		<tr>
			<td class="text-center"><?php echo $key+1 ?></td>
			<td><?php echo $value['nama_fakultas'] ?></td>
			<td class="text-center"><?php echo $value['jml_dosen'] ?></td>
			<td class="text-center"><?php echo $value['jml_lit_fakultas'] ?></td>
			<td class="text-center"><?php echo $value['jml_publish'] ?></td>
			<td class="text-center">
				<a href="index.php?halaman=detail_rekapitulasi&id_rekapitulasi=<?php echo $value['id_fakultas']; ?>" class="btn btn-xs  btn-info"><i class="fa fa-eye"></i> Detail</a>	
				<!-- <a href="index.php?halaman=hapus_rekapitulasi&id_rekapitulasi=<?php echo $value['id_fakultas']; ?>" class="btn btn-xs btn-danger" onclick="return confirm('hapus data ?')" ><i class="fa fa-trash"></i> Hapus</a> -->
			</td>
			
		</tr>
		<?php endforeach ?>
	</tbody>
</table>

	
</div>
</div>
</div>
</section>
</body>
</html>