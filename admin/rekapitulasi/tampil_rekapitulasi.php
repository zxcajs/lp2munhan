
<?php Helper::checkPage();?>
<!DOCTYPE html>
<html>
<head>
	<title> REKAPITULASI</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<style>
		.highcharts-figure,
		.highcharts-data-table table {
			min-width: 310px;
			max-width: 800px;
			margin: 1em auto;
		}

		#container {
			height: 400px;
		}

		.highcharts-data-table table {
			font-family: Verdana, sans-serif;
			border-collapse: collapse;
			border: 1px solid #ebebeb;
			margin: 10px auto;
			text-align: center;
			width: 100%;
			max-width: 500px;
		}

		.highcharts-data-table caption {
			padding: 1em 0;
			font-size: 1.2em;
			color: #555;
		}

		.highcharts-data-table th {
			font-weight: 600;
			padding: 0.5em;
		}

		.highcharts-data-table td,
		.highcharts-data-table th,
		.highcharts-data-table caption {
			padding: 0.5em;
		}

		.highcharts-data-table thead tr,
		.highcharts-data-table tr:nth-child(even) {
			background: #f8f8f8;
		}

		.highcharts-data-table tr:hover {
			background: #f1f7ff;
		}
	</style>
</head>
<body>
<?php 
	$data = $rekapitulasi->getRekapitulasi();
	$dataGrafik = $grafik->grafikPenelitianAll();
	$dataGrafikByProdi = $grafik->grafikPenelitianAllByProdi();
	$dataGrafikByTahun = $grafik->grafikPenelitianAllByTahun();
	
	$dataFilterTahun = $rekapitulasi->getFIlterTahun();
 ?>
<div class="inner">
	<div class="panel panel-primary">
 	<div class="panel-heading text-center">
 		<h2> DATA REKAPITULASI </h2>
 	</div>
 	<div class="panel-body">
 	
		<div class="col-md-6">
			<table class="table" id="" >
				<tr>
					<th>TAHUN </th>
					<th class="pull-right">
						<select onchange="setData(this);" class="form-control js-example-basic-single" name="combo1" id="combo1">
							<option value="all">keseluruhan</option>
							<?php 
								foreach ($dataFilterTahun as $key => $value)
								{
									echo "<option value=".$value['id'].">".$value['name']."</option>";
								}
							?> 
						</select>
					</th>
				</tr>
			</table>
			<table class="table table-striped" id="thetable" >
				
			</table>
		</div>
		<div class="col-md-6" style="min-height:600px">
			<figure class="highcharts-figure">
				<div id="container1"></div>
			</figure>
		</div>
		<div class="col-md-6">
			<figure class="highcharts-figure">
				<div id="grafikProdi"></div>
			</figure>
		</div>
		<div class="col-md-6">
			<figure class="highcharts-figure">
				<div id="container"></div>
			</figure>
		</div>
 	</div>
 </div>
<a href="index.php?halaman=tampil_rekapitulasi" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
</div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
	var dataTable = <?php echo json_encode($data); ?>;
	var dataGrafik = <?php echo json_encode($dataGrafik); ?>;
	var dataGrafikByTahun = <?php echo json_encode($dataGrafikByTahun); ?>;
	var dataGrafikByProdi = <?php echo json_encode($dataGrafikByProdi); ?>;
	
	var html =  '';
	for(var i=0;i<dataTable.length;i++){
		if(dataTable[i].total=='-'){
			html +=  '<tr><th class="text-center">'+dataTable[i].label+'</th></tr>';
		}else{
			html +=  '<tr><th>'+dataTable[i].label+'</th><th class="text-right">'+dataTable[i].total+'</th></tr>';
		}
	}
	$("#thetable").html(html);

	function setData(sel)
	{
		$.ajax({
			type: "POST",
			url: 'ref_data/refdata.php',
			data: {
				'tahun': sel.value,
				'action': 'getDataRekapitulasi'
			},
			dataType: 'json',
			success: function(response)
			{
				var dataTable = response.data;
				var html =  '';
				for(var i=0;i<dataTable.length;i++){
					if(dataTable[i].total=='-'){
						html +=  '<tr><th class="text-center">'+dataTable[i].label+'</th></tr>';
					}else{
						html +=  '<tr><th>'+dataTable[i].label+'</th><th>'+dataTable[i].total+'</th></tr>';
					}
				}
				$("#thetable").html(html);

				//set grafik
				var dataGrafik = response.dataGrafik;
				Highcharts.chart('container', {
					chart: {
						type: 'column'
					},
					title: {
						text: 'Grafik Perkembangan Penelitian By Fakultas'
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						categories: dataGrafik.fakultas,
						crosshair: true
					},
					yAxis: {
						title: {
							useHTML: true,
							text: 'Jumlah'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
							'<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
					series: dataGrafik.series
				});

				//set grafik by tahun
				var dataGrafikByTahun = response.dataGrafikByTahun;
				Highcharts.chart('container1', {
					chart: {
						type: 'column'
					},
					title: {
						text: 'Grafik Perkembangan Penelitian By Tahun'
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						categories: dataGrafikByTahun.tahun,
						crosshair: true
					},
					yAxis: {
						title: {
							useHTML: true,
							text: 'Jumlah'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
							'<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
					series: dataGrafikByTahun.series
				});

				console.log('fakultas',response.dataGrafik)
				console.log('prodi',response.dataGrafikByProdi)
				//grafik by prodi
				var dataGrafikByProdi = response.dataGrafikByProdi;
				Highcharts.chart('grafikProdi', {
					chart: {
						type: 'column'
					},
					title: {
						text: 'Grafik Perkembangan Penelitian By Prodi'
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						categories: dataGrafikByProdi.fakultas,
						crosshair: true
					},
					yAxis: {
						title: {
							useHTML: true,
							text: 'Jumlah Penelitian'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
							'<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
					series: dataGrafikByProdi.series
				});
			}
		});
	}
</script>
<script>
	Highcharts.chart('container', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Grafik Perkembangan Penelitian By Fakultas'
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: dataGrafik.fakultas,
			crosshair: true
		},
		yAxis: {
			title: {
				useHTML: true,
				text: 'Jumlah Penelitian'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: dataGrafik.series
	});
	Highcharts.chart('grafikProdi', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Grafik Perkembangan Penelitian By Prodi'
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: dataGrafikByProdi.prodi,
			crosshair: true
		},
		yAxis: {
			title: {
				useHTML: true,
				text: 'Jumlah Penelitian'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: dataGrafikByProdi.series
	});
	Highcharts.chart('container1', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Grafik Perkembangan Penelitian By Tahun'
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: dataGrafikByTahun.tahun,
			crosshair: true
		},
		yAxis: {
			title: {
				useHTML: true,
				text: 'Jumlah'
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: dataGrafikByTahun.series
	});
</script>
</body>
</html>