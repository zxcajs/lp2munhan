<?php Helper::checkPage();?>
<!DOCTYPE html>
<html>
<head>
	<title>DETAIL DATA REKAPITULASI</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<style>
		.highcharts-figure,
		.highcharts-data-table table {
			min-width: 310px;
			max-width: 800px;
			margin: 1em auto;
		}

		#container {
			height: 400px;
		}

		.highcharts-data-table table {
			font-family: Verdana, sans-serif;
			border-collapse: collapse;
			border: 1px solid #ebebeb;
			margin: 10px auto;
			text-align: center;
			width: 100%;
			max-width: 500px;
		}

		.highcharts-data-table caption {
			padding: 1em 0;
			font-size: 1.2em;
			color: #555;
		}

		.highcharts-data-table th {
			font-weight: 600;
			padding: 0.5em;
		}

		.highcharts-data-table td,
		.highcharts-data-table th,
		.highcharts-data-table caption {
			padding: 0.5em;
		}

		.highcharts-data-table thead tr,
		.highcharts-data-table tr:nth-child(even) {
			background: #f8f8f8;
		}

		.highcharts-data-table tr:hover {
			background: #f1f7ff;
		}
	</style>
</head>
<body>

<?php 
	//mengambil id pasien dari url
	$id = $_GET['id_rekapitulasi'];
	$data = $rekapitulasi->getDetailFakultas($id);
	$dataGrafik = $grafik->grafikPenelitianByFakultas($id);
 ?>
 
 
<div class="inner">
	<div class="panel panel-primary">
 	<!-- <div class="panel-heading text-center">
 		<h2>DATA FAKULTAS</h2>
 	</div> -->
 	<div class="panel-body">
		<div class="col-md-6" style="min-height:600px;">
			<table class="table table-striped" id="thetable" >
				
			</table>
 		</div>

 		<div class="col-md-6">
			<figure class="highcharts-figure">
				<div id="container"></div>
			</figure>
		</div>
 	</div>
 </div>
<a href="index.php?halaman=tampil_rekapitulasi" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
	var dataTable = <?php echo json_encode($data); ?>;
	var dataGrafik = <?php echo json_encode($dataGrafik); ?>;

	var html =  '';
	for(var i=0;i<dataTable.length;i++){
		if(dataTable[i].total=='-'){
			html +=  '<tr><th class="text-center">'+dataTable[i].label+'</th></tr>';
		}else{
			html +=  '<tr><th>'+dataTable[i].label+'</th><th class="text-right">'+dataTable[i].total+'</th></tr>';
		}
	}
	$("#thetable").html(html);

	Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Perkembangan'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: <?php echo json_encode($dataGrafik['tahun']); ?>,
        crosshair: true
    },
    yAxis: {
        title: {
            useHTML: true,
            text: 'Jumlah'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: <?php echo json_encode($dataGrafik['series']); ?>
});
</script>
 </body>
</html>

