<?php Helper::checkPage();?>
<section class="content-header">
	<h1>
		Ubah 
		<small> |  Data Staff </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
			
				<div class="panel-body">
					<?php 
						$id_staff = $_GET['id_staff'];
						$data_staff = $staff->detail_staff($id_staff);
						$refPangkat = $refData->getPangkatList();
						$refJabatan = $refData->getJabatanList();
					?>

					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label> Nama  </label>
							<input type="" class="form-control" name="nama_staff" value="<?php echo $data_staff['nama_staff'] ?>">
						</div>
						<div class="form-group">
							<label> Pangkat </label>
							<select class="form-control js-example-basic-single" name="pangkat_staff" id="pangkat_staff">
								<option value="">pilih</option>
								<?php 
									foreach ($refPangkat as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
						<div class="form-group">
							<label> Jabatan </label>
							<select class="form-control js-example-basic-single" name="jabatan_staff" id="jabatan_staff">
								<option value="">pilih</option>
								<?php 
									foreach ($refJabatan as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
						
						<a href="index.php?halaman=tampil_staff" class="btn btn-xs btn-danger"><i class="fa fa-close"></i> Batal</a>
						<button class="btn btn-xs btn-primary" name="simpan"><i class="fa fa-save"></i> Simpan</button>

					</form>
				</div>
			</div>
		</div>
	</section>
	<?php if (isset($_POST['simpan'])) 
	{
		$staff->ubah_staff($_POST['nama_staff'],$_POST['pangkat_staff'],$_POST['jabatan_staff'],$id_staff);

	echo "<script>alert('data tersimpan')</script>";
		echo "<script>location='index.php?halaman=tampil_staff'</script>";
	}

	?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>
		$('select[name=pangkat_staff]').val(<?php echo $data_staff['ref_pangkat_id'] ?>).change();
		$('select[name=jabatan_staff]').val(<?php echo $data_staff['ref_jabatan_id'] ?>).change();
	</script>