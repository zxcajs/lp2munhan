<?php Helper::checkPage();?>
<section class="content-header">
	<h1>
	Tambah Data 
		<small> | Data Staff  </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body">
		<div class="box">
				<div class="panel-body">
					<?php 
						$refPangkat = $refData->getPangkatList();
						$refJabatan = $refData->getJabatanList();
					?>
					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nama </label>
							<input type="" name="nama_staff" class="form-control" required="">
						</div>
						<div class="form-group">
							<label> Pangkat </label>
							<select class="form-control js-example-basic-single" name="pangkat_staff" id="pangkat_staff">
								<option value="">pilih</option>
								<?php 
									foreach ($refPangkat as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>

						</div>
						<div class="form-group">
							<label>Jabatan</label>
							<select class="form-control js-example-basic-single" name="jabatan_staff" id="jabatan_staff">
								<option value="">pilih</option>
								<?php 
									foreach ($refJabatan as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>						
						<button class="btn btn-primary" name="simpan">simpan</button>
					</form>
				</div>
			</div>
		</div>
	</section>

	<?php 
	if (isset($_POST['simpan'])) 
	{
		$staff->simpan_staff($_POST['nama_staff'],$_POST['pangkat_staff'],$_POST['jabatan_staff']);
		echo "<script>alert('data tersimpan')</script>";
		echo "<script>location='index.php?halaman=tampil_staff'</script>";
	}
	?>