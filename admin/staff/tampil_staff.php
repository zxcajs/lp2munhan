<?php Helper::checkPage();?>
<?php
$data_staff = $staff->tampil_staff(); 
?>


<section class="content-header">
	<h1>
		Data Staff LPPM
		<small> </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
			<div class="add-table">

               <a type="button" class="btn btn-xs btn-primary btn-flat" href="index.php?halaman=tambah_staff">
                <i class="fa fa-plus-square"></i> Tambah Data</a> 
              </div>
				<div class="panel-body">
					<div class="inner">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th>Nama </th>
									<th>Pangkat</th>
									<th>Jabatan</th>
									<th class="text-center">Opsi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data_staff as $key => $value) : ?>
									<tr>
										<td class="text-center w-70"><?php echo $key+1 ?></td>
										<td><?php echo $value['nama_staff']; ?></td>
										<td><?php echo $value['pangkat_staff']; ?></td>
										<td><?php echo $value ['jabatan_staff'];?></td>
										<td class="text-center w-150">
											
											<a href="index.php?halaman=ubah_staff&id_staff=<?php echo $value['id_staff']; ?>" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Ubah</a>
											<a href="index.php?halaman=hapus_staff&id_staff=<?php  echo $value['id_staff'] ?>" class="btn btn-xs btn-danger" onclick="return confirm('hapus data ?')" ><i class="fa fa-trash"></i> Hapus</a>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						
					</div>
				</div>
			</div>
		</div>
	</section>
