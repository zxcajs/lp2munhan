
<?php Helper::checkPage();?>
<!DOCTYPE html>
<html>
	<head>
		<title> Prodi</title>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	</head>
	<body>

		<section class="content-header">
			<h1>
				Data Prodi
				<small> </small>
			</h1>
		</section>
		<section class="inner">
			<div class="form-body wow fadeIn animated">
				<div class="box">
					<div class="panel-body">
						<?php 
							// objek pasien menjalankan fungsi tampil_pasien
							$data_prodi = $prodi->tampil_prodi();
						?>
						<table class="table table-bordered" id="thetable">
							<thead>
								<tr>
									<th class="text-center w-70">NO </th>			
									<th>PRODI</th>
									<th>FAKULTAS</th>
									<th class="text-center">JUMLAH DOSEN/PENULIS</th>
									<th class="text-center">JUMLAH PENELITIAN</th>
									<th class="text-center">JUMLAH PUBLIKSI ARTIKEL</th>
									<th class="text-center w-70">OPSI</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data_prodi as $key => $value) :?>
									<tr>
										<td class="text-center"><?php echo $key+1 ?></td>
									
										<td><?php echo $value['nama_prodi'] ?></td>
										<td><?php echo $value['nama_fakultas'] ?></td>
										<td class="text-center"><?php echo $value['jml_dosen'] ?></td>
										<td class="text-center"><?php echo $value['jml_lit_prodi'] ?></td>
										<td class="text-center"><?php echo $value['jml_publish_prodi'] ?></td>
										<td class="text-center">
											<a href="index.php?halaman=detail_prodi&id_prodi=<?php echo $value['id_prodi']; ?>" class="btn btn-xs btn-info"><i class="fa fa-eye"></i> Detail</a>
										</td>
										
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
	</body>
</html>