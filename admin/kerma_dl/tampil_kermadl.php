<?php Helper::checkPage();?>
<?php
$data_kermadl = $kerma_dl->tampil_kermadl(); 
?>


<section class="content-header">
	<h1>
		Kerja Sama LPPM UNHAN RI
		<small> Dalam Negeri</small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
			<div class="add-table">

               <a type="button" class="btn btn-xs btn-primary btn-flat" href="index.php?halaman=tambah_kermadl">
                <i class="fa fa-plus-square"></i> Tambah Data</a> 
              </div>
				<div class="panel-body">
					<div class="inner">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center w-70">No</th>
									<th>Instansi/ Kementrian</th>
									<th>Bidang Kerja Sama</th>
									<th class="text-center w-100">Tanggal</th>
									<th class="text-center w-150">Opsi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data_kermadl as $key => $value) : ?>
									<tr>
										<td class="text-center"><?php echo $key+1 ?></td>
										<td><?php echo $value['nama_kerma']; ?></td>
										<td><?php echo $value['bidang_kerma']; ?></td>
										<td class="text-center"><?php echo $value ['tanggal'];?></td>
										<td class="text-center">
											<a href="index.php?halaman=ubah_kermadl&id_kerma=<?php echo $value['id_kerma']; ?>" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Ubah</a>
											<a href="index.php?halaman=hapus_kermadl&id_kerma=<?php  echo $value['id_kerma'] ?>" class="btn btn-xs btn-danger" onclick="return confirm('hapus data ?')" ><i class="fa fa-trash"></i> Hapus</a>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						
					</div>
				</div>
			</div>
		</div>
	</section>
