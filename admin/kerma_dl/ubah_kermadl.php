<?php Helper::checkPage();?>
<section class="content-header">
	<h1>
		 Kerjasama Dalam Negeri
		<small> |  Merubah Data  </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
			
				<div class="panel-body">
					<?php 
					$id_kerma = $_GET['id_kerma'];
					$data_kermadl = $kerma_dl->detail_kermadl($id_kerma);
					?>

					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label> Institusi/Kementrian  </label>
							<input type="" class="form-control" name="nama_kerma" value="<?php echo $data_kermadl['nama_kerma'] ?>">
						</div>
						<div class="form-group">
							<label> Bidang </label>
							<input type="" class="form-control" name="bidang_kerma" value="<?php echo $data_kermadl['bidang_kerma'] ?>">
						</div>
						<div class="form-group">
							<label> Tanggal </label>
							<input type="date" class="form-control" name="tanggal" value="<?php echo $data_kermadl['tanggal']?>">
						</div>
						
						<a href="index.php?halaman=tampil_kermadl" class="btn btn-xs btn-danger"><i class="fa fa-backward"></i> Kembali</a>
						<button class="btn btn-xs btn-primary" name="simpan"><i class="fa fa-save"></i> Simpan</button>

					</form>
				</div>
			</div>
		</div>
	</section>
	<?php if (isset($_POST['simpan'])) 
	{
		$kerma_dl->ubah_kermadl($_POST['nama_kerma'],$_POST['bidang_kerma'],$_POST['tanggal'],$id_kerma);

		echo "<script>alert('data tersimpan')</script>";
		echo "<script>location='index.php?halaman=tampil_kermadl'</script>";
	}

	?>