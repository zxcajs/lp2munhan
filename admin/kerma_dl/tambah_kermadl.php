<?php Helper::checkPage();?>
<section class="content-header">
	<h1>
	 Data Kerja Sama 
		<small> | Dalam Negeri  </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body">
		<div class="box">
			
				<div class="panel-body">


					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Instansi/ Kementrian </label>
							<input type="" name="nama_kerma" class="form-control" required="">
						</div>
						<div class="form-group">
							<label>Bidang Kerjasama</label>
							<textarea class="form-control"  name="bidang_kerma" required="" ></textarea>

						</div>
						<div class="form-group">
							<label>Tanggal</label>
							<input class="form-control" type="date" name="tanggal" value="<?date('Y-m-d')?>" required="" >
						</div>
						<a href="index.php?halaman=tampil_kermadl" class="btn btn-xs btn-danger"><i class="fa fa-backward"></i> Kembali</a>
						<button class="btn btn-xs btn-primary" name="simpan"><i class="fa fa-save"></i> simpan</button>
					</form>
				</div>
			</div>
		</div>
	</section>

	<?php 
	if (isset($_POST['simpan'])) 
	{
		$kerma_dl->simpan_kermadl($_POST['nama_kerma'],$_POST['bidang_kerma'],$_POST['tanggal']);
		echo "<script>alert('data tersimpan')</script>";
		echo "<script>location='index.php?halaman=tampil_kermadl'</script>";
	}
	?>