<?php Helper::checkPage();?>
<section class="content-header">
	<h1>
		Data Dosen
		<small> | Isi Data </small>
	</h1>
</section>

<?php 
	// objek pasien menjalankan fungsi tampil_pasien
	$refProdi = $refData->getProdiList();
	$refFakultas = $refData->getFakultasList();
	$refPangkat = $refData->getPangkatList();
	$refJabatan = $refData->getJabatanList();
	$refJabatanAkademik = $refData->getJabatanAkademikList();
 ?>

<!-- Main content -->
<section class="inner">
	<!-- Your Page Content Here -->
	<div class="form-body">
		<div class="row">
			<form class="form-horizontal" method="POST" enctype='multipart/form-data'>
				<div class="box-body">

					<div class="form-group">
						<label class="col-md-3 control-label">NAMA DOSEN</label>
						<div class="col-sm-7">
							<input type="" class="form-control" name="nama_dosen" required="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">NRP/NIP/NIDN</label>
						<div class="col-sm-7">
							<input type="" class="form-control" name="nrp" required="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">PANGKAT</label>
						<div class="col-sm-7">
							<select class="form-control js-example-basic-single" name="pangkat" id="pangkat">
								<option value="">pilih</option>
								<?php 
									foreach ($refPangkat as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">JABATAN</label>
						<div class="col-sm-7">
							<select class="form-control js-example-basic-single" name="jabatan" id="jabatan">
								<option value="">pilih</option>
								<?php 
									foreach ($refJabatan as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">JABATAN AKADEMIK</label>
						<div class="col-sm-7">
							<select class="form-control js-example-basic-single" name="jabatan_akademik" id="jabatan_akademik">
								<option value="">pilih</option>
								<?php 
									foreach ($refJabatanAkademik as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group">
					<label class="col-md-3 control-label">KEILMUAN</label>
						<div class="col-sm-7">
							<input type="" class="form-control" name="keilmuan" required="">
						</div>
					</div>
					<div class="form-group">
					<label class="col-md-3 control-label">POSISI</label>
						<div class="col-sm-7">
							<input type="" class="form-control" name="posisi" required="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">PROGRAM STUDI</label>
						<div class="col-sm-7">
							<select onchange="getval(this);" class="form-control js-example-basic-single" name="prodi" id="prodi">
								<option value="">pilih</option>
								<?php 
									foreach ($refProdi as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">FAKULTAS</label>
						<div class="col-sm-7">
							<select disabled class="form-control js-example-basic-single" name="fakultas" id="fakultas">
								<option value="">pilih</option>
								<?php 
									foreach ($refFakultas as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="col-md-3 control-label">Password</label>
							<div class="col-sm-7">
							<input type="" class="form-control" name="password_pasien" required="">
						</div>
					</div> -->


						</div><!-- /.box-body -->
						<div class="col-sm-offset-2 col-sm-3">
							<a href="index.php?halaman=dosen" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
							<button type="submit" name="simpan" class="btn btn-xs btn-default btn-flat "> <i class="fa fa-save"></i> Simpan</button>
						</div><!-- /.box-footer -->
					</form>
				</div> <!-- row -->
			</div>
		</section>

		<?php 
		if (isset($_POST['simpan']))
		{	
			$dosen->simpan_dosen($_POST['nama_dosen'],$_POST['nrp'],$_POST['pangkat'],$_POST['jabatan'],$_POST['jabatan_akademik'],$_POST['keilmuan'],$_POST['posisi'],$_POST['prodi']);

			echo "<script>alert('data dosen berhasil disimpan')</script>";
			echo"<script>location= 'index.php?halaman=dosen'</script>";
		}
		
		?>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>
			function getval(sel)
			{
				$.ajax({
					type: "POST",
					url: 'ref_data/refdata.php',
					data: {
						'id': sel.value,
						'action': 'getFakultasId'
					},
					dataType: 'json',
					success: function(response)
					{
						$('select[name=fakultas]').val(response.fakultas_id).change();
					}
				});
			}
		</script>








