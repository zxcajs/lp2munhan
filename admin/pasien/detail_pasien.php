<?php Helper::checkPage();?>
<!DOCTYPE html>
<html>
<head>
	<title>detail</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>

<?php 
//mengambil id pasien dari url
$id_dosen = $_GET['id_dosen'];
$datadosen = $dosen->detail_dosen($id_dosen);

?>
 
 
<div class="inner">
	<div class="panel panel-primary">
 	<div class="panel-heading text-center">
 		<h2>Detail Dosen</h2>
 	</div>
 	<div class="panel-body">
 	
 	<div class="col-md-6">
 		<table class="table table-striped">
 			<tr>
 				<th>NAMA</th>
 				<th><?php echo $datadosen['nama_dosen']; ?></th>
 			</tr>
 			<tr>
 				<th>NRP/NIP/NIDN</th>
 				<th><?php echo $datadosen['nrp']; ?></th>
 			</tr>
 			<tr>
 				<th>PANGKAT</th>
 				<th><?php echo $datadosen['pangkat']; ?></th>
 			</tr>
 			<tr>
 				<th>JABATAN</th>
 				<th><?php echo $datadosen ['jabatan']; ?></th>
 			</tr>
 			<tr>
 				<th>JABATAN AKADEMIK</th>
 				<th><?php echo $datadosen['jabatan_akademik']; ?></th>
 			</tr>
 			<tr>
 				<th>KEILMUAN</th>
 				<th><?php echo $datadosen['keilmuan']; ?></th>
 			</tr>
 			<tr>
 				<th>POSISI</th>
 				<th><?php echo $datadosen['posisi']; ?></th>
 			</tr>
 			<tr>
 				<th>PRODI</th>
 				<th><?php echo $datadosen['prodi']; ?></th>
 			</tr>
 			<tr>
 				<th>FAKULTAS</th>
 				<th><?php echo $datadosen['fakultas']; ?></th>
 			</tr>
 		</table>
 	</div>
 		
 	</div>
 </div>
<a href="index.php?halaman=dosen" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
</div>
 </body>
</html>