
 <?php Helper::checkPage();?>
 <!DOCTYPE html>
<html>
<head>
	<title> Dosen</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>

<section class="content-header">
 	<h1>
 		Data Dosen
 		<small> </small>
 	</h1>
 </section>
 <section class="inner">
	<div class="form-body wow fadeIn animated">
	<div class="box">
	<div class="add-table">

               <a type="button" class="btn btn-xs btn-primary btn-flat" href="index.php?halaman=tambah_dosen">
                <i class="fa fa-plus-square"></i> Tambah Dosen</a> 
              </div>
 		<div class="panel-body">


<?php 
// objek pasien menjalankan fungsi tampil_pasien
$data_pasien = $dosen->tampil_dosen();
 ?>



	<table class="table table-bordered">
	<thead>
		<tr>
			<th class="text-center w-70">NO </th>			
			<th>NAMA</th>
			<th>NRP/NIP/NIDN</th>
			<th class="text-center w-220">Opsi</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($data_pasien as $key => $value) :?>
		<tr>
			<td class="text-center"><?php echo $key+1 ?></td>
		
			<td><?php echo $value['nama_dosen'] ?></td>
			
			
			<td><?php echo $value['nrp'] ?></td>

			<td class="text-center">
				<a href="index.php?halaman=detail_dosen&id_dosen=<?php echo $value['id_dosen']; ?>" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> Detail</a>	
				<a href="index.php?halaman=ubah_dosen&id_dosen=<?php echo $value['id_dosen']; ?>" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>  Ubah</a>
				<a href="index.php?halaman=hapus_dosen&id_dosen=<?php echo $value['id_dosen']; ?>" class="btn btn-xs btn-danger" onclick="return confirm('hapus data ?')" ><i class="fa fa-trash"></i> Hapus</a>
			</td>
			
		</tr>
		<?php endforeach ?>
	</tbody>
</table>

	
</div>
</div>
</div>
</section>
</body>
</html>