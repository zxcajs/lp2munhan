
<?php Helper::checkPage();?>
<?php 
	$id_dosen = $_GET['id_dosen'];
	$datadosen = $dosen->detail_dosen($id_dosen);
	$refProdi = $refData->getProdiList();
	$refFakultas = $refData->getFakultasList();
	$refPangkat = $refData->getPangkatList();
	$refJabatan = $refData->getJabatanList();
	$refJabatanAkademik = $refData->getJabatanAkademikList();
?>


<section class="content-header">
	<h1>
		Data dosen
		<small> | Memperbaiki Data Anda </small>
	</h1>
</section>

<section class="inner">
	<div class="form-body">
		<div class="row">
			<form class="form-horizontal" method="POST" enctype='multipart/form-data'>
				<div class="box-body">

					<div class="form-group">
						<label class="col-md-3 control-label">NAMA DOSEN</label>
						<div class="col-sm-7">
							<input type="" class="form-control" name="nama_dosen" value="<?php echo $datadosen['nama_dosen'] ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">NRP/NIP/NIDN</label>
						<div class="col-sm-7">
							<input type="" class="form-control" name="nrp" value="<?php echo $datadosen['nrp']?>">	
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">PANGKAT</label>
						<div class="col-sm-7">
							<select class="form-control js-example-basic-single" name="pangkat" id="pangkat">
								<option value="">pilih</option>
								<?php 
									foreach ($refPangkat as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">JABATAN</label>
						<div class="col-sm-7">
							<select class="form-control js-example-basic-single" name="jabatan" id="jabatan">
								<option value="">pilih</option>
								<?php 
									foreach ($refJabatan as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">JABATAN AKADEMIK</label>
						<div class="col-sm-7">
							<select class="form-control js-example-basic-single" name="jabatan_akademik" id="jabatan_akademik">
								<option value="">pilih</option>
								<?php 
									foreach ($refJabatanAkademik as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group">
					<label class="col-md-3 control-label">KEILMUAN</label>
						<div class="col-sm-7">
							<input type="" class="form-control" name="keilmuan" value="<?php echo $datadosen['keilmuan'] ?>">
						</div>
					</div>
					<div class="form-group">
					<label class="col-md-3 control-label">POSISI</label>
						<div class="col-sm-7">
							<input type="" class="form-control" name="posisi" value="<?php echo $datadosen['posisi'] ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">PRODI</label>
						<div class="col-sm-7">
							<select class="form-control js-example-basic-single" name="prodi" id="prodi">
								<option value="">pilih</option>
								<?php 
									foreach ($refProdi as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
							</div>
						</div>
					<div class="form-group">
					<label class="col-md-3 control-label">FAKULTAS</label>
						<div class="col-sm-7">
							<select disabled class="form-control js-example-basic-single" name="fakultas" id="fakultas">
								<option value="">pilih</option>
								<?php 
									foreach ($refFakultas as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>

					<div class="col-sm-offset-3 col-sm-4">
						<a href="index.php?halaman=dosen" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
						<button type="submit" name="simpan" class="btn btn-xs btn-default btn-flat "> <i class="fa fa-save"></i> Simpan</button>
					</div><!-- /.box-footer -->

				</div>
			</form>
		</div>
	</div>
</section>
<?php if (isset($_POST['simpan'])) 
{
	$dosen->ubah_dosen($_POST['nama_dosen'],$_POST['nrp'],$_POST['pangkat'],$_POST['jabatan'],$_POST['jabatan_akademik'],$_POST['keilmuan'],$_POST['posisi'],$_POST['prodi'],$id_dosen);
	//echo "<script>alert('data dosen tersimpan')</script>";
	echo "<script>location='index.php?halaman=dosen'</script>";
}

?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
	$('select[name=pangkat]').val(<?php echo $datadosen['ref_pangkat_id'] ?>).change();
	$('select[name=jabatan]').val(<?php echo $datadosen['ref_jabatan_id'] ?>).change();
	$('select[name=jabatan_akademik]').val(<?php echo $datadosen['ref_jabatan_akademik_id'] ?>).change();
	$('select[name=prodi]').val(<?php echo $datadosen['ref_prodi_id'] ?>).change();
	$('select[name=fakultas]').val(<?php echo $datadosen['ref_fakultas_id'] ?>).change();
</script>
