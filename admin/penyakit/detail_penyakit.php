<?php Helper::checkPage();?>
<!DOCTYPE html>
<html>
<head>
	<title>Detail</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>

<?php 
$id_lit = $_GET['id_lit'];
$data_lit = $lit->detail_lit($id_lit);

 ?>


 <div class="inner">
 <div class="panel panel-primary">
 <div class="panel-heading text-center">
 <h2>Detail Penelitian</h2>
 </div>

 <div class="panel-body">
 	

 	<div class="col-md-15">
 	<table class="table table-striped">
 		<tr>
 			<th>Nama Penulis</th>
 			<th><?php echo $data_lit['nama_penulis']; ?></th>
 		</tr>
 		<tr>
 			<th>Judul Penelitian</th>
 			<th><?php echo $data_lit['judul_penelitian']; ?></th>
 		</tr>
 		<tr>
 			<th>Author/Responding Author</th>
 			<th><?php echo $data_lit['author']; ?></th>
 		</tr>
		 <tr>
 			<th>Prodi</th>
 			<th><?php echo $data_lit['ref_prodi_name']; ?></th>
 		</tr>
		 <tr>
 			<th>Fakultas</th>
 			<th><?php echo $data_lit['ref_fakultas_name']; ?></th>
 		</tr>
		 <tr>
 			<th>Tahun</th>
 			<th><?php echo $data_lit['tahun']; ?></th>
 		</tr>
 		<tr>
 			<th>Jurnal </th>
 			<th><?php echo $data_lit['jurnal']; ?></th>
 		</tr>
		<?php if($data_lit['jurnal']<>'Tidak Ada') { ?>
			<tr>
				<th>Publisher  </th>
				<th><?php echo $data_lit['publiser']; ?></th>
			</tr>
			<tr>
				<th>E-ISSN  </th>
				<th><?php echo $data_lit['issn']; ?></th>
			</tr>
			<tr>
				<th>Jadwal terbit </th>
				<th><?php echo $data_lit['terbit']; ?></th>
			</tr>
			<tr>
				<th>Negara </th>
				<th><?php echo $data_lit['negara']; ?></th>
			</tr>
			<tr>
				<th>URL </th>
				<th><?php echo $data_lit['url']; ?></th>
			</tr>
 		<?php } ?>

 	</table>
 	</div>
 </div>
 </div>
 	<a href="index.php?halaman=tampil_penelitian" class="btn btn-xs btn-danger"><i class="fa fa-backward"></i> Kembali</a>
 </div>
</body>
</html>