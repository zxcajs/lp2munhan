<?php Helper::checkPage();?>
<section class="content-header">
	<h1>
	Menambahkan Data 
		<small> | Penelitian  </small>
	</h1>
</section>

<?php 
	// objek pasien menjalankan fungsi tampil_pasien
	$dosenList = $dosen->getListDosen();
	$refCountryList = $refData->getCountryList();
	$refPublisherList = $refData->getPublisherList();
	$refJurnalList = $refData->getJurnalList();
	$refAuthorList = $refData->getAuthorList();
	$refProdi = $refData->getProdiList();
 ?>

<!-- Main content -->
<section class="inner">
	<!-- Your Page Content Here -->
	<div class="form-body">

		<div class="row">
			<form class="form-horizontal" method="POST" enctype='multipart/form-data'>
				<div class="box-body">

					<div class="form-group">
						<label class="col-md-3 control-label">NAMA PENULIS (DOSEN)</label>
						<div class="col-sm-7">
							<select class="form-control js-example-basic-multiple" name="nama_penulis[]" multiple="multiple" id="" required>
								<option value="">pilih</option>
								<?php 
								foreach ($dosenList as $key => $value)
								{
									echo "<option value=".$value['id_dosen'].">".$value['nama_dosen']."</option>";
								}
								?> 
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label">JUDUL PENELITIAN</label>
						<div class="col-sm-7">
							<textarea class="form-control"  name="judul_penelitian" required="" ></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">AUTHOR/RESPONDING AUTHOR</label>
						<div class="col-sm-7">
							<select class="form-control js-example-basic-single" name="author" id="author">
								<option value="">pilih</option>
								<?php 
									foreach ($refAuthorList as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Prodi </label>
						<div class="col-sm-7">
							<select onchange="getval(this);" class="form-control js-example-basic-single" name="prodi" id="prodi">
								<option value="">pilih</option>
								<?php 
									foreach ($refProdi as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Fakultas </label>
						<div class="col-sm-7">
							<input type="hidden" maxlength="4" name="fakultas" class="form-control" required="">
							<input type="text" readonly maxlength="4" name="fakultas_name" class="form-control" required="">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label">Tahun</label>
						<div class="col-sm-7">
							<input type="text" maxlength="4" class="form-control" name="tahun" >
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">Jurnal</label>
						<div class="col-sm-7">
							<select onchange="changeJurnal()" class="form-control js-example-basic-single" name="jurnal" id="jurnal">
								<?php 
									foreach ($refJurnalList as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group publish-group">
					<label class="col-md-3 control-label">PUBLISHER</label>
						<div class="col-sm-7">
							<select onchange="setOther(this);" class="form-control" name="publiser" id="publiser">
								<option value="">pilih</option>
								<?php 
									foreach ($refPublisherList as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group other-publisher publish-group hide">
						<label class="col-md-3 control-label"></label>
						<div class="col-sm-7">
							<input class="form-control " placeholder="silahkan isi lainnya" name="other_publisher" >
						</div>
					</div>
					<div class="form-group publish-group">
						<label class="col-md-3 control-label">E-ISSN </label>
						<div class="col-sm-7">
							<input class="form-control" name="issn" >
						</div>
					</div>
					<div class="form-group publish-group">
					<label class="col-md-3 control-label">JADWAL TERBIT</label>
						<div class="col-sm-7">
							<input type="date" class="form-control" name="terbit" value="<?date('Y-m-d')?>" >
						</div>
					</div>

					<div class="form-group publish-group">
					<label class="col-md-3 control-label">NEGARA</label>
						<div class="col-sm-7">
							<select class="form-control" name="negara" id="negara">
								<option value="">pilih</option>
								<?php 
									foreach ($refCountryList as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group publish-group">
						<label class="col-md-3 control-label">AKUN GOOGLE SCHOLAR</label>
						<div class="col-sm-7">
							<select onchange="setGs(this);" class="form-control" name="punya_google_scholar" id="punya_google_scholar">
									<option value="1">ADA </option>
									<option value="0">TIDAK ADA</option>
							</select>
						</div>
					</div>
					<div class="form-group url-google publish-group hide">
						<label class="col-md-3 control-label"></label>
						<div class="col-sm-7">
							<input class="form-control " placeholder="silahkan isi link disini" name="url">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">File </label>
						<div class="col-sm-7">
							<input class="form-control" type="file" name="document" accept="application/pdf">
						</div>
					</div>
					
						</div><!-- /.box-body -->
						<div class="col-sm-offset-2 col-sm-3">
							<a href="index.php?halaman=tampil_penelitian" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
							<button type="submit" name="simpan" class="btn btn-xs btn-default btn-flat "> <i class="fa fa-save"></i> Simpan</button>
						</div><!-- /.box-footer -->
					</form>
				</div> <!-- row -->
			</div>
		</section>
	<?php 
	if (isset($_POST['simpan'])) 
	{
		$lit->simpan_lit(
			$_POST['nama_penulis'],
			$_POST['judul_penelitian'],
			$_POST['author'],
			$_POST['tahun'],
			$_POST['jurnal'],
			$_POST['other_publisher'],
			$_POST['issn'],
			$_POST['terbit'],
			$_POST['negara'],
			$_POST['punya_google_scholar'],
			$_POST['url'],
			'general',
			$_POST['publiser'],
			$_POST['prodi'],
			$_POST['fakultas'],
			$_FILES['document']
		);

		echo "<script>alert('data tersimpan')</script>";
		echo "<script>location='index.php?halaman=tampil_penelitian'</script>";
	}
	?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>
		$('select[name=jurnal]').val('none').change();
		$('select[name=negara]').val(100).change();
		$('select[name=punya_google_scholar]').val(0).change();
		
		var jurnal = $('select[name=jurnal]').val();
		$('.publish-group').hide();

		function setOther(sel)
		{
			if(sel.value==17){
				$(".other-publisher").removeClass("hide");
			}else{
				$(".other-publisher").addClass("hide");
			}
		}
		function changeJurnal()
		{
			var jurnal = $('select[name=jurnal]').val();
			if(jurnal=='none'){
				$('.publish-group').hide();
				$('select[name=negara]').val('100').change();
				$('select[name=publiser]').val('').change();
				$('select[name=punya_google_scholar]').val('0').change();
				$("[name='url']").val('');
				$("[name='other_publisher']").val('');
				$("[name='issn']").val('');
				$("[name='terbit']").val('');
			}else{
				$('.publish-group').show()
			}
		}
		function setGs(sel)
		{
			if(sel.value=='1'){
				$(".url-google").removeClass("hide");
			}else{
				$(".url-google").addClass("hide");
			}
		}
	</script>
	<script>
		function getval(sel)
		{
			$.ajax({
				type: "POST",
				url: 'ref_data/refdata.php',
				data: {
					'id': sel.value,
					'action': 'getFakultasId'
				},
				dataType: 'json',
				success: function(response)
				{
					$("[name='fakultas']").val(response.fakultas_id);
					$("[name='fakultas_name']").val(response.fakultas_name);
				}
			});
		}
	</script>