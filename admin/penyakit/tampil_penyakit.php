<?php Helper::checkPage();?>
<?php
$data_lit = $lit->tampil_lit();

?>


<section class="content-header">
	<h1>
		Data Penelitian LPPM UNHAN RI
		<small> </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
				<div class="panel-body">
					<div class="inner">
						
					<div class="add-table">

             	  <a type="button" class="btn btn-xs btn-primary btn-flat" href="index.php?halaman=tambah_penelitian">
              	  <i class="fa fa-plus-square"></i> Tambah Data </a> 
             	 </div>

						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center w-70">No</th>
									<th>Judul Penelitian</th>
									<th>Nama Penulis</th>
									<th class="text-center w-290">Opsi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data_lit as $key => $value) : ?>
									<tr>
										<td class="text-center"><?php echo $key+1 ?></td>
										<td><?php echo $value['judul_penelitian']; ?></td>
										<td><?php echo $value ['nama_penulis'];?></td>
										<td class="text-left">
											<?php if(!empty($value['document'])){ ?>
												<a target="_blank" href="<?php echo Helper::baseUrl().'admin/upload/penelitian/'.$value['document']; ?>" class="btn btn-xs btn-primary"><i class="fa fa-download"></i> Download</a>
											<?php }else{ ?>
												<a href="#0" disabled="disabled" class="btn btn-xs btn-primary"><i class="fa fa-download"></i> Download</a>
											<?php } ?>
											<a href="index.php?halaman=detail_penelitian&id_lit=<?php echo $value['id_lit']; ?>" class="btn btn-xs btn-info"><i class="fa fa-eye"></i> Detail</a>
											<a href="index.php?halaman=ubah_penelitian&id_lit=<?php echo $value['id_lit']; ?>" class="btn btn-xs btn-warning"> <i class="fa fa-edit"></i> Ubah</a>
											<a href="index.php?halaman=hapus_penelitian&id_lit=<?php  echo $value['id_lit'] ?>" class="btn btn-xs btn-danger" onclick="return confirm('hapus data ?')" ><i class="fa fa-trash"></i> Hapus</a>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						
					</div>
				</div>
			</div>
		</div>
	</section>
