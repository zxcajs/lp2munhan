<?php Helper::checkPage();?>
<!DOCTYPE html>
<html>
<head>
	<title>Detail</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>

<?php 
$id_pkm = $_GET['id_pkm'];
$data_pkm = $pkm->detail_pkm($id_pkm);

 ?>


 <div class="inner">
 <div class="panel panel-primary">
 <div class="panel-heading text-center">
 <h2>PENGABDIAN KEPADA MASYARAKAT</h2>
 </div>

 <div class="panel-body">
 	

 	<div class="col-md-15">
 	<table class="table table-striped">
 		<tr>
 			<th>Nama Kegiatan</th>
 			<th><?php echo $data_pkm['judul_penelitian']; ?></th>
 		</tr>
 		<tr>
 			<th>Nama Penulis</th>
 			<th><?php echo $data_pkm['nama_penulis']; ?></th>
 		</tr>
 		<tr>
 			<th>Author/Responding Author</th>
 			<th><?php echo $data_pkm['author']; ?></th>
 		</tr>
 		<tr>
 			<th>Jurnal </th>
 			<th><?php echo $data_pkm['jurnal']; ?></th>
 		</tr>
 		<tr>
 			<th>Publisher  </th>
 			<th><?php echo $data_pkm['publiser']; ?></th>
 		</tr>
 		<tr>
 			<th>E-ISSN  </th>
 			<th><?php echo $data_pkm['issn']; ?></th>
 		</tr>
 		<tr>
 			<th>Jadwal terbit </th>
 			<th><?php echo $data_pkm['terbit']; ?></th>
 		</tr>
		 <tr>
 			<th>Tahun </th>
 			<th><?php echo $data_pkm['tahun']; ?></th>
 		</tr>
 		<tr>
 			<th>Negara </th>
 			<th><?php echo $data_pkm['negara']; ?></th>
 		</tr>
 		<tr>
 			<th>URL </th>
 			<th><?php echo $data_pkm['url']; ?></th>
 		</tr>
 	</table>
 	</div>
 </div>
 </div>
 	<a href="index.php?halaman=tampil_pkm" class="btn btn-xs btn-danger"><i class="fa fa-backward"></i> Kembali</a>
 </div>
</body>
</html>