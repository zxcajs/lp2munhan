<?php Helper::checkPage();?>
<?php
$data_pkm = $pkm->tampil_pkm(); 
?>

<section class="content-header">
	<h1>
		PENGABDIAN KEPADA MASYARAKAT
		<small> </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
				<div class="panel-body">
					<div class="inner">
						
					<div class="add-table">

             	  <a type="button" class="btn btn-xs btn-primary btn-flat" href="index.php?halaman=tambah_pkm">
              	  <i class="fa fa-plus-square"></i> Tambah Data</a> 
             	 </div>

						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center w-70">No</th>
									<th>NAMA KEGIATAN </th>
									<th>NAMA PENULIS</th>
									<th class="text-center w-220">Opsi</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data_pkm as $key => $value) : ?>
									<tr>
										<td class="text-center"><?php echo $key+1 ?></td>
										<td><?php echo $value['judul_penelitian']; ?></td>
										<td><?php echo $value ['nama_penulis'];?></td>
										<td class="text-center">
											<a href="index.php?halaman=detail_pkm&id_pkm=<?php echo $value ['id_pkm']; ?>" class="btn btn-xs btn-info"><i class="fa fa-eye"></i> Detail</a>
											<a href="index.php?halaman=ubah_pkm&id_pkm=<?php echo $value['id_pkm']; ?>" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Ubah</a>
											<a href="index.php?halaman=hapus_pkm&id_pkm=<?php  echo $value['id_pkm'] ?>" class="btn btn-xs btn-danger" onclick="return confirm('hapus data ?')" ><i class="fa fa-plus-trash"></i> Hapus</a>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						
					</div>
				</div>
			</div>
		</div>
	</section>
