<?php Helper::checkPage();?>
<section class="content-header">
	<h1>
	Menambahkan Data 
		<small> | PKM  </small>
	</h1>
</section>

<?php 
	// objek pasien menjalankan fungsi tampil_pasien
	$dosenList = $dosen->tampil_dosen();
	$refCountryList = $refData->getCountryList();
	$refPublisherList = $refData->getPublisherList();
	$refJurnalList = $refData->getJurnalList();
	$refAuthorList = $refData->getAuthorList();
	$refProdi = $refData->getProdiList();
	$refFakultas = $refData->getFakultasList();
 ?>

<!-- Main content -->
<section class="inner">
	<!-- Your Page Content Here -->
	<div class="form-body">

		<div class="row">
			<form class="form-horizontal" method="POST" enctype='multipart/form-data'>
				<div class="box-body">

					<div class="form-group">
						<label class="col-md-3 control-label">NAMA KEGIATAN</label>
						<div class="col-sm-7">
							<input class="form-control " name="judul_penelitian" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">NAMA PENULIS (DOSEN)</label>
						<div class="col-sm-7">
							<select class="form-control js-example-basic-multiple" name="nama_penulis[]" multiple="multiple" id="" required>
								<option value="">pilih</option>
								<?php 
								foreach ($dosenList as $key => $value)
								{
									echo "<option value=".$value['id_dosen'].">".$value['nama_dosen']."</option>";
								}
								?> 
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label">AUTHOR/RESPONDING AUTHOR</label>
						<div class="col-sm-7">
							<select class="form-control js-example-basic-single" name="author" id="author">
								<option value="">pilih</option>
								<?php 
									foreach ($refAuthorList as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>

					
					<div class="form-group">
						<label class="col-md-3 control-label">JURNAL</label>
						<div class="col-sm-7">
							<select class="form-control js-example-basic-single" name="jurnal" id="jurnal">
								<option value="">pilih</option>
								<?php 
									foreach ($refJurnalList as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group">
					<label class="col-md-3 control-label">PUBLISHER</label>
						<div class="col-sm-7">
							<select onchange="setOther(this);" class="form-control js-example-basic-single" name="publiser" id="publiser">
								<option value="">pilih</option>
								<?php 
									foreach ($refPublisherList as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group other-publisher hide">
						<label class="col-md-3 control-label"></label>
						<div class="col-sm-7">
							<input class="form-control " placeholder="silahkan isi lainnya" name="other_publisher" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">E-ISSN </label>
						<div class="col-sm-7">
							<input class="form-control" name="issn" >
						</div>
					</div>
					<div class="form-group">
					<label class="col-md-3 control-label">JADWAL TERBIT</label>
						<div class="col-sm-7">
							<input type="date" class="form-control" name="terbit" value="<?date('Y-m-d')?>" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">TAHUN</label>
						<div class="col-sm-7">
							<input type="text" maxlength="4" class="form-control" name="tahun" >
						</div>
					</div>

					<div class="form-group">
					<label class="col-md-3 control-label">NEGARA</label>
						<div class="col-sm-7">
							<select class="form-control js-example-basic-single" name="negara" id="negara">
								<option value="">pilih</option>
								<?php 
									foreach ($refCountryList as $key => $value)
									{
										echo "<option value=".$value['id'].">".$value['name']."</option>";
									}
								?> 
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">AKUN GOOGLE SCHOLAR</label>
						<div class="col-sm-7">
							<select onchange="setGs(this);" class="form-control js-example-basic-single" name="punya_google_scholar" id="punya_google_scholar">
									<option value="">pilih</option>
									<option value="1">ADA </option>
									<option value="0">TIDAK ADA</option>
							</select>
						</div>
					</div>
					<div class="form-group url-google hide">
						<label class="col-md-3 control-label"></label>
						<div class="col-sm-7">
							<input class="form-control " placeholder="silahkan isi link disini" name="url">
						</div>
					</div>
					
						</div><!-- /.box-body -->
						<div class="col-sm-offset-2 col-sm-3">
							<a href="index.php?halaman=tampil_penelitian" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
							<button type="submit" name="simpan" class="btn btn-xs btn-default btn-flat "> <i class="fa fa-save"></i> Simpan</button>
						</div><!-- /.box-footer -->
					</form>
				</div> <!-- row -->
			</div>
		</section>




	<?php 
	if (isset($_POST['simpan'])) 
	{
		foreach($_POST['nama_penulis'] as $dosenId){
			$prodiId = $refData->getProdiId($dosenId);
			$fakultasId = $refData->getFakultasId($prodiId);
			$lit->simpan_lit(
				$dosenId,
				$_POST['judul_penelitian'],
				$_POST['author'],
				$_POST['tahun'],
				$_POST['jurnal'],
				$_POST['other_publisher'],
				$_POST['issn'],
				$_POST['terbit'],
				$_POST['negara'],
				$_POST['punya_google_scholar'],
				$_POST['url'],
				'pkm',
				$_POST['publiser'],
				$prodiId,
				$fakultasId
			);
		}

		echo "<script>alert('data tersimpan')</script>";
		echo "<script>location='index.php?halaman=tampil_pkm'</script>";
	}
	?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>
		$('select[name=negara]').val(100).change();
		function setOther(sel)
		{
			if(sel.value==17){
				$(".other-publisher").removeClass("hide");
			}else{
				$(".other-publisher").addClass("hide");
			}
		}
		function setGs(sel)
		{
			if(sel.value=='1'){
				$(".url-google").removeClass("hide");
			}else{
				$(".url-google").addClass("hide");
			}
		}
	</script>