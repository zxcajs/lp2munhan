<?php Helper::checkPage();?>
<section class="content-header">
	<h1>
		Merubah 
		<small> |  Data Penelitian </small>
	</h1>
</section>		
<div class="panel-body">
<?php 
	$id_lit = $_GET['id_pkm'];
	$data_lit = $lit->detail_lit($id_lit);
	$dosenList = $dosen->tampil_dosen();
	$refCountryList = $refData->getCountryList();
	$refPublisherList = $refData->getPublisherList();
	$refJurnalList = $refData->getJurnalList();
	$refAuthorList = $refData->getAuthorList();
	$refProdi = $refData->getProdiList();
	$refFakultas = $refData->getFakultasList();
	// Helper::dump($data_lit);
	?>

	<section class="inner">
		<!-- Your Page Content Here -->
		<div class="form-body">
			<div class="row">
				<form class="form-horizontal" method="POST" enctype='multipart/form-data'>
					<div class="box-body">
						<div class="form-group">
							<label class="col-md-3 control-label">NAMA KEGIATAN</label>
							<div class="col-sm-7">
								<input class="form-control " value="<?php echo $data_lit['judul_penelitian']?>" name="judul_penelitian" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">NAMA PENULIS (DOSEN)</label>
							<div class="col-sm-7">
								<select class="form-control js-example-basic-single" name="nama_penulis" id="nama_penulis" required>
									<option value="">pilih</option>
									<?php 
									foreach ($dosenList as $key => $value)
									{
										echo "<option value=".$value['id_dosen'].">".$value['nama_dosen']."</option>";
									}
									?> 
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3 control-label">AUTHOR/RESPONDING AUTHOR</label>
							<div class="col-sm-7">
								<select class="form-control js-example-basic-single" name="author" id="author">
									<option value="">pilih</option>
									<?php 
										foreach ($refAuthorList as $key => $value)
										{
											echo "<option value=".$value['id'].">".$value['name']."</option>";
										}
									?> 
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">JURNAL</label>
							<div class="col-sm-7">
								<select class="form-control js-example-basic-single" name="jurnal" id="jurnal">
									<option value="">pilih</option>
									<?php 
										foreach ($refJurnalList as $key => $value)
										{
											echo "<option value=".$value['id'].">".$value['name']."</option>";
										}
									?> 
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">PUBLISHER</label>
							<div class="col-sm-7 ">
								<select onchange="setOther(this);" class="form-control js-example-basic-single" name="publiser" id="publiser">
									<option value="">pilih</option>
									<?php 
										foreach ($refPublisherList as $key => $value)
										{
											echo "<option value=".$value['id'].">".$value['name']."</option>";
										}
									?> 
								</select>
							</div>
						</div>
						<div class="form-group other-publisher hide">
							<label class="col-md-3 control-label"></label>
							<div class="col-sm-7">
								<input class="form-control " value="<?php echo $data_lit['other_publisher']?>" placeholder="silahkan isi lainnya" name="other_publisher" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">E-ISSN </label>
							<div class="col-sm-7">
								<input type="" class="form-control" name="issn" value="<?php echo $data_lit['issn']?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">JADWAL TERBIT</label>
							<div class="col-sm-7">
								<input type="date" class="form-control" name="terbit" value="<?php echo $data_lit['terbit']?>" value="<?date('Y-m-d')?>" required="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">TAHUN</label>
							<div class="col-sm-7">
								<input type="text" maxlength="4" class="form-control" name="tahun" value="<?php echo $data_lit['tahun']?>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">NEGARA</label>
							<div class="col-sm-7">
								<select class="form-control js-example-basic-single" name="negara" id="negara">
									<option value="">pilih</option>
									<?php 
										foreach ($refCountryList as $key => $value)
										{
											echo "<option value=".$value['id'].">".$value['name']."</option>";
										}
									?> 
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">AKUN GOOGLE SCHOLAR</label>
							<div class="col-sm-7">
								<select onchange="setGs(this);" class="form-control js-example-basic-single" name="punya_google_scholar" id="punya_google_scholar">
										<option value="">pilih</option>
										<option value="1">ADA </option>
										<option value="0">TIDAK ADA</option>
								</select>
							</div>
						</div>
						<div class="form-group url-google hide">
							<label class="col-md-3 control-label"></label>
							<div class="col-sm-7">
								<input class="form-control " value="<?php echo $data_lit['url']?>" placeholder="silahkan isi link disini" name="url">
							</div>
						</div>
					</div><!-- /.box-body -->
					<div class="col-sm-offset-2 col-sm-3">
						<a href="index.php?halaman=tampil_penelitian" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
						<button type="submit" name="simpan" class="btn btn-xs btn-default btn-flat "> <i class="fa fa-save"></i> Simpan</button>
					</div><!-- /.box-footer -->
				</form>
			</div> <!-- row -->
		</div>
	</section>
</div>
	<?php if (isset($_POST['simpan'])) 
	{
		$prodiId = $refData->getProdiId($_POST['nama_penulis']);
		$fakultasId = $refData->getFakultasId($prodiId);
		
		$lit->ubah_lit(
			$_POST['nama_penulis'],
			$_POST['judul_penelitian'],
			$_POST['author'],
			$_POST['tahun'],
			$_POST['jurnal'],
			$_POST['other_publisher'],
			$_POST['issn'],
			$_POST['terbit'],
			$_POST['negara'],
			$_POST['punya_google_scholar'],
			$_POST['url'],
			'pkm',
			$_POST['publiser'],
			$prodiId,
			$fakultasId,
			$id_lit
		);
		
		echo "<script>alert('data tersimpan')</script>";
		echo "<script>location='index.php?halaman=tampil_pkm'</script>";
	}

	?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>
		$('select[name=negara]').val(<?php echo $data_lit['ref_country_id'] ?>).change();
		$('select[name=nama_penulis]').val(<?php echo $data_lit['dosen_id'] ?>).change();
		$('select[name=author]').val('<?php echo $data_lit['author'] ?>').change();
		$('select[name=jurnal]').val('<?php echo $data_lit['ref_jurnal'] ?>').change();
		$('select[name=publiser]').val(<?php echo $data_lit['ref_publisher_id'] ?>).change();
		$('select[name=punya_google_scholar]').val(<?php echo $data_lit['punya_google_scholar'] ?>).change();
		if(<?php echo $data_lit['punya_google_scholar'] ?>=='1'){
			$(".url-google").removeClass("hide");
		}else{
			$(".url-google").addClass("hide");
		}
		if(<?php echo $data_lit['ref_publisher_id'] ?>==17){
				$(".other-publisher").removeClass("hide");
			}else{
				$(".other-publisher").addClass("hide");
			}

		function setOther(sel)
		{
			if(sel.value==17){
				$(".other-publisher").removeClass("hide");
			}else{
				$(".other-publisher").addClass("hide");
			}
		}
		function setGs(sel)
		{
			if(sel.value=='1'){
				$(".url-google").removeClass("hide");
			}else{
				$(".url-google").addClass("hide");
			}
		}
	</script>