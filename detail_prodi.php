<?php include'config/konek_database.php'; ?>


<!DOCTYPE html>
<html>
<head>
	<title>detail</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
</head>
<body>

<?php 
//mengambil id pasien dari url
$id_prodi = $_GET['id_prodi'];
$dataprodi = $prodi->detail_prodi($id_prodi);


$koneksi = mysqli_connect("localhost", "root", "","cff");
$jml_lit = mysqli_query($koneksi, "SELECT jml_lit_prodi FROM prodi");
$name_prod = mysqli_query($koneksi, "SELECT id_prodi FROM prodi");




 ?>
 
 
<div class="inner">
	<div class="panel panel-primary">
 	<div class="panel-heading text-center">
 		<h2>Data Prodi</h2>
 	</div>
 	<div class="panel-body">
 	
 	<div class="col-md-6">
 		<table class="table table-striped" id="thetable" >
 			<tr>
 				<th>PRODI</th>
 				<th><?php echo $dataprodi['nama_prodi']; ?></th>
 			</tr>
 			<tr>
 				<th>FAKULTAS</th>
 				<th><?php echo $dataprodi['nama_fakultas']; ?></th>
 			</tr>

 			<tr>
 				<th>Responding Author Tim/Mandiri</th>

 			</tr>
 			<tr>
 				<th>Responding Author Bersama Mahasiswa</th>
 			</tr>
 			<tr>
 				<th>Jurnal Nasional Akreditasi</th>
 			</tr>
 			<tr>
 				<th>Jurnal Nasional Non Akreditasi</th>
 			</tr>
 			<tr>
 				<th>Jurnal International Reputasi</th>
 			</tr>
 			<tr>
 				<th>Jurnal International Non Reputasi</th>
 			</tr>
 			<tr>
 				<th>JUMLAH PENELITIAN</th>
 				<th><?php echo $dataprodi['jml_lit_prodi']; ?></th>
 			</tr>
 			
 		</table>
 	</div>
 	<div class="col-md-6">
 		<canvas id="myPieChart"></canvas>
			<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3"></script>
			<script>
			  var mychart = document.getElementById("myPieChart").getContext('2d');
			  let round_graph = new Chart(mychart, {
			    type: 'bar',
			    data: {
			      labels: [<?php while ($b = mysqli_fetch_array($name_prod)) { echo '"' . $b['id_prodi'] . '",';}?>],
			          datasets: [
				          	{
				            label: "Data Penlitian Dosen",
				            data: [<?php while ($p = mysqli_fetch_array($jml_lit)) { echo '"' . $p['jml_lit_prodi'] . '",';}?>],
					            backgroundColor: [
					              '#29B0D0',
					              '#2A516E',
					              '#F07124',
					              '#CBE0E3',
					              '#979193'
					            ]

				          	}

			          	]

			    			}
			  			})
			</script>

				
	</div>		
 	</div>
 </div>
<a href="index.php?halaman=tampil_prodi" class="btn btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
</div>
 </body>
</html>