<?php include 'config/konek_database.php' ?>
<?php include 'config/app.config.php' ?>
<?php include 'Helper/Helper.php'; ?>
<?php include 'config/helper.php' ?>
<?php
$data_pimpinan = $pimpinan->tampil_pimpinan();
?>

<!DOCTYPE html>
<html>
<head>	
	<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>SIM LPPM UNHAN RI</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
		<link rel="stylesheet" href="css/linearicons.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/magnific-popup.css">
		<link rel="stylesheet" href="css/jquery-ui.css">				
		<link rel="stylesheet" href="css/nice-select.css">							
		<link rel="stylesheet" href="css/animate.min.css">
		<link rel="stylesheet" href="css/owl.carousel.css">				
		<link rel="stylesheet" href="css/main.css">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> SIM LPPM UNHAN RI</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../dist/css/admin.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="assets/flex/flexslider.css">

		<style type="text/css">

		@media(min-width: 768px)
		{
			
			.form-body
			{
				padding: 1.4%;
				background: #050a24;
				min-height: 550px;
				border: 1px solid #000;
				box-shadow: -10px 50px 12px rgba(0, 0, 0, 0.66);
				border-bottom-right-radius: 5%; 
				border-bottom-left-radius: 5%; 
				border-top-left-radius: 5%;
				border-top-right-radius: 5%;
				border-bottom: 1px solid rgba(229, 75, 56, 0.85);
			}			
		}
	</style>
	
</head>
<body>	
	<header id="header">
				<div class="header-top">
					<div class="container">
			  		<div class="row align-items-center">
			  			<div class="col-lg-6 col-sm-6 col-6 header-top-left">
			  				<ul>
							 	 <li><a href="<?php echo Helper::baseUrl()?>index.php">HOME</a></li>
                                <li><a href="<?php echo Helper::baseUrl()?>admin/login/login.php">LOGIN</a></li>
			  				</ul>			
			  			</div>
			  			<div class="col-lg-6 col-sm-6 col-6 header-top-right">
							<div class="header-social">
								<a href="https://id-id.facebook.com/unhanhumas.idu.ac.id" target="_blank"><i class="fa fa-facebook"></i></a>

								<a href="https://twitter.com/Unhan_RI" target="_blank"><i class="fa fa-twitter"></i></a>

								<a href="https://www.instagram.com/unhan_ri" target="_blank"><i class="fa fa-instagram"></i></a>

								<a href="https://www.youtube.com/c/universitaspertahananofficial" target="_blank"><i class="fa fa-youtube"></i></a>

							</div>
			  			</div>
			  		</div>			  					
					</div>
				</div>
				<div class="container main-menu">
					<div class="row align-items-center justify-content-between d-flex">
				      <div id="logo">
				        <a href="<?php echo Helper::baseUrl()?>index.php"><img src="assets/img/penyakit/kecil.png" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>
				      <div id="logo">
				        <a href="index.html"><img src="" alt="" title="" /></a>
				      </div>



				      <nav id="nav-menu-container">
				        <ul class="nav-menu">
				          <li><a href="index_sim.php">Home</a></li>
				           <li class="menu-has-children"><a href="">Tentang LPPM</a>
				            <ul>
				              <li><a href="<?php echo Helper::baseUrl()?>organisasi.php">Organisasi</a></li>
				              <li><a href="<?php echo Helper::baseUrl()?>tampil_pimpinan.php">Pimpinan</a></li>
				              <li><a href="<?php echo Helper::baseUrl()?>tampil_staff.php">Staff</a></li>
				            </ul>
				          </li>
				          <li class="menu-has-children"><a href="">Kerja Sama LPPM</a>
				            <ul>
				              <li><a href="<?php echo Helper::baseUrl()?>tampil_kermadl.php">Dalam Negeri</a></li>
				              <li><a href="<?php echo Helper::baseUrl()?>tampil_kermaln.php">Luar Negeri</a></li>
				            </ul>
				          </li>
				          	
				    					          					          		          
				         
				        </ul>
				      </nav><!-- #nav-menu-container -->					      		  
					</div>
				</div>
			</header>
			
		
			

			<!-- start footer Area -->		
			<footer class="footer-area section-gap">		
				<div class="row footer-bottom d-flex justify-content-between align-items-center">	
					<div class="text-center">
						<h1 class="text-white">SISTEM INFORMASI MANAJEMEN LPPM </h1>
						<h1 class="text-white">DATA PIMPINAN LPPM </h1>
						<h2 class="text-white"> </h2>
						<h6 class="text-white"> LEMBAGA PENELITIAN & PENGABDIAN KEPADA MASYARAKAT  </h6>
					</div>		
				</div>
				<div class="row footer-bottom d-flex justify-content-between align-items-center">	</div>
			</footer>
			<!-- End footer Area -->

<body>
 <div class="panel-primary">	
	<div class="panel-body">
		<section class="inner">
			<div class="form-body wow fadeIn animated">
				<div class="box">
					<div class="panel-body">
						<div class="inner">					 
					 		
								<div class=" text-center inner" >
									
										<div class=" panel panel-primary">
											<?php foreach ($data_pimpinan as $key => $value) : ?>
											<div class="panel-body">
											<div class="col-md-4">
												<img src="assets/img/pimpinan/<?php echo $value['foto_pimpinan'] ?>" class="img-responsive" width="250">
											</div>
											 	<div class="col-md-6">
											 	<table class="table table-striped">	
											 		<tr>
											 			<th>NAMA PIMPINAN</th>
											 			<td class="text-left">: <?php echo $value['nama_pimpinan']; ?></td>
											 		</tr>
											 		<tr>
											 			<th>NIP/NRP</th>
											 			<td class="text-left">: <?php echo $value['nomor']; ?></td>	
											 		</tr>
											 		<tr>
											 			<th>PANGKAT</th>
											 			<td class="text-left">: <?php echo $value ['pangkat']; ?></td>		 			
											 		</tr>
											 		<tr>
											 			<th>JABATAN</th>
											 			<td class="text-left">: <?php echo $value ['jabatan']; ?></td>		 		
											 		</tr>				 	
											 	</table>		
											 	</div>
											 </div>
											 <?php endforeach ?>	
										</div>
									
								</div>
							
						</div>
					</div>
				</div>
			</div>
			</section>
		</div>
	</div>
</body>


			<footer class="footer-area section-gap">
				<div class="container">

					<div class="row">
						<div class="col-lg-3  col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>LPPM Unhan RI</h6>
								<p>
									Merupakan lembaga...
								</p>
							</div>
						</div>
						<div class="col-lg-5 col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>Kantor LPPM UNHAN RI</h6>
								<div class="row">
									<div class="col">
										<ul>
											<li><a href="#"> UNIVERSITAS PERTAHANAN RI</a></li>
											<li><a href="#"> PROGRAM STUDI</a></li>
											<li><a href="#"> PASCASARJANA & DOKTORAL </a></li>
											<li><a href="https://goo.gl/maps/FjcfbZjpXkSCMLL87">Jl. Salemba Raya No.3, RT.1/RW.3, Paseban, Jakarta, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10440</a></li>
										
										</ul>
									</div>
																	
								</div>							
							</div>
						</div>							
					
						<div class="col-lg-3  col-md-6 col-sm-6">
							<div class="single-footer-widget">
								<h6>Email</h6>
								<p>
									More information about LPPM UNHAN
								</p>								
								<div id="mc_embed_signup">
									<form target="_blank" action="">
										<div class="input-group d-flex flex-row">
											<input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
											<button class="btn bb-btn"><span class="lnr lnr-location"></span></button>		
										</div>									
										<div class="mt-10 info"></div>
									</form>
								</div>
							</div>
						</div>					
					</div>
				

					<div class="row footer-bottom d-flex justify-content-between align-items-center">
						<p class="col-lg-8 col-sm-12 footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | UNIVERSITAS PERTAHANAN REPUBLIK INDONESIA  <a href="https://www.idu.ac.id/" target="_blank">www.idu.ac.id</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
						<div class="col-lg-4 col-sm-12 footer-social">
							<a href="https://id-id.facebook.com/unhanhumas.idu.ac.id" target="_blank"><i class="fa fa-facebook"></i></a>
							<a href="https://twitter.com/Unhan_RI" target="_blank"><i class="fa fa-twitter"></i></a>
							<a href="https://www.instagram.com/unhan_ri" target="_blank"><i class="fa fa-instagram"></i></a>
							<a href="https://www.youtube.com/c/universitaspertahananofficial" target="_blank"><i class="fa fa-youtube"></i></a>
						</div>
					</div>
				</div>
			</footer>
			<!-- End footer Area -->	


	

			<script src="js/vendor/jquery-2.2.4.min.js"></script>
			<script src="js/popper.min.js"></script>
			<script src="js/vendor/bootstrap.min.js"></script>			
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>		
 			<script src="js/jquery-ui.js"></script>					
  			<script src="js/easing.min.js"></script>			
			<script src="js/hoverIntent.js"></script>
			<script src="js/superfish.min.js"></script>	
			<script src="js/jquery.ajaxchimp.min.js"></script>
			<script src="js/jquery.magnific-popup.min.js"></script>						
			<script src="js/jquery.nice-select.min.js"></script>					
			<script src="js/owl.carousel.min.js"></script>							
			<script src="js/mail-script.js"></script>	
			<script src="js/main.js"></script>	
		</body>

		






</html>


