<section class="content-header">
	<h1>
		Merubah 
		<small> |  Data Penelitian </small>
	</h1>
</section>
<section class="inner">
	<div class="form-body wow fadeIn animated">
		<div class="box">
			
				<div class="panel-body">
					<?php 
					$id_lit = $_GET['id_lit'];
					$data_lit = $lit->detail_lit($id_lit);
					?>

<section class="inner">
	<!-- Your Page Content Here -->
	<div class="form-body">

		<div class="row">
			<form class="form-horizontal" method="POST" enctype='multipart/form-data'>
				<div class="box-body">
					<div class="form-group">
						<label class="col-md-3 control-label">NAMA PENULIS</label>
						<div class="col-sm-7">
							<input type="" class="form-control" name="nama_penulis" value="<?php echo $data_lit['nama_penulis']?>">
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label">JUDUL PENELITIAN</label>
						<div class="col-sm-7">
							<textarea class="form-control"  name="judul_penelitian" value="<?php echo $data_lit['judul_penelitian']?>"></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-3 control-label">AUTHOR/RESPONDING AUTHOR</label>
						<div class="col-sm-7">
							<select class="form-control" name="author" id="author">
								<option value="">pilih</option>
								<option value="Tim">Tim</option>
								<option value="Mahasiswa">Mahasiswa</option>
								<option value="Mandiri">Mandiri</option>
							</select>
						</div>
					</div>

					
					<div class="form-group">
						<label class="col-md-3 control-label">JURNAL</label>
						<div class="col-sm-7">
							<select class="form-control" name="jurnal" id="">
									<option value="">pilih</option>
									<option value="Nasional (Terkareditasi)">Nasional (Terkareditasi) </option>
									<option value="Nasional (Non Terakreditasi)">Nasional (Non Terakreditasi)</option>
									<option value="International (Bereputasi)">International (Bereputasi)</option>
									<option value="International (Non Reputasi)">International (Non Reputasi)</option>
							</select>
						</div>
					</div>
					<div class="form-group">
					<label class="col-md-3 control-label">PUBLISHER</label>
						<div class="col-sm-7 ">
							<select class="form-control" name="publiser" id="author">
								<option value="<?php echo $data_lit['publiser']?>">pilih</option>
								<option value="Sinta">Sinta </option>
								<option value="ISI Knowledge – Thomson Reuter (USA)">ISI Knowledge – Thomson Reuter (USA)</option>
								<option value="SCOPUS">SCOPUS</option>
								<option value="Ulrich’s Periodicals Directory (Proquest)">Ulrich’s Periodicals Directory (Proquest)</option>
								<option value="Academic Search Complete (EBSCO)">Academic Search Complete (EBSCO)</option>
								<option value="Zentralblatt MATH (Springer - Verlag)">Zentralblatt MATH (Springer - Verlag)</option>
								<option value="DOAJ (Lund University Swedia)">DOAJ (Lund University Swedia)</option>
								<option value="Peridoque (EP Lausanne Switzerland)">Peridoque (EP Lausanne Switzerland)</option>
								<option value="SHERPA/ROMEO (Nottingham University,UK)">SHERPA/ROMEO (Nottingham University,UK)</option>
								<option value="Index Copernicus (Poland)">Index Copernicus (Poland)</option>
								<option value="Google scholar">Google scholar</option>
								<option value="Elsevier">Elsevier</option>
								<option value="Springer">Springer</option>
								<option value="Directory of Open Access Journals (DOAJ)">Directory of Open Access Journals (DOAJ)</option>
								<option value="Wiley Online Library">Wiley Online Library</option>
								<option value="Taylor & Francis">Taylor & Francis</option>
								<input value="" placeholder="Ketik disini jika tidak ada di pilihan" type="" class="form-control" name="publiser" required="">
							</select>
						</div>
					</div>
					<div class="form-group">
					<label class="col-md-3 control-label">E-ISSN </label>
						<div class="col-sm-7">
							<input type="" class="form-control" name="issn" value="<?php echo $data_lit['issn']?>">
						</div>
					</div>
					<div class="form-group">
					<label class="col-md-3 control-label">JADWAL TERBIT</label>
						<div class="col-sm-7">
							<input type="date" class="form-control" name="terbit" value="<?date('Y-m-d')?>" required="">
						</div>
					</div>
					<div class="form-group">
					<label class="col-md-3 control-label">NEGARA</label>
						<div class="col-sm-7">
							<input type="" class="form-control" name="negara" value="<?php echo $data_lit['negara']?>">
						</div>
					</div>
					<div class="form-group">
					<label class="col-md-3 control-label">URL</label>
						<div class="col-sm-7">
							<input type="" class="form-control" name="url" value="<?php echo $data_lit['url']?>">
						</div>
					</div>
						</div><!-- /.box-body -->
						<div class="col-sm-offset-2 col-sm-3">
							<button type="submit" name="simpan" class="btn btn-default btn-flat "> <i class="fa fa-save"></i> Simpan</button>
							<a href="index.php?halaman=tampil_penelitian" class="btn btn-danger btn-flat"><i class="fa fa-backward"></i> Kembali</a>
						</div><!-- /.box-footer -->
					</form>
				</div> <!-- row -->
			</div>
		</section>
			</div>
			
		</div>
	</section>
	<?php if (isset($_POST['simpan'])) 
	{
		$lit->ubah_lit($_POST['nama_penulis'],$_POST['judul_penelitian'],$_POST['author'],$_POST['jurnal'],$_POST['publiser'],$_POST['issn'],$_POST['terbit'],$_POST['negara'],$_POST['url'],$id_lit);

	//echo "<script>alert('data pasien tersimpan')</script>";
		echo "<script>location='index.php?halaman=tampil_lit'</script>";
	}

	?>